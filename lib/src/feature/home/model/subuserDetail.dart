class SubuserDetailNew {
  Profile? profile;
  List<String>? document;

  SubuserDetailNew({this.profile, this.document});

  SubuserDetailNew.fromJson(Map<String, dynamic> json) {
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    document = json['document'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    data['document'] = this.document;
    return data;
  }
}

class Profile {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? profileImageName;
  String? operatorsLicense;
  String? tbClearance;
  String? physicalCheckup;
  String? miscDoc;
  int? userType;
  int? masterUser;
  String? createdAt;
  String? updatedAt;

  Profile(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.profileImageName,
      this.operatorsLicense,
      this.tbClearance,
      this.physicalCheckup,
      this.miscDoc,
      this.userType,
      this.masterUser,
      this.createdAt,
      this.updatedAt});

  Profile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    profileImageName = json['profile_image_name'].toString();
    operatorsLicense = json['operators_license'];
    tbClearance = json['tb_clearance'];
    physicalCheckup = json['physical_checkup'];
    miscDoc = json['misc_doc'];
    userType = json['user_type'];
    masterUser = json['master_user'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['profile_image_name'] = this.profileImageName;
    data['operators_license'] = this.operatorsLicense;
    data['tb_clearance'] = this.tbClearance;
    data['physical_checkup'] = this.physicalCheckup;
    data['misc_doc'] = this.miscDoc;
    data['user_type'] = this.userType;
    data['master_user'] = this.masterUser;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
