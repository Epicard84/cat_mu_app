// import 'dart:convert';
// import 'package:cat/src/core/common/client/APIhelper.dart';
// import 'package:cat/src/core/common/widget/commonAppbar.dart';
// import 'package:cat/src/core/common/widget/commonButton.dart';
// import 'package:cat/src/core/common/widget/common_textfield.dart';
// import 'package:cat/src/core/common/widget/snackbar.dart';
// import 'package:cat/src/feature/home/ui/clients/medication/add/add_note_vm.dart';
// import 'package:cat/src/feature/home/ui/clients/medication/noteScreen.dart';
// import 'package:cat/src/core/constant/constant.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:http/http.dart' as http;

// class AddNoteScreen extends StatefulWidget {
//   final MedData data;
//   final bool isChart;
//   final bool isNarrative;

//   const AddNoteScreen(
//       {super.key,
//       required this.data,
//       required this.isChart,
//       required this.isNarrative});
// //
//   @override
//   State<AddNoteScreen> createState() => _AddNoteScreenState();
// }

// class _AddNoteScreenState extends State<AddNoteScreen> {
//   TextEditingController title = TextEditingController();
//   bool loading = false;
//   final client = HTTPClient();

//   addDetails(AddNoteVM _vm) async {
//     try {
//       setState(() {
//         loading = true;
//       });

// // [type: note=0, chart=1, narrative=2]
//       var data = {
//         'description': title.text,
//         'patient_id': widget.data.patientId,
//         'schedule_id': widget.data.scheduleId,
//         'type': widget.isChart
//             ? '1'
//             : widget.isNarrative
//                 ? '2'
//                 : '0',
//         'date': widget.data.date
//       };
//       http.StreamedResponse res = await client.postFileWithToken(
//           APIEndpoint.newDetails, data,
//           fileKey: "detailsImage", files: _vm.docFile);
//       var respp = await res.stream.bytesToString();
//       if (res.statusCode == 200) {
//         var jsonResp = jsonDecode(respp);
//         if (jsonResp['success']) {
//           showSnackBar(context, text: "${jsonResp['message']}", success: true);
//           Navigator.pop(context, true);
//         }
//       }
//     } catch (e) {
//       print("err >> $e");
//     } finally {
//       setState(() {
//         loading = false;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Consumer(builder: (context, ref, child) {
//       final _vm = ref.watch(addNoteVm);
//       return Container(
//           height: double.infinity,
//           width: double.infinity,
//           decoration: BoxDecoration(
//               image: DecorationImage(
//                   image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
//           child: Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CommonAppBar(
//                   isBack: true,
//                   title:
//                       "Add ${widget.isChart ? 'Prog Notes' : widget.isNarrative ? 'Narrative' : 'Note'}"),
//               body: Padding(
//                 padding: const EdgeInsets.all(15.0),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     CommonTextFiled(
//                         controller: title,
//                         maxLine: 4,
//                         keyboardType: TextInputType.multiline,
//                         hintText: "Enter Description",
//                         name: "Add Title",
//                         validator: null),
//                     SizedBox(height: 20),
//                     GestureDetector(
//                       onTap: () {
//                         _vm.mediaOption(context);
//                       },
//                       child: CommonTextFiled(
//                           isEnabled: false,
//                           suffixIcon: CommonButtonWidget(
//                               width: 150,
//                               height: 60,
//                               onTap: () {},
//                               text: "Upload"),
//                           hintText: "Documents",
//                           name: "Documents",
//                           validator: null),
//                     ),
//                     SizedBox(height: 20),
//                     ListView.builder(
//                         shrinkWrap: true,
//                         physics: NeverScrollableScrollPhysics(),
//                         itemCount: _vm.docFile.length,
//                         padding: EdgeInsets.zero,
//                         itemBuilder: (BuildContext context, int i) {
//                           var text = _vm.docFile[i];
//                           return Padding(
//                             padding: const EdgeInsets.symmetric(vertical: 5.0),
//                             child: ListTile(
//                                 trailing: IconButton(
//                                     onPressed: () {
//                                       _vm.removeFile(i);
//                                     },
//                                     icon: Icon(Icons.close)),
//                                 shape: RoundedRectangleBorder(
//                                     borderRadius:
//                                         BorderRadius.all(Radius.circular(10))),
//                                 tileColor: Colors.white,
//                                 title: Text(text.path.split('/').last)),
//                           );
//                         }),
//                     SizedBox(height: 20),
//                     loading
//                         ? Center(child: CircularProgressIndicator())
//                         : CommonButtonWidget(
//                             onTap: () {
//                               addDetails(_vm);
//                             },
//                             text: "Add Now"),
//                     SizedBox(height: 20),
//                   ],
//                 ),
//               )));
//     });
//   }
// }
