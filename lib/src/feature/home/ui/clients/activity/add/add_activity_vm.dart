import 'dart:io';
import 'package:cat/src/feature/home/model/activity.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final addActivityVm = ChangeNotifierProvider.autoDispose<AddActivityVm>((ref) {
  return AddActivityVm();
});

class AddActivityVm with ChangeNotifier {
  List<String> timesString = [
    // "00:00",
    "01:00",
    "02:00",
    "03:00",
    "04:00",
    "05:00",
    "06:00",
    "07:00",
    "08:00",
    "09:00",
    "10:00",
    "11:00",
    "12:00",
    "13:00",
    "14:00",
    "15:00",
    "16:00",
    "17:00",
    "18:00",
    "19:00",
    "20:00",
    "21:00",
    "22:00",
    "23:00",
    "24:00",
  ];

  AddActivityVm() {}

  bool showAdd = true;

  List<ActivityModel> activityList = [
    ActivityModel(
        activity: TextEditingController(),
        start: "",
        times: ["01:00"],
        perDay: "1",
        desc: TextEditingController(),
        image: [])
  ];

  setData(List<GetActivities> item) {
    if (item.length > 0) {
      activityList.removeAt(0);
    }
    for (var i in item) {
      // List<TextEditingController> editing = [];
      List<String> editing = [];
      var k = i.scheduledTime?.split(",") ?? [];
      for (var j in k) {
        // editing.add(TextEditingController(text: j.trim()));
        if (timesString.contains(j.trim())) {
          editing.add(j.trim());
        } else {
          editing.add("01:00");
        }
      }

      activityList.add(ActivityModel(
          id: "${i.id}",
          isUpdate: true,
          times: editing,
          activity: TextEditingController(text: i.activityName),
          start: "${i.startDate}",
          perDay: "${i.timeDays}",
          desc: TextEditingController(text: i.descriptions),
          imagepath: i.docs ?? [],
          status: "${i.status ?? 0}",
          image: []));
    }
  }

  updateStatus(int index, String status) {
    activityList[index].status = status;
    notifyListeners();
  }

  addActivity() {
    activityList.add(ActivityModel(
        activity: TextEditingController(),
        start: "",
        times: ["01:00"],
        perDay: "1",
        desc: TextEditingController(),
        image: []));
    showAdd = false;
    notifyListeners();
  }

  setTimeDropdown(int medIdx, int ctrIdx, String text) {
    activityList[medIdx].times[ctrIdx] = text;
    notifyListeners();
  }

  // setCtrl(int medIdx, int ctrIdx, String text) {
  //   activityList[medIdx].times[ctrIdx] = TextEditingController(text: text);
  //   notifyListeners();
  // }

  addTime(int index, String time) {
    int tim = int.parse(time);
    activityList[index].times = [];
    for (int i = 0; i < tim; i++) {
      activityList[index].times.add("01:00");
    }

    // int tim = int.parse(time);
    // activityList[index].times = [];
    // for (int i = 0; i < tim; i++) {
    //   activityList[index].times.add(TextEditingController());
    // }

    notifyListeners();
  }

  removeFile(int index, int imgIndex) async {
    activityList[index].image.remove(activityList[index].image[imgIndex]);
    notifyListeners();
  }

  // removeTime(int index) {
  //   activityList[index].times.remove(activityList[index].times.last);
  //   notifyListeners();
  // }

  removeActivity(index) {
    activityList.remove(index);
    notifyListeners();
  }

  setTime(String time, int index) {
    // sGender = gen;
    activityList[index].perDay = time;
    notifyListeners();
  }

  mediaOption(context, index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                      leading: Icon(Icons.photo_camera_outlined),
                      onTap: () {
                        openCamera(index);
                        Navigator.pop(context);
                      },
                      title: Text("Camera")),
                  ListTile(
                      leading: Icon(Icons.folder_open_outlined),
                      onTap: () {
                        pickFile(index);
                        Navigator.pop(context);
                      },
                      title: Text("Gallery")),
                ],
              ),
            ),
          );
        });
  }

  pickFile(index) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      activityList[index].image.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  pickPhotos(index) async {
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      activityList[index].image.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  void openCamera(index) async {
    var imgCamera = await ImagePicker().pickImage(source: ImageSource.camera);
    if (imgCamera != null) {
      activityList[index].image.add(File(imgCamera.path));
    }
    notifyListeners();
  }
}

class ActivityModel {
  TextEditingController activity;
  String id;
  String start;
  String perDay;
  TextEditingController desc;
  List<File> image;
  // List<TextEditingController> times;
  List<String> times;
  List<String> imagepath;
  String status;
  bool isUpdate;

  ActivityModel(
      {required this.activity,
      required this.start,
      required this.perDay,
      required this.desc,
      required this.image,
      this.status = "0",
      required this.times,
      this.isUpdate = false,
      this.imagepath = const [],
      this.id = ""});
}
