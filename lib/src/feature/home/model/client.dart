class ClientResp {
  bool? success;
  String? message;
  List<ClientData>? data;

  ClientResp({this.success, this.message, this.data});

  ClientResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['data'] != null) {
      data = <ClientData>[];
      json['data'].forEach((v) {
        data!.add(new ClientData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ClientData {
  int? id;
  String? firstName;
  String? lastName;
  String? dob;
  String? gender;
  String? additionalMiscInformation;
  String? status;
  List<String>? documents;
  String? masterUser;
  List<Doctor>? doctor;
  List<Guardian>? guardian;
  String? createdAt;

  ClientData(
      {this.id,
      this.firstName,
      this.lastName,
      this.dob,
      this.gender,
      this.additionalMiscInformation,
      this.status,
      this.documents,
      this.createdAt,
      this.masterUser,
      this.doctor,
      this.guardian});

  ClientData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    gender = json['gender'];
    additionalMiscInformation = json['additional_misc_information'];
    status = json['status'];
    createdAt = json['created_at'];
    documents = json['documents'].cast<String>();
    masterUser = json['master_user'].toString();
    if (json['doctor'] != null) {
      doctor = <Doctor>[];
      json['doctor'].forEach((v) {
        doctor!.add(new Doctor.fromJson(v));
      });
    }
    if (json['guardian'] != null) {
      guardian = <Guardian>[];
      json['guardian'].forEach((v) {
        guardian!.add(new Guardian.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['additional_misc_information'] = this.additionalMiscInformation;
    data['status'] = this.status;
    data['documents'] = this.documents;
    data['master_user'] = this.masterUser;
    data['created_at'] = this.createdAt;
    if (this.doctor != null) {
      data['doctor'] = this.doctor!.map((v) => v.toJson()).toList();
    }
    if (this.guardian != null) {
      data['guardian'] = this.guardian!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Doctor {
  int? id;
  String? clientId;
  String? doctorName;
  String? doctorPhoneNumber;
  String? doctorAddress;

  Doctor(
      {this.id,
      this.clientId,
      this.doctorName,
      this.doctorPhoneNumber,
      this.doctorAddress});

  Doctor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    doctorName = json['doctor_name'];
    doctorPhoneNumber = json['doctor_phone_number'];
    doctorAddress = json['doctor_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['doctor_name'] = this.doctorName;
    data['doctor_phone_number'] = this.doctorPhoneNumber;
    data['doctor_address'] = this.doctorAddress;
    return data;
  }
}

class Guardian {
  int? id;
  int? clientId;
  String? guardianName;
  String? guardianPhoneNumber;
  String? guardianAddress;

  Guardian(
      {this.id,
      this.clientId,
      this.guardianName,
      this.guardianPhoneNumber,
      this.guardianAddress});

  Guardian.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    guardianName = json['guardian_name'];
    guardianPhoneNumber = json['guardian_phone_number'];
    guardianAddress = json['guardian_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['guardian_name'] = this.guardianName;
    data['guardian_phone_number'] = this.guardianPhoneNumber;
    data['guardian_address'] = this.guardianAddress;
    return data;
  }
}
