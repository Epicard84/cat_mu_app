import 'dart:convert';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/auth/forget/enterCodeScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import '../../../core/common/client/APIhelper.dart';
import '../../../core/common/widget/snackbar.dart';

class ForgetScreen extends StatefulWidget {
  const ForgetScreen({super.key});

  @override
  State<ForgetScreen> createState() => _ForgetScreenState();
}

class _ForgetScreenState extends State<ForgetScreen> {
  TextEditingController email = TextEditingController();
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  bool loading = false;
  final client = HTTPClient();

  onForget(BuildContext context, {required String email}) async {
    if (email.isEmpty) {
      showSnackBar(context, text: ValidationString.emailRequried);
      return null;
    }
    if (!email.validateEmail()) {
      showSnackBar(context, text: ValidationString.validEmail);
      return null;
    }

    try {
      setState(() {
        loading = true;
      });
      var res = await await client
          .postMethod(APIEndpoint.forgetPassword, {'email': email});
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      EnterCodeScreen(email: email)));
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
            child: SingleChildScrollView(
                child: Column(children: [
              CommonAppBar(isBack: true),
              Padding(
                  padding: EdgeInsets.all(25),
                  child: FormBuilder(
                    key: formKey,
                    child: Column(children: [
                      SizedBox(height: 50),
                      Image.asset(AppAsset.forgetIcons, height: 80),
                      SizedBox(height: 75),
                      BoldText(text: "Enter your email address!"),
                      SizedBox(height: 15),
                      SubText(
                          text:
                              "Please enter your email address to receive \na verification code."),
                      SizedBox(height: 50),
                      CommonTextFiled(
                          controller: email,
                          keyboardType: TextInputType.emailAddress,
                          hintText: "Enter email address",
                          name: "Enter email address",
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(),
                            FormBuilderValidators.email(),
                          ])),
                      Align(
                          heightFactor: 3.8,
                          alignment: Alignment.bottomCenter,
                          child: loading
                              ? CircularProgressIndicator()
                              : CommonButtonWidget(
                                  onTap: () {
                                    if (formKey.currentState!.validate()) {
                                      onForget(context,
                                          email: email.text.trim());
                                    }
                                  },
                                  text: "Next"))
                    ]),
                  ))
            ]))));
  }
}
