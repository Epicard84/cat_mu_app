import 'dart:convert';
import 'dart:io';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/clientsDetailsContainer.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/doctorInfo.dart';
import 'package:cat/src/core/common/widget/pdfDoc.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/client.dart';
// import 'package:cat/src/feature/home/ui/clients/clientVm.dart';
import 'package:cat/src/feature/home/ui/clients/medication/model/archived_medication.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:share_plus/share_plus.dart';

class ClientsDetails extends StatefulWidget {
  final ClientData clientData;
  const ClientsDetails({super.key, required this.clientData});

  @override
  State<ClientsDetails> createState() => _ClientsDetailsState();
}

class _ClientsDetailsState extends State<ClientsDetails> {
  GlobalKey _containerKey = GlobalKey();
  bool loading = false;
  List<ArchivedMedicationData> medicineList = [];

  final client = HTTPClient();

  void initState() {
    super.initState();
    fetchArchivedMedicine();
  }

  fetchArchivedMedicine() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await client.getWithToken(
          "${APIEndpoint.archivedMedictaionlist}/${widget.clientData.id}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          ArchivedMedication resp = ArchivedMedication.fromJson(jsonResp);
          medicineList = resp.data ?? [];
        }
      }
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Consumer(builder: (context, ref, child) {
      // final _vm = ref.watch(clientsProvider);
      // List<Widget> pageView = [
      //   ListView.builder(
      //       shrinkWrap: true,
      //       physics: NeverScrollableScrollPhysics(),
      //       itemCount: _vm.medicinesList.length,
      //       padding: EdgeInsets.zero,
      //       itemBuilder: (BuildContext context, int i) {
      //         var list = _vm.medicinesList[i];
      //         return MedicinesList(
      //             medicinesName: list["medicinesName"]!,
      //             onTap1: () {
      //               _vm.countingM();
      //             },
      //             onTap2: () {
      //               _vm.countingP();
      //             },
      //             menu: () {
      //               setState(() {
      //                 print("dybscyub");
      //               });
      //             },
      //             text: "$hours");
      //       }),
      //   // Center(child: Pages(text: "Page 2"))
      // ];

      return Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(50),
              child: CommonAppBar(
                title: "Clients",
                actions: [
                  IconButton(
                      onPressed: () async {
                        File file = await captureImage(context, _containerKey);
                        Share.shareXFiles([XFile(file.path)]);
                      },
                      icon: Icon(Icons.share, color: Colors.black)),
                  IconButton(
                      onPressed: () async {
                        printImage(await captureImage(context, _containerKey));
                      },
                      icon: Icon(Icons.print, color: Colors.black))
                ],
              )),
          backgroundColor: Constant.bgColor,
          body: SingleChildScrollView(
              child: RepaintBoundary(
            key: _containerKey,
            child: Container(
              color: Constant.bgColor,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClientsDetailsContainer(clientData: widget.clientData),
                      SizedBox(height: 10),
                      // Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      //   IconButton(
                      //       onPressed: () {
                      //         setState(() {
                      //           if (pageIndex >= 0) {
                      //             setState(() {
                      //               pageIndex--;
                      //               controller.jumpToPage(pageIndex);
                      //             });
                      //           }
                      //         });
                      //       },
                      //       icon:
                      //           Icon(Icons.arrow_back_ios, color: Constant.textColor)),
                      //   BoldText(text: "Medicines for 23rd Jan."),
                      //   IconButton(
                      //       onPressed: () {
                      //         setState(() {
                      //           if (pageIndex < pageView.length) {
                      //             setState(() {
                      //               pageIndex++;
                      //               controller.jumpToPage(pageIndex);
                      //             });
                      //           }
                      //         });
                      //       },
                      //       icon: Icon(Icons.arrow_forward_ios,
                      //           color: Constant.textColor))
                      // ]),

                      // Container(
                      //     height: screenHeight / 1.5,
                      //     child: PageView(
                      //         children: pageView,
                      //         scrollDirection: Axis.horizontal,
                      //         physics: BouncingScrollPhysics(),
                      //         controller: controller,
                      //         onPageChanged: (num) {
                      //           setState(() {
                      //             pageIndex = num;
                      //           });
                      //         })),

                      // Padding(
                      //   padding: EdgeInsets.symmetric(vertical: 5),
                      //   child: ButttonWithIconWidget(
                      //       align: MainAxisAlignment.center,
                      //       width: screenWidth,
                      //       onTap: () {},
                      //       icon: Icons.add,
                      //       text: "Add new medicine"),
                      // ),
                      // Padding(
                      //     padding: EdgeInsets.symmetric(vertical: 10),
                      //     child: Container(
                      //         width: screenWidth,
                      //         decoration: BoxDecoration(
                      //             borderRadius: BorderRadius.circular(15),
                      //             color: Colors.white),
                      //         child: Padding(
                      //             padding: EdgeInsets.symmetric(
                      //                 horizontal: 15, vertical: 10),
                      //             child: Column(children: [
                      //               LabelText(
                      //                   label: "Notes",
                      //                   widget: IconButton(
                      //                       onPressed: () {
                      //                         // Navigator.push(
                      //                         //     context,
                      //                         //     MaterialPageRoute(
                      //                         //         builder: (context) =>
                      //                         //             NotesScreen()));
                      //                       },
                      //                       icon: Icon(
                      //                           Icons.remove_red_eye_outlined,
                      //                           color: Constant.textColor))),
                      //               SizedBox(height: 5),
                      //               ListView.separated(
                      //                   separatorBuilder: (context, index) =>
                      //                       Divider(height: 30),
                      //                   shrinkWrap: true,
                      //                   physics: NeverScrollableScrollPhysics(),
                      //                   itemCount: 2,
                      //                   padding: EdgeInsets.zero,
                      //                   itemBuilder:
                      //                       (BuildContext context, int i) {
                      //                     var list = _vm.notes[i];
                      //                     return NoteCont(
                      //                         numb: "${i + 1}",
                      //                         notes: list["notes"]!);
                      //                   })
                      //             ])))),

                      Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: LabelText(label: "Diagnosis")),

                      Container(
                          width: screenWidth,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                          child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              child: Text(
                                  "${widget.clientData.additionalMiscInformation ?? "No information found"}"))),

                      DoctorInfo(doctors: widget.clientData.doctor ?? []),
                      SizedBox(height: 20),
                      Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: LabelText(label: "Documents")),
                      Container(
                          height: 230,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: widget.clientData.documents?.length,
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              itemBuilder: (BuildContext context, int i) {
                                String item = widget.clientData.documents![i];
                                return !item.split('/').last.contains(".")
                                    ? SizedBox()
                                    : PDFDoc(
                                        name: item,
                                        onTap: () async {
                                          print("${DOC_PATH}${item}");
                                          await downloadOpenFile(
                                              url: "${DOC_PATH}${item}",
                                              fileName: item);
                                        });
                              })),

                      BoldText(text: "Archived Medicines"),

                      SizedBox(height: 20),
                      SizedBox(height: 10),

                      loading
                          ? Center(child: CircularProgressIndicator())
                          : Container(
                              width: double.infinity,
                              child: DataTable(
                                showBottomBorder: true,
                                headingRowColor: MaterialStateProperty.all(
                                    Constant.primaryColor),
                                headingTextStyle: const TextStyle(
                                    color: Colors.white, fontSize: 14),
                                columns: [
                                  DataColumn(label: Text('Name')),
                                  DataColumn(label: Text('Start Date')),
                                  DataColumn(label: Text('End Date')),
                                ],
                                rows: [
                                  ...medicineList
                                      .map((item) => DataRow(cells: [
                                            DataCell(
                                                Text('${item.medicineName}')),
                                            DataCell(Text('${item.startDate}')),
                                            DataCell(Text('${item.endDate}')),
                                          ]))
                                      .toList()
                                ],
                              ),
                            ),

                      // loading
                      //     ? Center(child: CircularProgressIndicator())
                      //     : ListView.builder(
                      //         shrinkWrap: true,
                      //         physics: NeverScrollableScrollPhysics(),
                      //         itemCount: medicineList.length,
                      //         padding: EdgeInsets.zero,
                      //         itemBuilder: (BuildContext context, int i) {
                      //           ArchivedMedicationData item = medicineList[i];
                      //           return Padding(
                      //               padding: const EdgeInsets.all(8.0),
                      //               child: ListTile(
                      //                   shape: RoundedRectangleBorder(
                      //                       borderRadius: BorderRadius.all(
                      //                           Radius.circular(10))),
                      //                   tileColor: Colors.white,
                      //                   title: Text("${item.medicineName}",
                      //                       textAlign: TextAlign.center)));
                      //         }),

                      SizedBox(height: 20),
                      // Padding(
                      //     padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                      //     child: ButttonWithIconWidget(
                      //         icon: Icons.add,
                      //         align: MainAxisAlignment.center,
                      //         width: screenWidth,
                      //         onTap: () {
                      //           Navigator.push(
                      //               context,
                      //               MaterialPageRoute(
                      //                   builder: (context) => AddDocScreen()));
                      //         },
                      //         text: "Add new Documents"))
                    ]),
              ),
            ),
          )));
    });
  }
}

class Pages extends StatelessWidget {
  final text;
  Pages({this.text});
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [BoldText(text: text, textSize: 30)]));
  }
}
