import 'package:cat/src/core/common/widget/commonCont.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/client.dart';
import 'package:cat/src/feature/home/ui/clients/edit_client/editClient.dart';
import 'package:flutter/material.dart';

class ClientsDetailsContainer extends StatelessWidget {
  final ClientData clientData;

  const ClientsDetailsContainer({super.key, required this.clientData});
  @override
  Widget build(BuildContext context) {
    return CommonContainer(
        image: AssetImage(AppAsset.roundEffect2),
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        BoldText(
                            text:
                                "${clientData.firstName} ${clientData.lastName}",
                            textColor: Colors.white),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => EditClientScreen(
                                          clientData: clientData)));
                            },
                            icon:
                                Icon(Icons.edit_outlined, color: Colors.white))
                      ]),
                  SizedBox(height: 15),
                  Divider(color: Colors.white),
                  SizedBox(height: 15),
                  CommonRichText(
                      text1: "Date of birth: ", text2: "${clientData.dob}"),
                  CommonRichText(
                      text1: "Gender: ", text2: "${clientData.gender}"),
                  for (Guardian i in clientData.guardian ?? [])
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CommonRichText(
                            text1: "Guardian name: ",
                            text2: "${i.guardianName}"),
                        CommonRichText(
                            text1: "Guardian number: ",
                            text2: "${i.guardianPhoneNumber}"),
                        CommonRichText(
                            text1: "Guardian address: ",
                            text2: "${i.guardianAddress}"),
                      ],
                    )
                ])));
  }
}
