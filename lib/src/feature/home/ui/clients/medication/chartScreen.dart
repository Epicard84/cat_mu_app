import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/medication/addNewDoc.dart';
import 'package:flutter/material.dart';

class ChartScreen extends StatefulWidget {
  const ChartScreen({super.key});

  @override
  State<ChartScreen> createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  List img = [
    AppAsset.img1,
    AppAsset.img2,
    AppAsset.img3,
    AppAsset.img2,
    AppAsset.img3,
  ];
  final PageController controller = PageController();
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(title: "Chart"),
        body: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(children: [
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset(AppAsset.calender,
                                    height: 20),
                                SizedBox(width: 5),
                                Text("01/02/2023, 12:00",
                                    style:
                                        TextStyle(color: Colors.grey))
                              ],
                            ),
                            Container(
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Constant.primaryColor),
                                  shape: BoxShape.circle),
                              child: const Text("HE",
                                  style: TextStyle(
                                      color: Constant.primaryColor)),
                            ),
                          ],
                        ),
                        SizedBox(height: 15),
                        Text(
                          "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                          style: TextStyle(
                              height: 1.5, color: Color(0xff545C7A)),
                        ),
                        SizedBox(height: 15),
                      ],
                    ),
                  )),
              SizedBox(height: 20),
              Container(
                alignment: Alignment.center,
                width: size.width,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15)),
                child: Wrap(spacing: 5, runSpacing: 0.0, children: [
                  ...img
                      .map((v) => GestureDetector(
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return StatefulBuilder(
                                      builder: (context, setState2) {
                                    setPageIndex(int index) {
                                      print("index >> $index");
                                      pageIndex = index;
                                      controller
                                          .jumpToPage(pageIndex);
                                      setState2(() {});
                                    }

                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(
                                                  10)),
                                      content: Container(
                                          height: 300,
                                          width: 300,
                                          child: PageView.builder(
                                            controller: controller,
                                            onPageChanged: (i) {
                                              setPageIndex(i);
                                            },
                                            itemCount: img.length,
                                            scrollDirection:
                                                Axis.horizontal,
                                            itemBuilder:
                                                (context, index) {
                                              return Container(
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        image: Image.asset(
                                                                img[
                                                                    index],
                                                                height:
                                                                    200)
                                                            .image)),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(
                                                          Icons
                                                              .arrow_back_ios,
                                                          color: Colors
                                                              .white),
                                                      onPressed: () {
                                                        if (pageIndex >=
                                                            0) {
                                                          setPageIndex(
                                                              --pageIndex);
                                                        }
                                                      },
                                                    ),
                                                    IconButton(
                                                      icon: Icon(
                                                          Icons
                                                              .arrow_forward_ios,
                                                          color: Colors
                                                              .white),
                                                      onPressed: () {
                                                        setPageIndex(
                                                            ++pageIndex);
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                          )),
                                    );
                                  });
                                },
                              );
                            },
                            child: Container(
                                margin: EdgeInsets.all(2),
                                height: 80,
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.circular(10),
                                    child: Image.asset(v,
                                        fit: BoxFit.cover))),
                          ))
                      .toList(),
                  Padding(
                      padding: EdgeInsets.all(5),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      AddDocScreen()));
                        },
                        child: Container(
                            height: 80,
                            width: 80,
                            decoration: BoxDecoration(
                                color: Constant.bgColor,
                                borderRadius:
                                    BorderRadius.circular(7)),
                            child: Icon(Icons.add,
                                color: Constant.primaryColor)),
                      ))
                ]),
              ),
            ])));
  }
}
