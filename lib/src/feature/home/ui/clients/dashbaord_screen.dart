import 'dart:convert';
import 'dart:io';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/clientList/clientListScreen.dart';
import 'package:cat/src/feature/home/ui/mfd/mfdListScreen.dart';
import 'package:cat/src/feature/home/ui/notification/notificationScreen.dart';
import 'package:cat/src/feature/home/ui/reminder/reminderModel.dart';
import 'package:cat/src/feature/home/ui/reminder/reminderScreen.dart';
import 'package:cat/src/feature/home/ui/subuser/subuserScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:upgrader/upgrader.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreen();
}

class _DashboardScreen extends State<DashboardScreen> {
  final client = HTTPClient();
  DateTime timeNow = DateTime.now();

  @override
  void initState() {
    super.initState();
    onInit();
  }

  onInit() async {
    try {
      var res = await client.getWithToken("${APIEndpoint.getTime}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          timeNow = DateTime.parse(jsonResp['message']);
        }
      }
      var res1 = await await client.getWithToken(
          "${APIEndpoint.reminderList}/${timeNow.year}-${timeNow.month}-${timeNow.day}");
      if (res1.statusCode == 200) {
        var jsonResp = jsonDecode(res1.body);
        if (jsonResp['success']) {
          ReminderResp resp = ReminderResp.fromJson(jsonResp);
          List<int> rem = [];
          for (ReminderData i in resp.data ?? []) {
            rem.add(i.status!);
          }
          if (rem.contains(0)) {
            // if (!showReminder) {
            //   LocalNotification().showNotification(
            //       title: "Reminder", body: "You have reminder");
            // }
            showReminder = true;
          }
          if (Platform.isIOS) {
            if (rem.contains(0)) {
              showReminder = true;
              FlutterAppBadger.updateBadgeCount(1);
            } else {
              showReminder = false;
              FlutterAppBadger.removeBadge();
            }
          }
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return UpgradeAlert(
      upgrader: Upgrader(
          canDismissDialog: false, dialogStyle: UpgradeDialogStyle.cupertino),
      child: Scaffold(
          body: ClientListScreen(),
          bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: Stack(
                      children: [
                        Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Image.asset(AppAsset.reminder,
                                scale: 2.5, color: Constant.nevIconColor)),
                        !showReminder
                            ? SizedBox()
                            : Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  height: 12,
                                  width: 12,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.red),
                                  alignment: Alignment.center,
                                ),
                              )
                      ],
                    ),
                    label: 'Reminder'),
                BottomNavigationBarItem(
                    icon: Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Image.asset(AppAsset.subUsers,
                            scale: 2.5, color: Constant.nevIconColor)),
                    label: 'Sub users'),
                BottomNavigationBarItem(
                    icon: Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Image.asset(AppAsset.mfd,
                            scale: 2.5, color: Constant.nevIconColor)),
                    label: 'Monthly fire drill'),
                BottomNavigationBarItem(
                    icon: Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Image.asset(AppAsset.notification,
                            scale: 2.5, color: Constant.nevIconColor)),
                    label: 'Notification')
              ],
              // currentIndex: _selectedIndex,
              unselectedItemColor: Constant.nevIconColor,
              selectedItemColor: Constant.nevIconColor,
              selectedLabelStyle: TextStyle(
                  height: 2, fontSize: 12, fontWeight: FontWeight.bold),
              unselectedLabelStyle: TextStyle(
                  height: 2, fontSize: 12, fontWeight: FontWeight.bold),
              onTap: (i) {
                i == 0
                    ? Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ReminderScreen()))
                    : i == 1
                        ? Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SubuserScreen()))
                        : i == 2
                            ? Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MFDListScreen()))
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        NotificationScreen()));
              })),
    );
  }
}


//!BACKUP

// import 'dart:convert';
// import 'package:cat/src/core/common/client/APIhelper.dart';
// import 'package:cat/src/core/constant/constant.dart';
// import 'package:cat/src/feature/home/ui/clients/clientList/clientListScreen.dart';
// import 'package:cat/src/feature/home/ui/mfd/mfdListScreen.dart';
// import 'package:cat/src/feature/home/ui/notification/notificationScreen.dart';
// import 'package:cat/src/feature/home/ui/reminder/reminderModel.dart';
// import 'package:cat/src/feature/home/ui/reminder/reminderScreen.dart';
// import 'package:cat/src/feature/home/ui/subuser/subuserScreen.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_app_badger/flutter_app_badger.dart';

// class DashboardScreen extends StatefulWidget {
//   const DashboardScreen({super.key});

//   @override
//   State<DashboardScreen> createState() => _DashboardScreen();
// }

// class _DashboardScreen extends State<DashboardScreen> {
//   final client = HTTPClient();
//   DateTime timeNow = DateTime.now();

//   @override
//   void initState() {
//     super.initState();
//     onInit();
//   }

//   onInit() async {
//     try {
//       var res = await client.getWithToken("${APIEndpoint.getTime}");
//       if (res.statusCode == 200) {
//         var jsonResp = jsonDecode(res.body);
//         if (jsonResp['success']) {
//           timeNow = DateTime.parse(jsonResp['message']);
//         }
//       }
//       var res1 = await await client.getWithToken(
//           "${APIEndpoint.reminderList}/${timeNow.year}-${timeNow.month}-${timeNow.day}");
//       if (res1.statusCode == 200) {
//         var jsonResp = jsonDecode(res1.body);
//         if (jsonResp['success']) {
//           ReminderResp resp = ReminderResp.fromJson(jsonResp);
//           List<int> rem = [];
//           for (ReminderData i in resp.data ?? []) {
//             rem.add(i.status!);
//           }

//           bool res = await FlutterAppBadger.isAppBadgeSupported();
//           if (res) {
//             print("Supported");
//           } else {
//             print("Not Supported");
//           }

//           if (rem.contains(0)) {
//             showReminder = true;
//             FlutterAppBadger.updateBadgeCount(1);
//           } else {
//             showReminder = false;
//             FlutterAppBadger.removeBadge();
//           }
//         }
//       }
//     } catch (e) {
//       print("error >> $e");
//     } finally {
//       setState(() {});
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: ClientListScreen(),
//         bottomNavigationBar: BottomNavigationBar(
//             type: BottomNavigationBarType.fixed,
//             items: [
//               BottomNavigationBarItem(
//                   icon: Stack(
//                     children: [
//                       Padding(
//                           padding: const EdgeInsets.only(top: 10),
//                           child: Image.asset(AppAsset.reminder,
//                               scale: 2.5, color: Constant.nevIconColor)),
//                       !showReminder
//                           ? SizedBox()
//                           : Positioned(
//                               top: 0,
//                               right: 0,
//                               child: Container(
//                                 height: 12,
//                                 width: 12,
//                                 decoration: BoxDecoration(
//                                     shape: BoxShape.circle, color: Colors.red),
//                                 alignment: Alignment.center,
//                               ),
//                             )
//                     ],
//                   ),
//                   label: 'Reminder'),
//               BottomNavigationBarItem(
//                   icon: Padding(
//                       padding: const EdgeInsets.only(top: 10),
//                       child: Image.asset(AppAsset.subUsers,
//                           scale: 2.5, color: Constant.nevIconColor)),
//                   label: 'Sub users'),
//               BottomNavigationBarItem(
//                   icon: Padding(
//                       padding: const EdgeInsets.only(top: 10),
//                       child: Image.asset(AppAsset.mfd,
//                           scale: 2.5, color: Constant.nevIconColor)),
//                   label: 'Monthly fire drill'),
//               BottomNavigationBarItem(
//                   icon: Padding(
//                       padding: const EdgeInsets.only(top: 10),
//                       child: Image.asset(AppAsset.notification,
//                           scale: 2.5, color: Constant.nevIconColor)),
//                   label: 'Notification')
//             ],
//             // currentIndex: _selectedIndex,
//             unselectedItemColor: Constant.nevIconColor,
//             selectedItemColor: Constant.nevIconColor,
//             selectedLabelStyle:
//                 TextStyle(height: 2, fontSize: 12, fontWeight: FontWeight.bold),
//             unselectedLabelStyle:
//                 TextStyle(height: 2, fontSize: 12, fontWeight: FontWeight.bold),
//             onTap: (i) {
//               i == 0
//                   ? Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => ReminderScreen()))
//                   : i == 1
//                       ? Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => SubuserScreen()))
//                       : i == 2
//                           ? Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => MFDListScreen()))
//                           : Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => NotificationScreen()));
//             }));
//   }
// }
