import 'package:flutter/material.dart';

class CommonTextField extends StatelessWidget {
  final String title;
  final TextEditingController controller;
  final Widget suffixIcon;
  final bool obscureText;
  const CommonTextField(
      {super.key,
      required this.title,
      required this.controller,
      this.suffixIcon = const SizedBox(),
      this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return TextField(
        autofocus: false,
        controller: controller,
        obscureText: obscureText,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.all(17),
            hintText: title,
            hintStyle: TextStyle(color: Colors.black),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
            suffixIcon: suffixIcon));
  }
}
