import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonCont.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/photo_view.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/client.dart';
import 'package:cat/src/feature/home/model/subUser.dart';
import 'package:cat/src/feature/home/ui/clients/add_client/add_client_screen.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
import 'package:cat/src/feature/home/ui/clients/medication/archived_client.dart';
import 'package:cat/src/feature/home/ui/profile/editProfile.dart';
import 'package:cat/src/feature/home/ui/profile/settingScreen.dart';
import 'package:cat/src/feature/home/ui/subuser/addSubuser.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MasterProfileScreen extends StatefulWidget {
  const MasterProfileScreen({super.key});

  @override
  State<MasterProfileScreen> createState() => _MasterProfileScreenState();
}

class _MasterProfileScreenState extends State<MasterProfileScreen> {
  List<ClientData> clientList = [];
  List<SubUserData> subuserList = [];
  bool loading = false;
  bool loading2 = false;

  final client = HTTPClient();

  void initState() {
    super.initState();
    getArchived();
    getArchivedSubuser();
  }

  getArchived() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(APIEndpoint.archiveList);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          ClientResp resp = ClientResp.fromJson(jsonResp);
          clientList = resp.data ?? [];
        }
      }
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  getArchivedSubuser() async {
    try {
      setState(() {
        loading2 = true;
      });
      var res = await await client.getWithToken(APIEndpoint.archiveSubuser);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          SubUserResp resp = SubUserResp.fromJson(jsonResp);
          subuserList = resp.data ?? [];
        }
      }
    } finally {
      setState(() {
        loading2 = false;
      });
    }
  }

  restoreClient({required String clientId}) async {
    try {
      setState(() {
        loading = true;
      });
      var res =
          await client.getWithToken("${APIEndpoint.restoreClient}/${clientId}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: jsonResp['message'], success: true);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => DashboardScreen()),
            (Route<dynamic> route) => false);
      }
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  restoreSubuser({required String subuserId}) async {
    try {
      setState(() {
        loading2 = true;
      });
      var res = await client
          .getWithToken("${APIEndpoint.restoreSubuser}/${subuserId}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: jsonResp['message'], success: true);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => DashboardScreen()),
            (Route<dynamic> route) => false);
      }
    } finally {
      setState(() {
        loading2 = false;
      });
    }
  }

  removeSubuser({required String subuserId}) async {
    try {
      setState(() {
        loading2 = true;
      });
      var res = await client
          .getWithToken("${APIEndpoint.removeSubUser}/${subuserId}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: jsonResp['message'], success: true);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => DashboardScreen()),
            (Route<dynamic> route) => false);
      }
    } finally {
      setState(() {
        loading2 = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Consumer(builder: (context, ref, child) {
      return Scaffold(
          backgroundColor: Constant.bgColor,
          appBar: CommonAppBar(
            title: "Master user",
            isBack: true,
            actions: [
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SettingScreen()));
                    // SharedPreferences prefs =
                    //     await SharedPreferences.getInstance();
                    // await prefs.remove("authToken");
                    // await Navigator.pushAndRemoveUntil(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (BuildContext context) => LoginScreen()),
                    //     (Route<dynamic> route) => false);
                  },
                  icon: Icon(Icons.settings, color: Colors.black))
            ],
          ),
          body: SingleChildScrollView(
              child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Column(children: [
              CommonContainer(
                  image: AssetImage(AppAsset.roundEffect),
                  child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                      child: Column(
                        children: [
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    EditProfile()));
                                      },
                                      child: CircleAvatar(
                                        radius: 40,
                                        backgroundColor: Constant.bgColor,
                                        backgroundImage: (GlobalVar.userData
                                                    .profileImageName !=
                                                null)
                                            ? Image.network(
                                                    "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.profileImageName}")
                                                .image
                                            : Image.asset(AppAsset.dp).image,
                                        child: Stack(children: [
                                          Align(
                                            alignment: Alignment.bottomRight,
                                            child: CircleAvatar(
                                              radius: 15,
                                              backgroundColor: Colors.white,
                                              child: Icon(Icons.add,
                                                  color: Constant.primaryColor),
                                            ),
                                          ),
                                        ]),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          BoldText(
                                              text:
                                                  "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                              textColor: Colors.white),
                                          SubText(
                                              text: "${GlobalVar.cityName}",
                                              textColor: Colors.white),
                                        ]),
                                  ],
                                ),
                                IconButton(
                                    onPressed: () async {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EditProfile()));
                                    },
                                    icon: Icon(Icons.edit_outlined,
                                        color: Colors.white))
                              ]),
                          SizedBox(height: 10),
                          Row(
                            children: [
                              Text("Status:",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 17)),
                              SizedBox(width: 30),
                              Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white.withOpacity(.5),
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Row(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 15.0),
                                          child: Text("Online",
                                              style: TextStyle(fontSize: 17)),
                                        ),
                                      ),
                                      Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 20.0),
                                          child: Text("Offline",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17))),
                                    ],
                                  ))
                            ],
                          )
                        ],
                      ))),
              SizedBox(height: 20),
              Container(
                  width: size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white),
                  child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CommonRichText(
                                maxLines: 3,
                                text1: "Phone number:  ",
                                text2: "${GlobalVar.userData.phoneNumber}",
                                textColor: Colors.black),
                            SizedBox(height: 5),
                            CommonRichText(
                                maxLines: 3,
                                text1: "Email address:  ",
                                text2: "${GlobalVar.userData.email}",
                                textColor: Colors.black),
                            SizedBox(height: 5),
                            // CommonRichText(
                            //     maxLines: 2,
                            //     text1: "Address: ",
                            //     text2: "${GlobalVar.userData.permanentAddress}",
                            //     textColor: Colors.black),
                            // SizedBox(height: 5),
                            CommonRichText(
                                text1: "Facility address: ",
                                maxLines: 5,
                                text2: "${GlobalVar.userData.facilityAddress}",
                                textColor: Colors.black),
                            SizedBox(height: 5),
                            // CommonRichText(
                            //     text1: "City: ",
                            //     text2: "${GlobalVar.userData.city}",
                            //     textColor: Colors.black),
                            // SpizedBox(height: 5),
                            // CommonRichText(
                            //     text1: "State:  ",
                            //     text2: "${GlobalVar.userData.state}",
                            //     textColor: Colors.black),
                            // SizedBox(height: 5),
                            // CommonRichText(
                            //     text1: "Country:  ",
                            //     text2: "${GlobalVar.userData.country}",
                            //     textColor: Colors.black),
                            // SizedBox(height: 5),
                            // CommonRichText(
                            //     text1: "Zip Code:  ",
                            //     text2: "${GlobalVar.userData.zipCode}",
                            //     textColor: Colors.black),
                            Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: LabelText(label: "Documents")),

                            Text("Operators License"),
                            SizedBox(height: 5),
                            GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ViewPhoto(
                                              image:
                                                  "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.operatorsLicense}")));
                                },
                                child: Container(
                                  height: 150,
                                  color: Colors.transparent,
                                  child: Image.network(
                                      "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.operatorsLicense}"),
                                )),

                            SizedBox(height: 15),
                            Text("TB Clearance"),
                            SizedBox(height: 5),
                            GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ViewPhoto(
                                              image:
                                                  "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.tbClearance}")));
                                },
                                child: Container(
                                  height: 150,
                                  color: Colors.transparent,
                                  child: Image.network(
                                      "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.tbClearance}"),
                                )),

                            SizedBox(height: 15),
                            Text("Physical Checkup"),
                            SizedBox(height: 5),
                            GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ViewPhoto(
                                              image:
                                                  "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.physicalCheckup}")));
                                },
                                child: Container(
                                  height: 150,
                                  color: Colors.transparent,
                                  child: Image.network(
                                      "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.physicalCheckup}"),
                                )),

                            SizedBox(height: 15),
                            Text("Miscellaneous Doc"),
                            SizedBox(height: 5),
                            GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ViewPhoto(
                                              image:
                                                  "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.miscDoc}")));
                                },
                                child: Container(
                                  height: 150,
                                  color: Colors.transparent,
                                  child: Image.network(
                                      "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.miscDoc}"),
                                )),
                          ]))),
              SizedBox(height: 20),
              CommonButtonWidget(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddSubuserScreen()));
                  },
                  text: "Add Sub user"),
              SizedBox(height: 20),
              CommonButtonWidget(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddClientScreen()));
                  },
                  text: "Add client"),
              SizedBox(height: 20),
              BoldText(text: "Archived clients"),
              SizedBox(height: 10),
              loading
                  ? Center(child: CircularProgressIndicator())
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: clientList.length,
                      padding: EdgeInsets.zero,
                      itemBuilder: (BuildContext context, int i) {
                        ClientData user = clientList[i];
                        return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListTile(
                                trailing: IconButton(
                                    onPressed: () {
                                      restoreClient(clientId: "${user.id}");
                                    },
                                    icon: Icon(Icons.restore)),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ArchivedMedicationScreen(
                                                  clientData: user)));
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                tileColor: Colors.white,
                                title: Text(
                                    "${user.firstName} ${user.lastName}",
                                    textAlign: TextAlign.center)));
                      }),
              SizedBox(height: 20),
              BoldText(text: "Archived Subuser"),
              SizedBox(height: 10),
              loading2
                  ? Center(child: CircularProgressIndicator())
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: subuserList.length,
                      padding: EdgeInsets.zero,
                      itemBuilder: (BuildContext context, int i) {
                        SubUserData user = subuserList[i];
                        return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListTile(
                                trailing: Container(
                                  width: 100,
                                  child: Row(
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            restoreSubuser(
                                                subuserId: "${user.id}");
                                          },
                                          icon: Icon(Icons.restore)),
                                      IconButton(
                                          onPressed: () {
                                            removeSubuser(
                                                subuserId: "${user.id}");
                                          },
                                          icon: Icon(Icons.delete)),
                                    ],
                                  ),
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                tileColor: Colors.white,
                                title: Text(
                                    "${user.firstName} ${user.lastName}",
                                    textAlign: TextAlign.center)));
                      }),
            ]),
          )));
    });
  }
}
