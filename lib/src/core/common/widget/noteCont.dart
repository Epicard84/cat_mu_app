import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class NoteCont extends StatelessWidget {
  final String numb;
  final String notes;
  const NoteCont({super.key, required this.numb, required this.notes});

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Container(
              height: 25,
              width: 25,
              decoration: BoxDecoration(
                  color: Constant.bgColor,
                  borderRadius: BorderRadius.circular(7)),
              child: Center(
                  child:
                      SubText(text: numb, textColor: Constant.primaryColor)))),
      SizedBox(width: 10),
      Container(
          width: screenWidth - 110,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [SubText(text: notes, textAlign: TextAlign.start)]))
    ]);
  }
}
