import 'package:cat/src/core/common/widget/commonCont.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class TopContainer extends StatelessWidget {
  const TopContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return CommonContainer(
        height: 90,
        image: AssetImage(AppAsset.roundEffect),
        child: Padding(
            padding: EdgeInsets.all(15),
            child: Row(children: [
              BoldText(text: "163", textColor: Constant.textColor3),
              SizedBox(width: 10),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    BoldText(text: "Clients", textColor: Colors.white),
                    SubText(
                        text: "are listed right now", textColor: Colors.white)
                  ])
            ])));
  }
}
