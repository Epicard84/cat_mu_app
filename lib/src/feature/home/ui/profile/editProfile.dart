import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/auth/model/loginResp.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
import 'package:cat/src/feature/home/ui/profile/editProfileVm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
// import 'package:latlong2/latlong.dart'  as latLng;
import 'package:google_maps_flutter/google_maps_flutter.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({super.key});

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();

  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController companyName = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController mailingAddress = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController state = TextEditingController();
  TextEditingController zipCode = TextEditingController();
  TextEditingController country = TextEditingController();

  bool loading = false;
  final client = HTTPClient();

  void initState() {
    super.initState();

    firstName.text = GlobalVar.userData.firstName ?? "";
    lastName.text = GlobalVar.userData.lastName ?? "";
    companyName.text = GlobalVar.userData.companyName ?? "";
    phone.text = "${GlobalVar.userData.phoneNumber}";
    email.text = GlobalVar.userData.email ?? "";
    address.text = GlobalVar.userData.permanentAddress ?? "";
    mailingAddress.text = GlobalVar.userData.facilityAddress ?? "";
    city.text = GlobalVar.userData.city ?? "";
    state.text = GlobalVar.userData.state ?? "";
    zipCode.text = GlobalVar.userData.zipCode ?? "";
    country.text = GlobalVar.userData.country ?? "";

    setState(() {});
  }

  editMasterUser(EditProfileVM _vm) async {
    try {
      setState(() {
        loading = true;
      });

      var data = {
        "id": "${GlobalVar.userData.id}",
        "first_name": firstName.text,
        "last_name": lastName.text,
        "company_name": companyName.text,
        "email": email.text,
        "phone_number": phone.text,
        // "permanent_address": address.text,
        "facility_address": mailingAddress.text,
        "city": city.text,
        "state": state.text,
        "zip_code": zipCode.text,
        "country": country.text,
      };
      print("data >> ${data}");
      print(_vm.file != null);

      http.StreamedResponse res = await await client.post5FileWithToken(
        APIEndpoint.editMasterUser,
        data,
        fileKey: "image",
        files: _vm.file != null ? [_vm.file!] : [],
        fileKey2: "operatorLicense",
        files2:
            _vm.operatorsLicensefile != null ? [_vm.operatorsLicensefile!] : [],
        fileKey3: "tbClearance",
        files3: _vm.clearancefile != null ? [_vm.clearancefile!] : [],
        fileKey4: "physicalCheckup",
        files4:
            _vm.physicalCheckupfile != null ? [_vm.physicalCheckupfile!] : [],
        fileKey5: "misc_doc",
        files5: _vm.miscDocsFile != null ? [_vm.miscDocsFile!] : [],
      );
      var respp = await res.stream.bytesToString();
      print(respp);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        print(respp);
        if (jsonResp['success']) {
          GlobalVar.userData = UserModel.fromJson(jsonResp['data']);
          // showSnackBar(context, text: "${jsonResp['message']}", success: true);
          await Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DashboardScreen()),
              (Route<dynamic> route) => false);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(respp);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      final _vm = ref.watch(editProfileVm);
      return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: CommonAppBar(title: "Edit master user"),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height * 2),
                  child: FormBuilder(
                      key: formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: GestureDetector(
                                  onTap: () {
                                    _vm.mediaOption(context);
                                  },
                                  child: CircleAvatar(
                                    radius: 40,
                                    backgroundColor: Constant.bgColor,
                                    backgroundImage: _vm.file != null
                                        ? Image.file(_vm.file!).image
                                        : (GlobalVar.userData
                                                    .profileImageName !=
                                                null)
                                            ? Image.network(
                                                    "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.profileImageName}")
                                                .image
                                            : Image.asset(AppAsset.dp).image,
                                    child: Stack(children: [
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: CircleAvatar(
                                          radius: 15,
                                          backgroundColor: Colors.white,
                                          child: Icon(Icons.add,
                                              color: Constant.primaryColor),
                                        ),
                                      ),
                                    ]),
                                  )),
                            ),

                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: firstName,
                                hintText: "First name",
                                name: "First name",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: lastName,
                                hintText: "Last name",
                                name: "Last name",
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required()])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: companyName,
                                hintText: "Company name",
                                name: "Company name",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: phone,
                                hintText: "Phone number",
                                name: "Phone number",
                                keyboardType: TextInputType.number,
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: email,
                                hintText: "Email address",
                                name: "Email address",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     maxLine: 4,
                            //     keyboardType: TextInputType.multiline,
                            //     controller: address,
                            //     hintText: "Address",
                            //     name: "Address",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            SizedBox(height: 20),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return PlacePicker(
                                        apiKey: Constant.mapKey,
                                        hintText: "Find a place ...",
                                        searchingText: "Please wait ...",
                                        selectText: "Select place",
                                        outsideOfPickAreaText:
                                            "Place not in area",
                                        initialPosition: LatLng(0, 0),
                                        useCurrentLocation: true,
                                        selectInitialPosition: true,
                                        usePinPointingSearch: true,
                                        usePlaceDetailSearch: true,
                                        onPlacePicked: (PickResult result) {
                                          PickResult selectedPlace = result;

                                          mailingAddress.text =
                                              selectedPlace.formattedAddress ??
                                                  "";

                                          // for (var i in selectedPlace
                                          //     .addressComponents!) {
                                          //   // log("place >> ${i.toJson()}");
                                          //   bool compareAddress =
                                          //       i.types.length ==
                                          //               [
                                          //                 "sublocality_level_2",
                                          //                 "sublocality",
                                          //                 "political"
                                          //               ].length &&
                                          //           i.types.every((element) => [
                                          //                 "sublocality_level_2",
                                          //                 "sublocality",
                                          //                 "political"
                                          //               ].contains(element));

                                          //   bool compareCity = i.types.length ==
                                          //           ["locality", "political"]
                                          //               .length &&
                                          //       i.types.every((element) => [
                                          //             "locality",
                                          //             "political"
                                          //           ].contains(element));

                                          //   bool compareCountry =
                                          //       i.types.length ==
                                          //               ["country", "political"]
                                          //                   .length &&
                                          //           i.types.every((element) => [
                                          //                 "country",
                                          //                 "political"
                                          //               ].contains(element));

                                          //   bool compareState =
                                          //       i.types.length ==
                                          //               [
                                          //                 "administrative_area_level_1",
                                          //                 "political"
                                          //               ].length &&
                                          //           i.types.every((element) => [
                                          //                 "administrative_area_level_1",
                                          //                 "political"
                                          //               ].contains(element));

                                          //   bool comparePostal = i
                                          //               .types.length ==
                                          //           ["postal_code"].length &&
                                          //       i.types.every((element) => [
                                          //             "postal_code",
                                          //           ].contains(element));
                                          //   if (compareAddress) {
                                          //     mailingAddress.text = i.longName;
                                          //   } else if (compareCity) {
                                          //     city.text = i.longName;
                                          //   } else if (compareCountry) {
                                          //     country.text = i.longName;
                                          //   } else if (compareState) {
                                          //     state.text = i.longName;
                                          //   } else if (comparePostal) {
                                          //     zipCode.text = i.longName;
                                          //   }
                                          // }

                                          Navigator.of(context).pop();

                                          setState(() {});
                                        },
                                      );
                                    },
                                  ),
                                );
                              },
                              child: CommonTextFiled(
                                  isEnabled: false,
                                  maxLine: 4,
                                  keyboardType: TextInputType.multiline,
                                  controller: mailingAddress,
                                  hintText: "Facility address",
                                  name: "Facility address",
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(),
                                  ])),
                            ),
                            SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: city,
                            //     hintText: "City",
                            //     name: "City",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: state,
                            //     hintText: "State",
                            //     name: "State",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: zipCode,
                            //     hintText: "Zip Code",
                            //     name: "Zip Code",
                            //     keyboardType: TextInputType.number,
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: country,
                            //     hintText: "Country",
                            //     name: "Country",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),

                            Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: LabelText(label: "Documents")),
                            Text("Operators License"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(0);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.operatorsLicensefile != null
                                        ? Image.file(_vm.operatorsLicensefile!,
                                            height: 100)
                                        : (GlobalVar.userData
                                                    .operatorsLicense !=
                                                null)
                                            ? Image.network(
                                                "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.operatorsLicense}",
                                                height: 100)
                                            : Image.asset(AppAsset.pdf,
                                                height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),
                            SizedBox(height: 15),
                            Text("TB Clearance"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(1);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.clearancefile != null
                                        ? Image.file(_vm.clearancefile!,
                                            height: 100)
                                        : (GlobalVar.userData.tbClearance !=
                                                null)
                                            ? Image.network(
                                                "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.tbClearance}",
                                                height: 100)
                                            : Image.asset(AppAsset.pdf,
                                                height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),
                            SizedBox(height: 15),
                            Text("Physical Checkup"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(2);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.physicalCheckupfile != null
                                        ? Image.file(_vm.physicalCheckupfile!,
                                            height: 100)
                                        : (GlobalVar.userData.physicalCheckup !=
                                                null)
                                            ? Image.network(
                                                "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.physicalCheckup}",
                                                height: 100)
                                            : Image.asset(AppAsset.pdf,
                                                height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),
                            SizedBox(height: 15),
                            Text("Miscellaneous Doc"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(3);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.miscDocsFile != null
                                        ? Image.file(_vm.miscDocsFile!,
                                            height: 100)
                                        : (GlobalVar.userData.miscDoc != null)
                                            ? Image.network(
                                                "${IMG_PATH_MASTER}MasterUserProfile/${GlobalVar.userData.miscDoc}",
                                                height: 100)
                                            : Image.asset(AppAsset.pdf,
                                                height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),

                            SizedBox(height: 20),
                            loading
                                ? Center(child: CircularProgressIndicator())
                                : CommonButtonWidget(
                                    onTap: () {
                                      editMasterUser(_vm);
                                    },
                                    text: "Save"),
                            SizedBox(height: 20),
                          ])),
                ),
              ),
            )),
      );
    });
  }
}
