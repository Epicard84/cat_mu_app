import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class BoldText extends StatelessWidget {
  final String text;
  final Color textColor;
  final double textSize;
  final double height;
  final TextAlign align;
  const BoldText(
      {super.key,
      required this.text,
      this.textColor = Constant.textColor,
      this.textSize = 20,
      this.height = 0.0,
      this.align = TextAlign.start});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: align,
        style: TextStyle(
            height: height,
            fontFamily: AppFont.josefinSansBold,
            fontSize: textSize,
            color: textColor));
  }
}

class SubText extends StatelessWidget {
  final String text;
  final Color textColor;
  final TextAlign textAlign;
  const SubText(
      {super.key,
      required this.text,
      this.textColor = Constant.textColor2,
      this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(height: 1.5, color: textColor), textAlign: textAlign);
  }
}

class LabelText extends StatelessWidget {
  final String label;
  final Widget widget;
  const LabelText({
    super.key,
    required this.label,
    this.widget = const SizedBox(),
  });
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [BoldText(text: label), widget]);
  }
}

class CommonRichText extends StatelessWidget {
  final String text1;
  final String text2;
  final int maxLines;
  final Color textColor;
  const CommonRichText(
      {super.key,
      required this.text1,
      required this.text2,
      this.maxLines = 1,
      this.textColor = Colors.white});
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 5),
        child: RichText(
            maxLines: maxLines,
            text: TextSpan(
                style: TextStyle(
                    fontFamily: AppFont.josefinSansMedium, color: textColor),
                children: [TextSpan(text: text1), TextSpan(text: text2)])));
  }
}
