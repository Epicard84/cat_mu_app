class MedicineData {
  bool? success;
  String? medName;
  List<DateData>? dateNote;
  String? patientId;
  MedShedule? medShedule;

  MedicineData(
      {this.success,
      this.medName,
      this.dateNote,
      this.patientId,
      this.medShedule});

  MedicineData.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    medName = json['med_name'];
    if (json['date_note'] != null) {
      dateNote = <DateData>[];
      json['date_note'].forEach((v) {
        dateNote!.add(new DateData.fromJson(v));
      });
    }
    if (json['date_chart'] != null) {
      dateNote = <DateData>[];
      json['date_chart'].forEach((v) {
        dateNote!.add(new DateData.fromJson(v));
      });
    }
    if (json['date_narrative'] != null) {
      dateNote = <DateData>[];
      json['date_narrative'].forEach((v) {
        dateNote!.add(new DateData.fromJson(v));
      });
    }

    patientId = json['patient_id'];
    medShedule = json['med_shedule'] != null
        ? new MedShedule.fromJson(json['med_shedule'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['med_name'] = this.medName;
    if (this.dateNote != null) {
      data['date_note'] = this.dateNote!.map((v) => v.toJson()).toList();
    }
    data['patient_id'] = this.patientId;
    if (this.medShedule != null) {
      data['med_shedule'] = this.medShedule!.toJson();
    }
    return data;
  }
}

class DateData {
  int? id;
  int? patientId;
  String? scheduleId;
  String? type;
  String? descriptions;
  List<String>? images;
  String? signature;
  CreatorId? creatorId;
  String? date;
  CreatorId? getName;
  String? createdAt;

  DateData(
      {this.id,
      this.patientId,
      this.scheduleId,
      this.type,
      this.descriptions,
      this.images,
      this.signature,
      this.creatorId,
      this.createdAt,
      this.date,
      this.getName});

  DateData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    patientId = json['patient_id'];
    scheduleId = json['schedule_id'];
    type = json['type'];
    signature = json['signature'];
    descriptions = json['descriptions'];
    images = json['images'].cast<String>();
    creatorId = json['creator_id'] != null
        ? new CreatorId.fromJson(json['creator_id'])
        : null;
    date = json['date'];
    createdAt = json['time'];
    getName = json['get_name'] != null
        ? new CreatorId.fromJson(json['get_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['patient_id'] = this.patientId;
    data['schedule_id'] = this.scheduleId;
    data['type'] = this.type;
    data['signature'] = this.signature;
    data['descriptions'] = this.descriptions;
    data['time'] = this.createdAt;
    data['images'] = this.images;
    if (this.creatorId != null) {
      data['creator_id'] = this.creatorId!.toJson();
    }
    data['date'] = this.date;
    if (this.getName != null) {
      data['get_name'] = this.getName!.toJson();
    }
    return data;
  }
}

class CreatorId {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  String? companyName;
  String? zipCode;
  String? city;
  String? country;
  String? state;
  String? permanentAddress;
  String? mailingAddress;
  String? facilityAddress;
  String? status;
  String? lastActivity;
  String? profileImageName;
  int? userType;
  String? masterUser;
  String? createdAt;
  String? updatedAt;

  CreatorId(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phoneNumber,
      this.companyName,
      this.zipCode,
      this.city,
      this.country,
      this.state,
      this.permanentAddress,
      this.mailingAddress,
      this.facilityAddress,
      this.status,
      this.lastActivity,
      this.profileImageName,
      this.userType,
      this.masterUser,
      this.createdAt,
      this.updatedAt});

  CreatorId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    companyName = json['company_name'];
    zipCode = json['zip_code'];
    city = json['city'];
    country = json['country'];
    state = json['state'];
    permanentAddress = json['permanent_address'];
    mailingAddress = json['mailing_address'];
    facilityAddress = json['facility_address'];
    status = json['status'];
    lastActivity = json['last_activity'];
    profileImageName = json['profile_image_name'];
    userType = json['user_type'];
    masterUser = json['master_user'].toString();
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['company_name'] = this.companyName;
    data['zip_code'] = this.zipCode;
    data['city'] = this.city;
    data['country'] = this.country;
    data['state'] = this.state;
    data['permanent_address'] = this.permanentAddress;
    data['mailing_address'] = this.mailingAddress;
    data['facility_address'] = this.facilityAddress;
    data['status'] = this.status;
    data['last_activity'] = this.lastActivity;
    data['profile_image_name'] = this.profileImageName;
    data['user_type'] = this.userType;
    data['master_user'] = this.masterUser;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class MedShedule {
  int? id;
  int? medicineId;
  String? date;
  String? completeDetails;
  String? status;

  MedShedule(
      {this.id, this.medicineId, this.date, this.completeDetails, this.status});

  MedShedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    medicineId = json['medicine_id'] ?? 0;
    date = json['date'];
    completeDetails = json['complete_details'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['medicine_id'] = this.medicineId;
    data['date'] = this.date;
    data['complete_details'] = this.completeDetails;
    data['status'] = this.status;
    return data;
  }
}
