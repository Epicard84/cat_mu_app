import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/medication/model/medicineNote.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;

class EditMfdScreen extends ConsumerStatefulWidget {
  final String desc;
  final String noteId;

  const EditMfdScreen({
    required this.noteId,
    required this.desc,
  });

  @override
  ConsumerState<EditMfdScreen> createState() => _EditMfdScreenState();
}

class _EditMfdScreenState extends ConsumerState<EditMfdScreen> {
  TextEditingController title = TextEditingController();
  MedicineData? medicineNote;
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  bool loading = false;
  final client = HTTPClient();

  @override
  void initState() {
    super.initState();
    title.text = widget.desc;
  }

  int pageIndex = 0;

  updateNote() async {
    try {
      setState(() {
        loading = true;
      });
      var data = {'mfd_details': title.text};
      http.Response res = await client.postWithToken(
          "${APIEndpoint.editMfd}/${widget.noteId}", data);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          Navigator.pop(context, true);
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
        }
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(title: "Edit MFD", isBack: true),
        body: SafeArea(
            child: SingleChildScrollView(
                child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: FormBuilder(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CommonTextFiled(
                  controller: title,
                  maxLine: 4,
                  hintText: "Enter Description",
                  name: "Add Title",
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                ),
                SizedBox(height: 20),
                loading
                    ? Center(child: CircularProgressIndicator())
                    : CommonButtonWidget(
                        onTap: () {
                          if (formKey.currentState!.validate()) {
                            updateNote();
                          }
                        },
                        text: "Update"),
              ],
            ),
          ),
        ))));
  }
}
