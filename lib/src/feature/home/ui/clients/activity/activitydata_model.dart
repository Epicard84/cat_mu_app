import '../medication/model/medicineNote.dart';

class ActivtyDataModel {
  bool? success;
  String? actName;
  List<DateNote>? dateNote;
  String? patientId;
  ActShedule? actShedule;

  ActivtyDataModel(
      {this.success,
      this.actName,
      this.dateNote,
      this.patientId,
      this.actShedule});

  ActivtyDataModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    actName = json['act_name'];
    if (json['date_note'] != null) {
      dateNote = <DateNote>[];
      json['date_note'].forEach((v) {
        dateNote!.add(new DateNote.fromJson(v));
      });
    }
    patientId = json['patient_id'];
    actShedule = json['act_shedule'] != null
        ? new ActShedule.fromJson(json['act_shedule'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['act_name'] = this.actName;
    if (this.dateNote != null) {
      data['date_note'] = this.dateNote!.map((v) => v.toJson()).toList();
    }
    data['patient_id'] = this.patientId;
    if (this.actShedule != null) {
      data['act_shedule'] = this.actShedule!.toJson();
    }
    return data;
  }
}

class DateNote {
  int? id;
  int? patientId;
  int? scheduleId;
  String? type;
  String? descriptions;
  List<String>? images;
  CreatorId? creatorId;
  String? date;
  String? createdAt;
  String? updatedAt;
  String? time;
  CreatorId? getName;

  DateNote(
      {this.id,
      this.patientId,
      this.scheduleId,
      this.type,
      this.descriptions,
      this.images,
      this.creatorId,
      this.date,
      this.createdAt,
      this.updatedAt,
      this.time,
      this.getName});

  DateNote.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    patientId = json['patient_id'];
    scheduleId = json['schedule_id'];
    type = json['type'];
    descriptions = json['descriptions'];
    images = json['images'].cast<String>();
    creatorId = json['creator_id'] != null
        ? new CreatorId.fromJson(json['creator_id'])
        : null;
    date = json['date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    time = json['time'];
    getName = json['get_name'] != null
        ? new CreatorId.fromJson(json['get_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['patient_id'] = this.patientId;
    data['schedule_id'] = this.scheduleId;
    data['type'] = this.type;
    data['descriptions'] = this.descriptions;
    data['images'] = this.images;
    if (this.creatorId != null) {
      data['creator_id'] = this.creatorId!.toJson();
    }
    data['date'] = this.date;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['time'] = this.time;
    if (this.getName != null) {
      data['get_name'] = this.getName!.toJson();
    }
    return data;
  }
}

class ActShedule {
  int? id;
  int? activityId;
  String? date;
  String? scheduledTime;
  String? completeDetails;
  String? status;
  String? createdAt;
  String? updatedAt;

  ActShedule(
      {this.id,
      this.activityId,
      this.date,
      this.scheduledTime,
      this.completeDetails,
      this.status,
      this.createdAt,
      this.updatedAt});

  ActShedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    activityId = json['activity_id'];
    date = json['date'];
    scheduledTime = json['scheduled_time'];
    completeDetails = json['complete_details'].toString();
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['activity_id'] = this.activityId;
    data['date'] = this.date;
    data['scheduled_time'] = this.scheduledTime;
    data['complete_details'] = this.completeDetails;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
