import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;
import 'package:cat/src/core/common/widget/snackbar.dart';

class DisableAccount extends StatefulWidget {
  const DisableAccount({super.key});

  @override
  State<DisableAccount> createState() => _DisableAccountState();
}

class _DisableAccountState extends State<DisableAccount> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  TextEditingController reason = TextEditingController();
  bool loading = false;
  final client = HTTPClient();

  disableAccount() async {
    try {
      setState(() {
        loading = true;
      });
      var data = {'reason': reason.text};
      http.Response res =
          await client.postWithToken(APIEndpoint.disableAccount, data);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          Navigator.pop(context);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: CommonAppBar(title: "Deactivate account"),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: FormBuilder(
                  key: formKey,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CommonTextFiled(
                            maxLine: 4,
                            keyboardType: TextInputType.multiline,
                            controller: reason,
                            hintText: "Reason",
                            name: "reason",
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(),
                            ])),
                        SizedBox(height: 20),
                        loading
                            ? Center(child: CircularProgressIndicator())
                            : CommonButtonWidget(
                                onTap: () {
                                  if (formKey.currentState!.validate()) {
                                    disableAccount();
                                  }
                                },
                                text: "Submit"),
                        SizedBox(height: 20),
                      ])),
            ),
          )),
    );
  }
}
