import 'package:cat/src/core/common/helper/decoration.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
import 'package:cat/src/feature/home/ui/reminder/reminderModel.dart';
import 'package:cat/src/feature/home/ui/reminder/reminder_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:table_calendar/table_calendar.dart';

class ReminderScreen extends StatefulWidget {
  const ReminderScreen({super.key});

  @override
  State<ReminderScreen> createState() => _ReminderScreenState();
}

class _ReminderScreenState extends State<ReminderScreen> {
  TextEditingController date = TextEditingController();
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => DashboardScreen()),
            (Route<dynamic> route) => false);
        return await true;
      },
      child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
          child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                  centerTitle: true,
                  title: BoldText(text: "Reminder", textSize: 14),
                  leading: IconButton(
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    DashboardScreen()),
                            (Route<dynamic> route) => false);
                      },
                      icon: Image.asset(AppAsset.back, height: 15)),
                  elevation: 0,
                  backgroundColor: Colors.transparent),
              body: Consumer(builder: (context, ref, _) {
                final _vm = ref.watch(remindereVm);
                return _vm.loading
                    ? Center(child: CircularProgressIndicator())
                    : Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                        child: FormBuilder(
                          key: formKey,
                          child: ListView(children: [
                            FormBuilderDateTimePicker(
                                name: 'discharge',
                                initialEntryMode: DatePickerEntryMode.calendar,
                                initialValue: null,
                                firstDate: DateTime.now(),
                                onChanged: (e) {
                                  if (e != null)
                                    setState(() {
                                      date.text =
                                          "${e.year}-${e.month}-${e.day}";
                                    });
                                },
                                inputType: InputType.date,
                                decoration: textFieldDecoration(
                                    hintText: "Date",
                                    suffix: Icon(Icons.calendar_month))),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                maxLine: 3,
                                controller: _vm.text,
                                keyboardType: TextInputType.multiline,
                                hintText: "Enter detail",
                                name: "detail",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            _vm.loading
                                ? Center(child: CircularProgressIndicator())
                                : CommonButtonWidget(
                                    onTap: () {
                                      if (formKey.currentState!.validate()) {
                                        _vm.addReminder(context,
                                            date: date.text);
                                      }
                                    },
                                    text: "Save"),
                            Divider(height: 30),
                            TableCalendar(
                              availableGestures: AvailableGestures.none,
                              headerStyle:
                                  HeaderStyle(formatButtonVisible: false),
                              firstDay: DateTime.utc(2010, 10, 16),
                              lastDay: DateTime.utc(2030, 3, 14),
                              focusedDay: _vm.selectedDate,
                              selectedDayPredicate: (day) {
                                return isSameDay(_vm.selectedDate, day);
                              },
                              onDaySelected: (selectedDay, focusedDay) {
                                setState(() {
                                  _vm.selectedDate = selectedDay;
                                  _vm.getReminderList();
                                });
                              },
                            ),
                            Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: Align(
                                    alignment: Alignment.center,
                                    child: BoldText(
                                        text:
                                            "${formatDateWithDate(_vm.selectedDate)}"))),
                            SizedBox(height: 10),
                            _vm.reminderList.length < 1
                                ? Center(
                                    child: Text("No Reminder available..."))
                                : ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: _vm.reminderList.length,
                                    itemBuilder: (BuildContext context, int i) {
                                      ReminderData list = _vm.reminderList[i];
                                      return Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 5),
                                        child:
                                            // Dismissible(
                                            //   background: Container(
                                            //       color: Colors.red),
                                            //   key: Key("${list.id}"),
                                            //   onDismissed: (direction) {
                                            //     _vm.deleteReminder(context,
                                            //         reminderId: "${list.id}");
                                            //     showSnackBar(context,
                                            //         text:
                                            //             "Reminder deleted successfully",
                                            //         success: true);
                                            //   },
                                            //   child:

                                            ListTile(
                                                trailing:
                                                    _vm.serverDate.day !=
                                                            _vm.selectedDate.day
                                                        ? null
                                                        : Container(
                                                            width: 100,
                                                            child: _vm.loadReminder &&
                                                                    i ==
                                                                        _vm
                                                                            .loadReminderIndex
                                                                ? Center(
                                                                    child:
                                                                        CircularProgressIndicator())
                                                                : Row(
                                                                    children: [
                                                                      list.status ==
                                                                              0
                                                                          ? IconButton(
                                                                              onPressed: () {
                                                                                _vm.updateStatus(context, reminderId: "${list.id}");
                                                                              },
                                                                              icon: Icon(Icons.radio_button_unchecked, color: Colors.green))
                                                                          : IconButton(onPressed: null, icon: Icon(Icons.check_circle, color: Colors.green)),
                                                                      IconButton(
                                                                          onPressed:
                                                                              () {
                                                                            _vm.deleteReminder(context,
                                                                                index: i,
                                                                                reminderId: "${list.id}");
                                                                            showSnackBar(context,
                                                                                text: "Reminder deleted successfully",
                                                                                success: true);
                                                                          },
                                                                          icon:
                                                                              Icon(Icons.delete)),
                                                                    ],
                                                                  ),
                                                          ),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                tileColor: Colors.white,
                                                leading: Image.asset(
                                                    AppAsset.reminder,
                                                    color:
                                                        Constant.primaryColor,
                                                    height: 30),
                                                title:
                                                    Text(list.reminder ?? "")),
                                      )
                                          // )

                                          ;
                                    }),
                          ]),
                        ));
              }))),
    );
  }
}
