import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/medication/addNewDoc.dart';
import 'package:flutter/material.dart';

class NarrativeScreen extends StatelessWidget {
  const NarrativeScreen({super.key});
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    List img = [
      AppAsset.img1,
      AppAsset.img2,
      AppAsset.img3,
      AppAsset.img2,
      AppAsset.img3,
    ];
    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(title: "Narrative"),
        body: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(children: [
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset(AppAsset.calender,
                                    height: 20),
                                SizedBox(width: 5),
                                Text("01/02/2023, 12:00",
                                    style:
                                        TextStyle(color: Colors.grey))
                              ],
                            ),
                            Container(
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Constant.primaryColor),
                                  shape: BoxShape.circle),
                              child: const Text("HE",
                                  style: TextStyle(
                                      color: Constant.primaryColor)),
                            ),
                          ],
                        ),
                        SizedBox(height: 15),
                        Text(
                          "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                          style: TextStyle(
                              height: 1.5, color: Color(0xff545C7A)),
                        ),
                        SizedBox(height: 15),
                      ],
                    ),
                  )),
              SizedBox(height: 20),
              Container(
                alignment: Alignment.center,
                width: size.width,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15)),
                child: Wrap(spacing: 5, runSpacing: 0.0, children: [
                  ...img
                      .map((v) => Container(
                          margin: EdgeInsets.all(2),
                          height: 80,
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child:
                                  Image.asset(v, fit: BoxFit.cover))))
                      .toList(),
                  Padding(
                      padding: EdgeInsets.all(5),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      AddDocScreen()));
                        },
                        child: Container(
                            height: 80,
                            width: 80,
                            decoration: BoxDecoration(
                                color: Constant.bgColor,
                                borderRadius:
                                    BorderRadius.circular(7)),
                            child: Icon(Icons.add,
                                color: Constant.primaryColor)),
                      ))
                ]),
              ),
            ])));
  }
}
