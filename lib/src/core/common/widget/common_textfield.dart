import 'package:cat/src/core/common/helper/decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class CommonTextFiled extends StatelessWidget {
  final String name;
  final String hintText;
  final TextEditingController? controller;
  final Widget suffixIcon;
  final bool obscureText;
  final TextInputType keyboardType;
  final String? Function(String?)? validator;
  final TextInputAction textInputAction;
  final List<TextInputFormatter>? inputFormatters;
  final bool isEnabled;
  final int maxLine;
  final int maxLength;

  const CommonTextFiled(
      {super.key,
      this.controller,
      this.maxLine = 1,
      this.maxLength = 100000,
      required this.hintText,
      required this.name,
      required this.validator,
      this.suffixIcon = const SizedBox(),
      this.obscureText = false,
      this.keyboardType = TextInputType.text,
      this.textInputAction = TextInputAction.next,
      this.inputFormatters,
      this.isEnabled = true});

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
        textCapitalization: TextCapitalization.sentences,
        enabled: isEnabled,
        maxLines: maxLine,
        maxLength: maxLength,
        controller: controller,
        obscureText: obscureText,
        inputFormatters: inputFormatters,
        decoration: textFieldDecoration(hintText: hintText, suffix: suffixIcon),
        validator: validator,
        keyboardType: keyboardType,
        // textInputAction: TextInputAction.newline,
        name: name);
  }
}
