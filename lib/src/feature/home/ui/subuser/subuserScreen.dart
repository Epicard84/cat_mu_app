import 'dart:convert';
import 'dart:developer';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/subUser.dart';
import 'package:cat/src/feature/home/ui/profile/profile_screen.dart';
import 'package:cat/src/feature/home/ui/subuser/addSubuser.dart';
import 'package:cat/src/feature/home/ui/subuser/subuserDetail.dart';
import 'package:flutter/material.dart';

class SubuserScreen extends StatefulWidget {
  const SubuserScreen({super.key});

  @override
  State<SubuserScreen> createState() => _SubuserScreenState();
}

class _SubuserScreenState extends State<SubuserScreen> {
  List<SubUserData> subuserList = [];
  bool loading = false;
  final client = HTTPClient();

  void initState() {
    super.initState();
    getSubuserList();
  }

  getSubuserList() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(APIEndpoint.subUserList);

      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          SubUserResp resp = SubUserResp.fromJson(jsonResp);
          subuserList = resp.data ?? [];
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      log("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  deleteSubuser({required String id}) async {
    try {
      setState(() {
        loading = true;
      });
      var res =
          await await client.getWithToken("${APIEndpoint.deleteSubUser}/$id");

      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          getSubuserList();
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      log("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  removeSubuser({required String id}) async {
    try {
      setState(() {
        loading = true;
      });
      var res =
          await await client.getWithToken("${APIEndpoint.removeSubUser}/$id");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          getSubuserList();
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      log("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constant.bgColor,
        body: SafeArea(
            child: SingleChildScrollView(
          child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Image.asset(AppAsset.logo, scale: 3),
                          SizedBox(width: 10),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          MasterProfileScreen()));
                            },
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SubText(text: "${GlobalVar.cityName}"),
                                  BoldText(
                                      text:
                                          "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}")
                                ]),
                          ),
                        ],
                      ),
                      SizedBox(width: 10),
                      IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(Icons.home_outlined)),
                    ]),
                SizedBox(height: 15),
                loading
                    ? Center(child: CircularProgressIndicator())
                    : ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: subuserList.length,
                        padding: EdgeInsets.zero,
                        itemBuilder: (BuildContext context, int i) {
                          SubUserData user = subuserList[i];
                          return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ListTile(
                                  leading: CircleAvatar(
                                      backgroundImage: (user
                                                  .profileImageName!.length >
                                              1)
                                          ? Image.network(
                                                  "${IMG_PATH}SubUserProfile/${user.profileImageName}")
                                              .image
                                          : Image.asset(AppAsset.dp).image,
                                      radius: 25),
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SubuserDetailScreen(
                                                    user: user)));
                                  },
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  tileColor: Colors.white,
                                  trailing: IconButton(
                                      onPressed: () {
                                        deleteSubuser(id: "${user.id}");
                                      },
                                      icon: Icon(Icons.archive_outlined)),
                                  title: BoldText(
                                      text:
                                          "${user.firstName} ${user.lastName}")));
                        }),
                SizedBox(height: 15),
                SizedBox(height: 10),
                ButttonWithIconWidget(
                    icon: Icons.add,
                    align: MainAxisAlignment.center,
                    text: "Add Sub user",
                    onTap: () async {
                      var isLoad = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddSubuserScreen()));
                      if (isLoad != null && isLoad) {
                        getSubuserList();
                      }
                    })
              ])),
        )));
  }
}
