class MedicineDetail {
  bool? success;
  MedicineData? data;

  MedicineDetail({this.success, this.data});

  MedicineDetail.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data =
        json['data'] != null ? new MedicineData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class MedicineData {
  int? id;
  String? clientId;
  String? medicineName;
  String? startDate;
  String? endDate;
  int? timeDays;
  int? status;
  String? descriptions;
  List<String>? docs;
  String? createdAt;
  String? updatedAt;

  MedicineData(
      {this.id,
      this.clientId,
      this.medicineName,
      this.startDate,
      this.endDate,
      this.timeDays,
      this.status,
      this.descriptions,
      this.docs,
      this.createdAt,
      this.updatedAt});

  MedicineData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    medicineName = json['medicine_name'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    timeDays = json['time_days'];
    status = json['status'];
    descriptions = json['descriptions'];
    docs = json['docs'].cast<String>();
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['medicine_name'] = this.medicineName;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['time_days'] = this.timeDays;
    data['status'] = this.status;
    data['descriptions'] = this.descriptions;
    data['docs'] = this.docs;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
