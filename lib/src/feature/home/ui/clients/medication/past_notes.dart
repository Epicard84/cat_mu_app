import 'dart:convert';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/photo_view.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/medication/edit_note.dart';
import 'package:cat/src/feature/home/ui/clients/medication/model/allChart.dart';
import 'package:cat/src/feature/home/ui/clients/medication/noteScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../model/client.dart';

class PastNotesScreen extends StatefulWidget {
  final MedData data;
  final bool isChart;
  final bool isNarrative;
  final bool isArchive;
  final DateTime timeNow;
  final ClientData client;
  final bool goBack;
  const PastNotesScreen({
    super.key,
    required this.data,
    this.isChart = false,
    this.isNarrative = false,
    this.isArchive = false,
    required this.timeNow,
    required this.client,
    this.goBack = true,
  });

  @override
  State<PastNotesScreen> createState() => _PastNotesScreenState();
}

class _PastNotesScreenState extends State<PastNotesScreen> {
  TextEditingController title = TextEditingController();
  AllChart? medicineNote;
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  bool loading = false;
  final client = HTTPClient();
  final PageController controller = PageController();
  int pageIndex = 0;

  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client
          .getWithToken("${APIEndpoint.allChart}/${widget.data.patientId}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          medicineNote = AllChart.fromJson(jsonResp);
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  fetchNotes() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(
          "${APIEndpoint.filterNotes}/${widget.data.patientId}/0");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          medicineNote = AllChart.fromJson(jsonResp);
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  fetchProgNote() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(
          "${APIEndpoint.filterNotes}/${widget.data.patientId}/1");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          medicineNote = AllChart.fromJson(jsonResp);
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  fetchDrOrder() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(
          "${APIEndpoint.filterNotes}/${widget.data.patientId}/2");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          medicineNote = AllChart.fromJson(jsonResp);
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Consumer(builder: (context, ref, child) {
      return Scaffold(
          backgroundColor: Constant.bgColor,
          appBar: CommonAppBar(
              isWidget: true,
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BoldText(text: "Notes/Prog Notes/Dr's Orders", textSize: 14),
                  BoldText(
                      text:
                          "${widget.client.firstName} ${widget.client.lastName}",
                      textSize: 14)
                ],
              ),
              isBack: true),
          body: loading
              ? Center(child: CircularProgressIndicator())
              : SafeArea(
                  child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CommonButtonWidget(
                            textSize: 13,
                            width: size.width / 4.8,
                            onTap: () {
                              fetchData();
                            },
                            text: "All"),
                        CommonButtonWidget(
                            textSize: 13,
                            width: size.width / 4.8,
                            onTap: () {
                              fetchNotes();
                            },
                            text: "Notes"),
                        CommonButtonWidget(
                            textSize: 13,
                            width: size.width / 4.8,
                            onTap: () {
                              fetchProgNote();
                            },
                            text: "Prog Notes"),
                        CommonButtonWidget(
                            textSize: 13,
                            width: size.width / 4.8,
                            onTap: () {
                              fetchDrOrder();
                            },
                            text: "Dr's Order"),
                      ],
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: FormBuilder(
                              key: formKey,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        padding: EdgeInsets.all(15),
                                        width: size.width,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: Colors.white),
                                        child: Column(children: [
                                          medicineNote!.details!.length < 1
                                              ? Text("Data not found.")
                                              : ListView.separated(
                                                  separatorBuilder:
                                                      (context, index) =>
                                                          Divider(height: 20),
                                                  shrinkWrap: true,
                                                  physics:
                                                      NeverScrollableScrollPhysics(),
                                                  itemCount: medicineNote!
                                                      .details!.length,
                                                  padding: EdgeInsets.zero,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int i) {
                                                    AllDetail list =
                                                        medicineNote!
                                                            .details![i];
                                                    return Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              CircleAvatar(
                                                                  radius: 25,
                                                                  backgroundColor:
                                                                      Constant
                                                                          .primaryColor,
                                                                  child: Text(
                                                                      getInitials(
                                                                          string:
                                                                              "${list.creatorId?.firstName} ${list.creatorId?.lastName}",
                                                                          limitTo:
                                                                              2),
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.white))),
                                                              Row(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Text(
                                                                      "${list.type}"),
                                                                  SizedBox(
                                                                      width: 5),
                                                                  Text(
                                                                      "${list.time}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              11),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start),
                                                                  widget.isArchive ||
                                                                          DateTime.parse(widget.data.date).isBefore(DateTime.now().subtract(Duration(
                                                                              days:
                                                                                  4)))
                                                                      // "${widget.data.date}" !=
                                                                      //     "${widget.timeNow.year}-${widget.timeNow.month.toString().padLeft(2, '0')}-${widget.timeNow.day.toString().padLeft(2, '0')}"
                                                                      ? SizedBox()
                                                                      : IconButton(
                                                                          padding: EdgeInsets.all(
                                                                              0),
                                                                          onPressed:
                                                                              () async {
                                                                            var load = await Navigator.push(
                                                                                context,
                                                                                MaterialPageRoute(
                                                                                    builder: (context) => EditNoteScreen(
                                                                                          noteId: "${list.id}",
                                                                                          desc: "${list.descriptions}",
                                                                                          image: list.images ?? [],
                                                                                          data: widget.data,
                                                                                          timeNow: widget.timeNow,
                                                                                          client: widget.client,
                                                                                          isChart: widget.isChart,
                                                                                          isArchive: widget.isArchive,
                                                                                          isNarrative: widget.isNarrative,
                                                                                        )));

                                                                            if (load != null &&
                                                                                load == true) {
                                                                              fetchData();
                                                                            }
                                                                          },
                                                                          icon:
                                                                              Icon(Icons.edit)),
                                                                ],
                                                              ),
                                                            ]),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 50.0),
                                                          child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                SubText(
                                                                    text:
                                                                        list.descriptions ??
                                                                            "",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start),
                                                                Wrap(
                                                                    direction: Axis
                                                                        .horizontal,
                                                                    spacing: 5,
                                                                    runSpacing:
                                                                        0.0,
                                                                    children: [
                                                                      ...list
                                                                          .images!
                                                                          .map((v) => !v.split('/').last.contains(".")
                                                                              ? SizedBox()
                                                                              : (v.split('.').last != "jpeg") && (v.split('.').last != "jpg") && (v.split('.').last != "png")
                                                                                  ? InkWell(
                                                                                      onTap: () {
                                                                                        downloadOpenFile(url: v, fileName: v.split('/').last);
                                                                                      },
                                                                                      child: Padding(
                                                                                          padding: const EdgeInsets.symmetric(vertical: 20.0),
                                                                                          child: IconButton(
                                                                                            onPressed: () {
                                                                                              downloadOpenFile(url: v, fileName: v.split('/').last);
                                                                                            },
                                                                                            icon: Icon(Icons.download),
                                                                                          )))
                                                                                  : GestureDetector(
                                                                                      onTap: () {
                                                                                        showDialog(
                                                                                          context: context,
                                                                                          builder: (BuildContext context) {
                                                                                            return StatefulBuilder(builder: (context, setState2) {
                                                                                              setPageIndex(int index) {
                                                                                                print("index >> $index");
                                                                                                pageIndex = index;
                                                                                                controller.jumpToPage(pageIndex);
                                                                                                setState2(() {});
                                                                                              }

                                                                                              return AlertDialog(
                                                                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                                                                content: Container(
                                                                                                    height: 300,
                                                                                                    width: 300,
                                                                                                    child: PageView.builder(
                                                                                                      controller: controller,
                                                                                                      onPageChanged: (i) {
                                                                                                        setPageIndex(i);
                                                                                                      },
                                                                                                      itemCount: list.images!.length,
                                                                                                      scrollDirection: Axis.horizontal,
                                                                                                      itemBuilder: (context, index) {
                                                                                                        return GestureDetector(
                                                                                                          onTap: () {
                                                                                                            Navigator.push(context, MaterialPageRoute(builder: (context) => ViewPhoto(image: v)));
                                                                                                          },
                                                                                                          child: Container(
                                                                                                            decoration: BoxDecoration(image: DecorationImage(image: Image.network(v, height: 200).image)),
                                                                                                            child: Row(
                                                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                              children: [
                                                                                                                IconButton(
                                                                                                                  icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                                                                                                                  onPressed: () {
                                                                                                                    if (pageIndex >= 0) {
                                                                                                                      setPageIndex(--pageIndex);
                                                                                                                    }
                                                                                                                  },
                                                                                                                ),
                                                                                                                GestureDetector(
                                                                                                                    onTap: () {
                                                                                                                      Navigator.push(context, MaterialPageRoute(builder: (context) => ViewPhoto(image: v)));
                                                                                                                    },
                                                                                                                    child: Container(
                                                                                                                      height: 200,
                                                                                                                      width: 200,
                                                                                                                      color: Colors.transparent,
                                                                                                                    )),
                                                                                                                IconButton(
                                                                                                                  icon: Icon(Icons.arrow_forward_ios, color: Colors.white),
                                                                                                                  onPressed: () {
                                                                                                                    setPageIndex(++pageIndex);
                                                                                                                  },
                                                                                                                ),
                                                                                                              ],
                                                                                                            ),
                                                                                                          ),
                                                                                                        );
                                                                                                      },
                                                                                                    )),
                                                                                              );
                                                                                            });
                                                                                          },
                                                                                        );
                                                                                      },
                                                                                      child: Container(margin: EdgeInsets.all(2), height: 100, child: ClipRRect(borderRadius: BorderRadius.circular(10), child: Image.network(v, fit: BoxFit.cover))),
                                                                                    ))
                                                                          .toList(),
                                                                    ]),
                                                              ]),
                                                        ),
                                                        if (list.signature !=
                                                            null) ...[
                                                          Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Column(
                                                              children: [
                                                                SubText(
                                                                    text:
                                                                        "Signature",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start),
                                                                Container(
                                                                  height: 40.0,
                                                                  width: 80.0,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    image:
                                                                        DecorationImage(
                                                                      image: NetworkImage(
                                                                          "${list.signature!}"),
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        ],
                                                      ],
                                                    );
                                                  }),
                                        ])),
                                  ]),
                            ),
                          )),
                    ),
                  ],
                )));
    });
  }
}
