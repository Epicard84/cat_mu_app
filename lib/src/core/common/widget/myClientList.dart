import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class MyClientList extends StatelessWidget {
  final String name;
  final bool isOnline;
  final Function onTap;
  final IconData icon;
  const MyClientList(
      {super.key,
      required this.name,
      this.isOnline = false,
      required this.onTap,
      required this.icon});

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Container(
        height: 140,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10)),
        child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Row(
                          mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                          children: [
                            BoldText(text: name),
                            IconButton(
                                onPressed: () {
                                  onTap();
                                },
                                icon: Icon(icon,
                                    color: Constant.textColor))
                          ])),
                  Row(
                      mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                      children: [
                        CommonButtonWidget(
                            onTap: () {},
                            text: "Update",
                            buttonColors: Constant.buttonColor,
                            width: screenWidth / 2.8,
                            height: 43),
                        CommonButtonWidget(
                            onTap: () {},
                            text: "Discharge",
                            buttonColors: Constant.buttonColor,
                            width: screenWidth / 2.8,
                            height: 43,
                            radius: 10)
                      ])
                ])));
  }
}
