import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonCont.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/photo_view.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/subUser.dart';
import 'package:cat/src/feature/home/ui/subuser/editSubuser.dart';
import 'package:flutter/material.dart';
import '../../../../core/common/helper/string.dart';
import '../../../../core/common/widget/snackbar.dart';
import '../../model/subuserDetail.dart';

class SubuserDetailScreen extends StatefulWidget {
  final SubUserData user;
  const SubuserDetailScreen({super.key, required this.user});

  @override
  State<SubuserDetailScreen> createState() => _SubuserDetailScreenState();
}

class _SubuserDetailScreenState extends State<SubuserDetailScreen> {
  bool loading = false;
  final client = HTTPClient();
  SubuserDetailNew subuser = SubuserDetailNew();
  void initState() {
    super.initState();
    fetchSubuser();
  }

  fetchSubuser() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client
          .getWithToken("${APIEndpoint.subUserProfile}/${widget.user.id}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          subuser = SubuserDetailNew.fromJson(jsonResp['data']);
        }
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      loading = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(title: "Subuser", isBack: true),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CommonContainer(
                          image: AssetImage(AppAsset.roundEffect),
                          child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 15),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(height: 10),
                                        BoldText(
                                            text:
                                                "${subuser.profile?.firstName} ${subuser.profile?.lastName}",
                                            textColor: Colors.white),
                                        SizedBox(height: 10),
                                      ]),
                                  IconButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    EditSubuserScreen(
                                                        user: subuser)));
                                      },
                                      icon: Icon(Icons.edit_outlined,
                                          color: Colors.white)),
                                ],
                              ))),
                      SizedBox(height: 20),
                      Container(
                          width: size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                          child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 15),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // CommonRichText(
                                    //     text1: "Phone number:  ",
                                    //     text2: "${user.phoneNumber}",
                                    //     textColor: Colors.black),
                                    // SizedBox(height: 5),
                                    CommonRichText(
                                        text1: "Email address:  ",
                                        text2: "${subuser.profile?.email}",
                                        textColor: Colors.black),
                                    SizedBox(height: 5),

                                    // CommonRichText(
                                    //     maxLines: 2,
                                    //     text1: "Address: ",
                                    //     text2: "${subuser.profile?..permanentAddress}",
                                    //     textColor: Colors.black),
                                    // SizedBox(height: 5),
                                    // CommonRichText(
                                    //     text1: "Mailing address: ",
                                    //     text2: "${user.mailingAddress}",
                                    //     textColor: Colors.black),
                                    // SizedBox(height: 5),
                                    // CommonRichText(
                                    //     text1: "City: ",
                                    //     text2: "${user.city}",
                                    //     textColor: Colors.black),
                                    // SizedBox(height: 5),
                                    // CommonRichText(
                                    //     text1: "State:  ",
                                    //     text2: "${user.state}",
                                    //     textColor: Colors.black),
                                    // SizedBox(height: 5),
                                    // CommonRichText(
                                    //     text1: "Country:  ",
                                    //     text2: "${user.country}",
                                    //     textColor: Colors.black),
                                    // SizedBox(height: 5)
                                  ]))),

                      // Padding(
                      //     padding: EdgeInsets.symmetric(vertical: 10),
                      //     child: LabelText(label: "Documents")),
                      // Container(
                      //     height: 230,
                      //     child: ListView.builder(
                      //         scrollDirection: Axis.horizontal,
                      //         itemCount: subuser.document?.length ?? 0,
                      //         padding: EdgeInsets.symmetric(horizontal: 10),
                      //         itemBuilder: (BuildContext context, int i) {
                      //           String item = subuser.document![i];
                      //           return !item.split('/').last.contains(".")
                      //               ? SizedBox()
                      //               : PDFDoc(
                      //                   name: item.split('/').last,
                      //                   onTap: () async {
                      //                     print("${BASE_URL}${item}");
                      //                     await downloadOpenFile(
                      //                         url: "${BASE_URL}${item}",
                      //                         fileName: item);
                      //                   });
                      //         })),
                      SizedBox(height: 10),
                      Text("Operators License"),
                      SizedBox(height: 5),
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewPhoto(
                                        image:
                                            "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.operatorsLicense}")));
                          },
                          child: Container(
                            height: 150,
                            color: Colors.transparent,
                            child: Image.network(
                                "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.operatorsLicense}"),
                          )),

                      SizedBox(height: 15),
                      Text("TB Clearance"),
                      SizedBox(height: 5),
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewPhoto(
                                        image:
                                            "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.tbClearance}")));
                          },
                          child: Container(
                            height: 150,
                            color: Colors.transparent,
                            child: Image.network(
                                "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.tbClearance}"),
                          )),

                      SizedBox(height: 15),
                      Text("Physical Checkup"),
                      SizedBox(height: 5),
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewPhoto(
                                        image:
                                            "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.physicalCheckup}")));
                          },
                          child: Container(
                            height: 150,
                            color: Colors.transparent,
                            child: Image.network(
                                "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.physicalCheckup}"),
                          )),

                      SizedBox(height: 15),
                      Text("Miscellaneous Doc"),
                      SizedBox(height: 5),
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewPhoto(
                                        image:
                                            "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.miscDoc}")));
                          },
                          child: Container(
                            height: 150,
                            color: Colors.transparent,
                            child: Image.network(
                                "${IMG_PATH_MASTER}SubUserProfile/${subuser.profile?.miscDoc}"),
                          )),

                      SizedBox(height: 20),
                    ]),
              )));
  }
}
