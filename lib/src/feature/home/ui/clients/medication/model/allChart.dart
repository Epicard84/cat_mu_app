class AllChart {
  bool? success;
  List<AllDetail>? details;
  String? patientId;

  AllChart({this.success, this.details, this.patientId});

  AllChart.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['details'] != null) {
      details = <AllDetail>[];
      json['details'].forEach((v) {
        details!.add(new AllDetail.fromJson(v));
      });
    }
    patientId = json['patient_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.details != null) {
      data['details'] = this.details!.map((v) => v.toJson()).toList();
    }
    data['patient_id'] = this.patientId;
    return data;
  }
}

class AllDetail {
  int? id;
  int? patientId;
  String? scheduleId;
  String? type;
  String? descriptions;
  List<String>? images;
  CreatorId? creatorId;
  String? date;
  String? createdAt;
  String? updatedAt;
  String? time;
  String? signature;
  CreatorId? getName;

  AllDetail(
      {this.id,
      this.patientId,
      this.scheduleId,
      this.type,
      this.descriptions,
      this.images,
      this.creatorId,
      this.date,
      this.createdAt,
      this.updatedAt,
      this.time,
      this.signature,
      this.getName});

  AllDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    patientId = json['patient_id'];
    scheduleId = json['schedule_id'];
    type = json['type'];
    descriptions = json['descriptions'];
    images = json['images'].cast<String>();
    creatorId = json['creator_id'] != null
        ? new CreatorId.fromJson(json['creator_id'])
        : null;
    date = json['date'];
    signature = json['signature'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    time = json['time'];
    getName = json['get_name'] != null
        ? new CreatorId.fromJson(json['get_name'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['patient_id'] = this.patientId;
    data['schedule_id'] = this.scheduleId;
    data['type'] = this.type;
    data['descriptions'] = this.descriptions;
    data['signature'] = this.signature;
    data['images'] = this.images;
    if (this.creatorId != null) {
      data['creator_id'] = this.creatorId!.toJson();
    }
    data['date'] = this.date;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['time'] = this.time;
    if (this.getName != null) {
      data['get_name'] = this.getName!.toJson();
    }
    return data;
  }
}

class CreatorId {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  String? companyName;
  String? city;
  String? country;
  String? state;
  String? permanentAddress;
  String? mailingAddress;
  String? facilityAddress;
  String? status;
  String? lastActivity;
  String? profileImageName;
  int? userType;
  String? createdAt;
  String? updatedAt;

  CreatorId(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phoneNumber,
      this.companyName,
      this.city,
      this.country,
      this.state,
      this.permanentAddress,
      this.mailingAddress,
      this.facilityAddress,
      this.status,
      this.lastActivity,
      this.profileImageName,
      this.userType,
      this.createdAt,
      this.updatedAt});

  CreatorId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    companyName = json['company_name'];
    city = json['city'];
    country = json['country'];
    state = json['state'];
    permanentAddress = json['permanent_address'];
    mailingAddress = json['mailing_address'];
    facilityAddress = json['facility_address'];
    status = json['status'];
    lastActivity = json['last_activity'];
    profileImageName = json['profile_image_name'];
    userType = json['user_type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['company_name'] = this.companyName;
    data['city'] = this.city;
    data['country'] = this.country;
    data['state'] = this.state;
    data['permanent_address'] = this.permanentAddress;
    data['mailing_address'] = this.mailingAddress;
    data['facility_address'] = this.facilityAddress;
    data['status'] = this.status;
    data['last_activity'] = this.lastActivity;
    data['profile_image_name'] = this.profileImageName;
    data['user_type'] = this.userType;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
