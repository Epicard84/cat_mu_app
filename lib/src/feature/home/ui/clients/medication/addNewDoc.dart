// import 'package:cat/src/core/common/widget/commonAppbar.dart';
// import 'package:cat/src/core/common/widget/commonButton.dart';
// import 'package:cat/src/core/common/widget/commonTextStyle.dart';
// import 'package:cat/src/core/common/widget/common_textfield.dart';
// import 'package:cat/src/feature/home/ui/clients/medication/addNewDocVm.dart';
// import 'package:dotted_border/dotted_border.dart';
// import 'package:cat/src/core/constant/constant.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';

// class AddDocScreen extends StatefulWidget {
//   const AddDocScreen({super.key});

//   @override
//   State<AddDocScreen> createState() => _AddDocScreenState();
// }

// class _AddDocScreenState extends State<AddDocScreen> {
//   TextEditingController title = TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     return Consumer(builder: (context, ref, child) {
//       final _vm = ref.watch(addNewDoctVm);
//       return Container(
//           height: double.infinity,
//           width: double.infinity,
//           decoration: BoxDecoration(
//               image: DecorationImage(
//                   image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
//           child: Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CommonAppBar(isBack: true, title: "Add New Documents"),
//               body: Padding(
//                 padding: const EdgeInsets.all(15.0),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     CommonTextFiled(
//                         controller: title,
//                         hintText: "Add Title",
//                         name: "Add Title",
//                         validator: null),
//                     SizedBox(height: 20),
//                     GestureDetector(
//                       onTap: () {
//                         _vm.mediaOption(context);
//                       },
//                       child: DottedBorder(
//                         color: Colors.black,
//                         strokeWidth: 1,
//                         radius: Radius.circular(20),
//                         child: Container(
//                           child: Column(
//                             children: [
//                               SizedBox(height: 20),
//                               Image.asset(AppAsset.doc, height: 40),
//                               SizedBox(height: 20),
//                               Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   SubText(text: "Select file to upload / "),
//                                   Image.asset(AppAsset.camera, height: 20),
//                                   SizedBox(width: 5),
//                                   SubText(
//                                       text: "Capture",
//                                       textColor: Constant.primaryColor),
//                                 ],
//                               ),
//                               SizedBox(height: 20),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                     SizedBox(height: 10),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         SubText(text: "Documents and images only"),
//                         Row(
//                           children: [
//                             Image.asset(AppAsset.lock, height: 20),
//                             SizedBox(width: 5),
//                             SubText(text: "Secured"),
//                           ],
//                         ),
//                       ],
//                     ),
//                     SizedBox(height: 20),
//                     ListView.builder(
//                         shrinkWrap: true,
//                         physics: NeverScrollableScrollPhysics(),
//                         itemCount: _vm.docFile.length,
//                         padding: EdgeInsets.zero,
//                         itemBuilder: (BuildContext context, int i) {
//                           var text = _vm.docFile[i];
//                           return Padding(
//                             padding: const EdgeInsets.symmetric(vertical: 5.0),
//                             child: ListTile(
//                                 trailing: IconButton(
//                                     onPressed: () {}, icon: Icon(Icons.close)),
//                                 shape: RoundedRectangleBorder(
//                                     borderRadius:
//                                         BorderRadius.all(Radius.circular(10))),
//                                 tileColor: Colors.white,
//                                 title: Text(text.path.split('/').last)),
//                           );
//                         }),
//                     SizedBox(height: 20),
//                     CommonButtonWidget(
//                         onTap: () {
//                           Navigator.pop(context);
//                         },
//                         text: "Add Now"),
//                     SizedBox(height: 20),
//                   ],
//                 ),
//               )));
//     });
//   }
// }
