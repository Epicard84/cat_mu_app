import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class PDFDoc extends StatelessWidget {
  final String name;
  final Function onTap;
  const PDFDoc({super.key, required this.name, required this.onTap});

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        child: Container(
            width: screenWidth / 2.2,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15), color: Colors.white),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Image.asset(AppAsset.pdf),
              SizedBox(height: 10),
              SubText(text: name, textColor: Constant.textColor2),
              SizedBox(height: 10),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ButttonWithIconWidget(
                      textSize: 14,
                      height: 40,
                      width: 150,
                      onTap: () {
                        onTap();
                      },
                      icon: Icons.download,
                      text: "Download"))
            ])));
  }
}
