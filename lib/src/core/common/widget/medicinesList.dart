import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class MedicinesList extends StatelessWidget {
  final Function onTap1;
  final Function onTap2;
  final Function menu;
  final String text;
  final String medicinesName;
  const MedicinesList(
      {super.key,
      required this.onTap1,
      required this.onTap2,
      required this.text,
      required this.medicinesName,
      required this.menu});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 8),
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.white),
            child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 20, vertical: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      LabelText(
                          label: medicinesName,
                          widget: IconButton(
                              padding: EdgeInsets.zero,
                              onPressed: () {
                                PopupMenuButton<int>(
                                    itemBuilder:
                                        (BuildContext context) =>
                                            <PopupMenuItem<int>>[
                                              new PopupMenuItem<int>(
                                                  value: 1,
                                                  child: new Text(
                                                      'Item One')),
                                              new PopupMenuItem<int>(
                                                  value: 2,
                                                  child: new Text(
                                                      'Item Two')),
                                              new PopupMenuItem<int>(
                                                  value: 3,
                                                  child: new Text(
                                                      'Item Three')),
                                              new PopupMenuItem<int>(
                                                  value: 4,
                                                  child: new Text(
                                                      'I am Item Four'))
                                            ],
                                    onSelected: (int value) {
                                      // setState(() { _value = value; });
                                    });
                              },
                              icon: Icon(Icons.more_horiz,
                                  color: Constant.textColor))),
                      Padding(
                          padding:
                              EdgeInsets.only(top: 5, bottom: 10),
                          child: Row(children: [
                            GestureDetector(
                                onTap: () {
                                  onTap1();
                                },
                                child: Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        color: Constant.bgColor,
                                        borderRadius:
                                            BorderRadius.circular(7)),
                                    child: Icon(Icons.remove))),
                            Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10),
                                child: SubText(text: "$text hours")),
                            GestureDetector(
                                onTap: () {
                                  onTap2();
                                },
                                child: Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        color: Constant.bgColor,
                                        borderRadius:
                                            BorderRadius.circular(7)),
                                    child: Icon(Icons.add)))
                          ]))
                    ]))));
  }
}
