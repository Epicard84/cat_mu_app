class LoginResp {
  bool? success;
  String? message;
  String? authToken;
  UserModel? data;

  LoginResp({this.success, this.message, this.authToken, this.data});

  LoginResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    authToken = json['auth_token'];
    data = json['data'] != null ? new UserModel.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['auth_token'] = this.authToken;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class UserModel {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  String? companyName;
  String? zipCode;
  String? city;
  String? country;
  String? state;
  String? mailingAddress;
  String? permanentAddress;
  String? facilityAddress;
  String? status;
  String? lastActivity;
  String? profileImageName;
  String? operatorsLicense;
  String? tbClearance;
  String? miscDoc;
  String? physicalCheckup;
  int? userType;
  int? totalBeds;
  String? createdAt;
  String? updatedAt;
  List<ArchiveClients>? archiveClients;

  UserModel(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phoneNumber,
      this.companyName,
      this.zipCode,
      this.city,
      this.country,
      this.state,
      this.mailingAddress,
      this.permanentAddress,
      this.facilityAddress,
      this.status,
      this.lastActivity,
      this.profileImageName,
      this.operatorsLicense,
      this.tbClearance,
      this.miscDoc,
      this.physicalCheckup,
      this.userType,
      this.totalBeds,
      this.createdAt,
      this.updatedAt,
      this.archiveClients});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    companyName = json['company_name'];
    zipCode = json['zip_code'];
    city = json['city'];
    country = json['country'];
    state = json['state'];
    mailingAddress = json['mailing_address'] ?? "";
    permanentAddress = json['permanent_address'] ?? "";
    facilityAddress = json['facility_address'] ?? "";
    status = json['status'];
    lastActivity = json['last_activity'];
    profileImageName = json['profile_image_name'];
    operatorsLicense = json['operators_license'];
    tbClearance = json['tb_clearance'];
    miscDoc = json['misc_doc'];
    physicalCheckup = json['physical_checkup'];
    userType = json['user_type'];
    totalBeds = json['total_beds'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['archive_clients'] != null) {
      archiveClients = <ArchiveClients>[];
      json['archive_clients'].forEach((v) {
        archiveClients!.add(new ArchiveClients.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['company_name'] = this.companyName;
    data['zip_code'] = this.zipCode;
    data['city'] = this.city;
    data['country'] = this.country;
    data['state'] = this.state;
    data['mailing_address'] = this.mailingAddress;
    data['permanent_address'] = this.permanentAddress;
    data['facility_address'] = this.facilityAddress;
    data['status'] = this.status;
    data['last_activity'] = this.lastActivity;
    data['profile_image_name'] = this.profileImageName;
    data['operators_license'] = this.operatorsLicense;
    data['tb_clearance'] = this.tbClearance;
    data['misc_doc'] = this.miscDoc;
    data['physical_checkup'] = this.physicalCheckup;
    data['user_type'] = this.userType;
    data['total_beds'] = this.totalBeds;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.archiveClients != null) {
      data['archive_clients'] =
          this.archiveClients!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ArchiveClients {
  String? firstName;
  String? lastName;

  ArchiveClients({this.firstName, this.lastName});

  ArchiveClients.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    return data;
  }
}
