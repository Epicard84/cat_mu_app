import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ViewPhoto extends StatelessWidget {
  final String image;
  const ViewPhoto({super.key, required this.image});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(), body: PhotoView(imageProvider: NetworkImage(image)));
  }
}
