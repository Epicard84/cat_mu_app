import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

textFieldDecoration({String hintText = "", suffix = null, prefix = null}) {
  return InputDecoration(
      suffixIcon: suffix,
      prefixIcon: prefix,
      hintText: hintText,
      counterText: "",
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(color: Constant.primaryColor)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(color: Colors.red)),
      disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(color: Colors.grey[600]!)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: const BorderSide(color: Constant.primaryColor)));
}
