import 'dart:convert';
import 'dart:io';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/decoration.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/inputField.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/add_client/add_client_vm.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;

class AddClientScreen extends StatefulWidget {
  const AddClientScreen({super.key});

  @override
  State<AddClientScreen> createState() => _AddClientScreenState();
}

class _AddClientScreenState extends State<AddClientScreen> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();

  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController dob = TextEditingController();
  String strDOB = "";
  ScrollController scrollController = ScrollController();
  // TextEditingController phone = TextEditingController();
  // TextEditingController otherContact = TextEditingController();
  // TextEditingController emergencyInfo = TextEditingController();
  TextEditingController mics = TextEditingController();
  // TextEditingController dischargedDate = TextEditingController();
  String differenceInYears = "";
  bool loading = false;
  final client = HTTPClient();

  addClient(AddClientVM _vm) async {
    var a = [];
    for (var i in _vm.doctorList) {
      a.add(i.toJson());
    }

    var b = [];
    for (var i in _vm.guardianList) {
      b.add(i.toJson());
    }
    try {
      setState(() {
        loading = true;
      });

      // String dobStr =
      //     "${dob.text.substring(0, 2)}-${dob.text.substring(2, 4)}-${dob.text.substring(4, 8)}";

      var data = {
        'first_name': firstName.text,
        'last_name': lastName.text,
        'dob': formatYMDfromMDY(dob.text),
        'gender': _vm.sGender ?? "",
        'guardian': jsonEncode(b),
        "doctor": jsonEncode(a),
        'additional_misc_information': mics.text,
      };

      http.StreamedResponse res = await client.postFileWithToken(
          APIEndpoint.addClient, data,
          fileKey: "documents", files: _vm.docFile);
      var respp = await res.stream.bytesToString();
      print(respp);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        if (jsonResp['success']) {
          // showSnackBar(context, text: "${jsonResp['message']}", success: true);
          await Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DashboardScreen()),
              (Route<dynamic> route) => false);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(respp);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("err >> $e");
      // showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      final _vm = ref.watch(addClientVm);
      var size = MediaQuery.of(context).size;
      return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: CommonAppBar(title: "Add New Clients"),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: FormBuilder(
                    key: formKey,
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height * 2.5),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CommonTextFiled(
                                controller: firstName,
                                hintText: "First name",
                                name: "First name",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: lastName,
                                hintText: "Last name",
                                name: "Last name",
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required()])),
                            SizedBox(height: 20),
                            FormBuilderTextField(
                                controller: dob,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                  DateTextFormatter()
                                ],
                                onChanged: (e) {
                                  if (e!.length == 10) {
                                    // String dobStr =
                                    //     "${dob.text.substring(0, 2)}-${dob.text.substring(2, 4)}-${dob.text.substring(4, 8)}";
                                    DateTime parseDate =
                                        DateTime.parse(formatYMDfromMDY(e));
                                    Duration dur =
                                        DateTime.now().difference(parseDate);
                                    int difference = (dur.inDays / 365).floor();
                                    differenceInYears = "$difference Age";
                                    setState(() {});
                                  } else {
                                    differenceInYears = "";
                                    setState(() {});
                                  }
                                },
                                decoration: textFieldDecoration(
                                    hintText: "Date of Birth : MMDDYYYY",
                                    suffix: null),
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required()]),
                                keyboardType: TextInputType.number,
                                name: "dob"),

                            Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 8),
                                child: Text(differenceInYears)),

                            SizedBox(height: 20),

                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: phone,
                            //     hintText: "Phone number",
                            //     name: "Phone number",
                            //     keyboardType: TextInputType.number,
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            FormBuilderDropdown<String>(
                                decoration:
                                    textFieldDecoration(hintText: "Gender"),
                                name: "gender",
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required()]),
                                items: ["male", "female"]
                                    .map((v) => DropdownMenuItem(
                                        value: v, child: Text(v)))
                                    .toList(),
                                onChanged: (val) => _vm.setGender(val)),
                            Divider(height: 40),
                            ListView.separated(
                                separatorBuilder:
                                    (BuildContext context, int index) =>
                                        const Divider(height: 40),
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: _vm.guardianList.length,
                                itemBuilder: (BuildContext context, int i) {
                                  PersonInfo item = _vm.guardianList[i];
                                  return Container(
                                    width: size.width,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(),
                                            i == 0
                                                ? SizedBox()
                                                : IconButton(
                                                    onPressed: () {
                                                      _vm.removeGuardian(item);
                                                    },
                                                    icon: Icon(
                                                        Icons.delete_outline))
                                          ],
                                        ),
                                        CommonTextFiled(
                                            controller: item.personName,
                                            hintText: "Guardian name",
                                            name: "Guardian name",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(),
                                            ])),
                                        SizedBox(height: 20),
                                        CommonTextFiled(
                                            controller: item.personPhoneNumber,
                                            keyboardType: TextInputType.number,
                                            hintText: "Guardian phone number",
                                            name: "Guardian phone number",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(),
                                            ])),
                                        SizedBox(height: 20),
                                        CommonTextFiled(
                                            maxLine: 4,
                                            keyboardType:
                                                TextInputType.multiline,
                                            controller: item.personAddress,
                                            hintText: "Guardian address",
                                            name: "Guardian address",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(),
                                            ]))
                                      ],
                                    ),
                                  );
                                }),
                            SizedBox(height: 20),
                            CommonButtonWidget(
                                onTap: () {
                                  _vm.addGuardian();
                                },
                                text: "Add Guardian"),
                            Divider(height: 40),
                            ListView.separated(
                                separatorBuilder:
                                    (BuildContext context, int index) =>
                                        const Divider(height: 40),
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: _vm.doctorList.length,
                                itemBuilder: (BuildContext context, int i) {
                                  PersonInfo item = _vm.doctorList[i];
                                  return Container(
                                    width: size.width,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(),
                                            i == 0
                                                ? SizedBox()
                                                : IconButton(
                                                    onPressed: () {
                                                      _vm.removeDoctor(item);
                                                    },
                                                    icon: Icon(
                                                        Icons.delete_outline))
                                          ],
                                        ),
                                        CommonTextFiled(
                                            controller: item.personName,
                                            hintText: "Doctor’s name",
                                            name: "Doctor’s name",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(),
                                            ])),
                                        SizedBox(height: 20),
                                        CommonTextFiled(
                                            controller: item.personPhoneNumber,
                                            keyboardType: TextInputType.number,
                                            hintText: "Doctor’s phone number",
                                            name: "Doctor’s phone number",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(),
                                            ])),
                                        SizedBox(height: 20),
                                        CommonTextFiled(
                                            maxLine: 4,
                                            controller: item.personAddress,
                                            keyboardType:
                                                TextInputType.multiline,
                                            hintText: "Doctor’s address",
                                            name: "Doctor’s address",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(),
                                            ]))
                                      ],
                                    ),
                                  );
                                }),

                            SizedBox(height: 20),
                            CommonButtonWidget(
                                onTap: () {
                                  _vm.addDoctor();
                                },
                                text: "Add Doctor"),
                            SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: otherContact,
                            //     hintText: "Other contact information",
                            //     name: "Other contact information",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            CommonTextFiled(
                                controller: mics,
                                hintText: "Diagnosis",
                                name: "Additional misc. information",
                                validator: null),
                            SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: emergencyInfo,
                            //     hintText: "Some Information for emergency",
                            //     name: "Some Information for emergency",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // FormBuilderDateTimePicker(
                            //     name: 'discharge',
                            //     // controller: dischargedDate,
                            //     initialEntryMode: DatePickerEntryMode.calendar,
                            //     initialValue: null,
                            //     onChanged: (e) {
                            //       setState(() {
                            //         dischargedDate.text =
                            //             "${e!.year}-${e.month}-${e.day}";
                            //       });
                            //     },
                            //     inputType: InputType.date,
                            //     decoration: textFieldDecoration(
                            //         hintText: "Discharged Date",
                            //         suffix: Icon(Icons.calendar_month))),
                            // SizedBox(height: 20),
                            Text("Upload Documents"),
                            SizedBox(height: 10),
                            Row(
                              children: [
                                Container(
                                    decoration: BoxDecoration(
                                        color: Constant.primaryColor,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: IconButton(
                                        onPressed: () {
                                          _vm.openCamera();
                                        },
                                        icon: Icon(Icons.photo_camera_outlined,
                                            color: Colors.white))),
                                SizedBox(width: 20),
                                Container(
                                    decoration: BoxDecoration(
                                        color: Constant.primaryColor,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: IconButton(
                                        onPressed: () {
                                          _vm.pickFile();
                                        },
                                        icon: Icon(Icons.attachment,
                                            color: Colors.white))),
                                SizedBox(width: 20),
                                Platform.isIOS
                                    ? Container(
                                        decoration: BoxDecoration(
                                            color: Constant.primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        child: IconButton(
                                            onPressed: () {
                                              _vm.pickPhotos();
                                            },
                                            icon: Icon(Icons.image_outlined,
                                                color: Colors.white)))
                                    : SizedBox()
                              ],
                            ),

                            SizedBox(height: 20),
                            ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: _vm.docFile.length,
                                padding: EdgeInsets.zero,
                                itemBuilder: (BuildContext context, int i) {
                                  var text = _vm.docFile[i];
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    child: ListTile(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        tileColor: Colors.white,
                                        title: Text(text.path.split('/').last)),
                                  );
                                }),
                            SizedBox(height: 20),
                            loading
                                ? Center(child: CircularProgressIndicator())
                                : CommonButtonWidget(
                                    onTap: () {
                                      if (formKey.currentState!.validate()) {
                                        addClient(_vm);
                                      }
                                    },
                                    text: "Save"),
                            SizedBox(height: 20),
                          ]),
                    )),
              ),
            )),
      );
    });
  }
}
