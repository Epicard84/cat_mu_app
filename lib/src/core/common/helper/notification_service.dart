// import 'package:cat/src/core/constant/constant.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class LocalNotification {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  AndroidNotificationChannel channel = const AndroidNotificationChannel(
      'high_importance_channel', 'High Importance Notifications',
      importance: Importance.high);

  showNotification({required String title, required String body}) async {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
    await flutterLocalNotificationsPlugin.show(
        123,
        title,
        body,
        NotificationDetails(
            android: AndroidNotificationDetails(channel.id, channel.name,
                icon: 'ic_launcher',
                enableVibration: false,
                playSound: false)));
    await flutterLocalNotificationsPlugin.initialize(InitializationSettings(
        android: AndroidInitializationSettings('@drawable/ic_launcher')));
  }
}

// class NotificationService {
//   AndroidNotificationChannel? channel;
//   FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;

//   onInit() async {
//     FirebaseMessaging.instance
//         .requestPermission(sound: true, badge: true, alert: true);
//     await onNotification();
//     await setDeviceToken();
//   }

//   Future<void> setDeviceToken() async {
//     deviceToken = await FirebaseMessaging.instance.getToken() ?? "";
//     print(deviceToken);
//   }

//   onNotification() async {
//     FirebaseMessaging.onMessageOpenedApp
//         .listen((RemoteMessage message) async {});
//     if (!kIsWeb) {
//       channel = const AndroidNotificationChannel(
//           'high_importance_channel', 'High Importance Notifications',
//           importance: Importance.high);
//       flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
//       await flutterLocalNotificationsPlugin!
//           .resolvePlatformSpecificImplementation<
//               AndroidFlutterLocalNotificationsPlugin>()
//           ?.createNotificationChannel(channel!);
//       await FirebaseMessaging.instance
//           .setForegroundNotificationPresentationOptions(
//               alert: true, badge: true, sound: true);
//     }

//     FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
//       RemoteNotification notification = message.notification!;
//       if (!kIsWeb) {
//         flutterLocalNotificationsPlugin!.show(
//             notification.hashCode,
//             notification.title,
//             notification.body,
//             NotificationDetails(
//                 android: AndroidNotificationDetails(channel!.id, channel!.name,
//                     icon: 'ic_launcher')));
//         var initialzationSettingsAndroid =
//             AndroidInitializationSettings('@mipmap/ic_launcher');
//         var initializationSettings =
//             InitializationSettings(android: initialzationSettingsAndroid);
//         flutterLocalNotificationsPlugin!.initialize(initializationSettings);
//       }
//     });
//   }
// }
