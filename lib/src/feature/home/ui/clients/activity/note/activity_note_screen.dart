import 'dart:convert';
import 'dart:io';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/photo_view.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/client.dart';
import 'package:cat/src/feature/home/ui/clients/activity/activitydata_model.dart';
import 'package:cat/src/feature/home/ui/clients/activity/edit/edit_act_note.dart';
import 'package:cat/src/feature/home/ui/clients/medication/add/add_note_vm.dart';
import 'package:cat/src/feature/home/ui/clients/medication/noteScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;

class ActivityNotesScreen extends StatefulWidget {
  final MedData data;
  final bool isChart;
  final bool isNarrative;
  final bool isArchive;
  final DateTime timeNow;
  final ClientData client;
  final bool goBack;

  const ActivityNotesScreen({
    super.key,
    required this.data,
    this.isChart = false,
    this.isNarrative = false,
    this.isArchive = false,
    required this.timeNow,
    required this.client,
    this.goBack = true,
  });

  @override
  State<ActivityNotesScreen> createState() => _ActivityNotesScreenState();
}

class _ActivityNotesScreenState extends State<ActivityNotesScreen> {
  TextEditingController title = TextEditingController();
  ActivtyDataModel? activtyDataModel;
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  bool loading = false;

  final client = HTTPClient();
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await client.getWithToken(
          "${APIEndpoint.activityNote}/${widget.data.scheduleId}/${widget.data.patientId}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          activtyDataModel = ActivtyDataModel.fromJson(jsonResp);
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  // deleteDetail(String detailId) async {
  //   try {
  //     setState(() {
  //       activtyDataModel = null;
  //     });

  //     var res = await await client
  //         .getWithToken("${APIEndpoint.deleteDetail}/$detailId");
  //     if (res.statusCode == 200) {
  //       fetchData();
  //     }
  //   } catch (e) {
  //     print("error >> $e");
  //   } finally {}
  // }

  final PageController controller = PageController();
  int pageIndex = 0;

  addDetails(AddNoteVM _vm) async {
    try {
      setState(() {
        loading = true;
      });
      var data = {
        'description': title.text,
        'patient_id': widget.data.patientId,
        'schedule_id': widget.data.scheduleId,
        'type': '0',
        'date': widget.data.date
      };
      http.StreamedResponse res = await client.postFileWithToken(
          APIEndpoint.activityNewDetails, data,
          fileKey: "detailsImage", files: _vm.docFile);
      var respp = await res.stream.bytesToString();

      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        if (jsonResp['success']) {
          if (widget.goBack) {
            Navigator.pop(context, true);
          } else {
            title.clear();
            fetchData();
          }
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Consumer(builder: (context, ref, child) {
      final _vm = ref.watch(addNoteVm);
      return Scaffold(
          backgroundColor: Constant.bgColor,
          appBar: CommonAppBar(
              isWidget: true,
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BoldText(
                      text: widget.isChart
                          ? "Prog Notes"
                          : widget.isNarrative
                              ? "Dr's Order"
                              : "Notes",
                      textSize: 14),
                  BoldText(
                      text:
                          "${widget.client.firstName} ${widget.client.lastName}",
                      textSize: 14),
                ],
              ),
              title: widget.isChart
                  ? "Prog Notes"
                  : widget.isNarrative
                      ? "Dr's Order"
                      : "Notes",
              isBack: true),
          body: activtyDataModel == null
              ? Center(child: CircularProgressIndicator())
              : SafeArea(
                  child: SingleChildScrollView(
                      child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: FormBuilder(
                    key: formKey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Text(
                          //     "${widget.data.date} , ${widget.timeNow.year}-${widget.timeNow.month.toString().padLeft(2, '0')}-${widget.timeNow.day.toString().padLeft(2, '0')}"),
                          widget.isArchive ||
                                  DateTime.parse(widget.data.date).isBefore(
                                      DateTime.now()
                                          .subtract(Duration(days: 4)))
                              // "${widget.data.date}" !=
                              //     "${widget.timeNow.year}-${widget.timeNow.month.toString().padLeft(2, '0')}-${widget.timeNow.day.toString().padLeft(2, '0')}"
                              ? SizedBox()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CommonTextFiled(
                                      controller: title,
                                      maxLine: 4,
                                      hintText: "Enter Description",
                                      name: "Add Title",
                                      keyboardType: TextInputType.multiline,
                                      validator: FormBuilderValidators.compose([
                                        FormBuilderValidators.required(),
                                      ]),
                                    ),
                                    SizedBox(height: 20),

                                    Text("Upload Documents"),
                                    SizedBox(height: 10),
                                    Row(
                                      children: [
                                        Container(
                                            decoration: BoxDecoration(
                                                color: Constant.primaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            child: IconButton(
                                                onPressed: () {
                                                  _vm.openCamera();
                                                },
                                                icon: Icon(
                                                    Icons.photo_camera_outlined,
                                                    color: Colors.white))),
                                        SizedBox(width: 20),
                                        Container(
                                            decoration: BoxDecoration(
                                                color: Constant.primaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            child: IconButton(
                                                onPressed: () {
                                                  _vm.pickFile();
                                                },
                                                icon: Icon(Icons.attachment,
                                                    color: Colors.white))),
                                        SizedBox(width: 20),
                                        Platform.isIOS
                                            ? Container(
                                                decoration: BoxDecoration(
                                                    color:
                                                        Constant.primaryColor,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                child: IconButton(
                                                    onPressed: () {
                                                      _vm.pickPhotos();
                                                    },
                                                    icon: Icon(
                                                        Icons.image_outlined,
                                                        color: Colors.white)))
                                            : SizedBox()
                                      ],
                                    ),

                                    // GestureDetector(
                                    //   onTap: () {
                                    //     _vm.mediaOption(context);
                                    //   },
                                    //   child: CommonTextFiled(
                                    //       isEnabled: false,
                                    //       suffixIcon: Container(
                                    //           alignment: Alignment.center,
                                    //           height: 60,
                                    //           width: 100,
                                    //           decoration: BoxDecoration(
                                    //               color: Constant.primaryColor,
                                    //               borderRadius:
                                    //                   BorderRadius.circular(
                                    //                       15)),
                                    //           child: Row(
                                    //             mainAxisAlignment:
                                    //                 MainAxisAlignment
                                    //                     .spaceEvenly,
                                    //             children: [
                                    //               Icon(
                                    //                   Icons
                                    //                       .photo_camera_outlined,
                                    //                   color: Colors.white),
                                    //               Icon(Icons.attachment,
                                    //                   color: Colors.white),
                                    //             ],
                                    //           )),
                                    //       hintText: "Documents",
                                    //       name: "Documents",
                                    //       validator: null),
                                    // ),
                                    SizedBox(height: 20),
                                    ListView.builder(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: _vm.docFile.length,
                                        padding: EdgeInsets.zero,
                                        itemBuilder:
                                            (BuildContext context, int i) {
                                          var text = _vm.docFile[i];
                                          return Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5.0),
                                            child: ListTile(
                                                trailing: IconButton(
                                                    onPressed: () {
                                                      _vm.removeFile(i);
                                                    },
                                                    icon: Icon(Icons.close)),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                tileColor: Colors.white,
                                                title: Text(
                                                    text.path.split('/').last)),
                                          );
                                        }),
                                    SizedBox(height: 20),
                                    loading
                                        ? Center(
                                            child: CircularProgressIndicator())
                                        : CommonButtonWidget(
                                            onTap: () {
                                              if (formKey.currentState!
                                                  .validate()) {
                                                addDetails(_vm);
                                              }
                                            },
                                            text: "Save"),
                                  ],
                                ),
                          SizedBox(height: 20),
                          Container(
                              padding: EdgeInsets.all(15),
                              width: size.width,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.white),
                              child: Column(children: [
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Text(
                                        "${formatDateYMD(widget.data.date)}")),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: BoldText(
                                      text:
                                          "${activtyDataModel?.actName ?? ''} "),
                                ),

                                Divider(),
                                activtyDataModel!.dateNote!.length < 1
                                    ? Text(
                                        "${widget.isChart ? "Prog Notes" : widget.isNarrative ? "Orders" : "Notes"} not found.")
                                    : ListView.separated(
                                        separatorBuilder: (context, index) =>
                                            Divider(height: 30),
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount:
                                            activtyDataModel!.dateNote!.length,
                                        padding: EdgeInsets.zero,
                                        itemBuilder:
                                            (BuildContext context, int i) {
                                          DateNote list =
                                              activtyDataModel!.dateNote![i];
                                          return Column(
                                            children: [
                                              Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                        padding: const EdgeInsets
                                                            .only(top: 5),
                                                        child: CircleAvatar(
                                                            radius: 25,
                                                            backgroundColor:
                                                                Constant
                                                                    .primaryColor,
                                                            child: Text(
                                                                getInitials(
                                                                    string:
                                                                        "${list.creatorId?.firstName} ${list.creatorId?.lastName}",
                                                                    limitTo: 2),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white)))),
                                                    SizedBox(width: 10),

                                                    Expanded(
                                                      child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Flexible(
                                                                    child: SubText(
                                                                        text: list.descriptions ??
                                                                            "",
                                                                        textAlign:
                                                                            TextAlign.start)),
                                                                SizedBox(
                                                                    width: 5),
                                                                Row(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    // Text(
                                                                    //     "${list.createdAt}",
                                                                    //     style: TextStyle(
                                                                    //         fontSize:
                                                                    //             11),
                                                                    //     textAlign:
                                                                    //         TextAlign.start),
                                                                    widget.isArchive ||
                                                                            "${widget.data.date}" !=
                                                                                "${widget.timeNow.year}-${widget.timeNow.month.toString().padLeft(2, '0')}-${widget.timeNow.day.toString().padLeft(2, '0')}"
                                                                        ? SizedBox()
                                                                        : Row(
                                                                            children: [
                                                                              IconButton(
                                                                                  onPressed: () async {
                                                                                    var load = await Navigator.push(
                                                                                        context,
                                                                                        MaterialPageRoute(
                                                                                            builder: (context) => EditActivityNoteScreen(
                                                                                                  noteId: "${list.id}",
                                                                                                  desc: "${list.descriptions}",
                                                                                                  image: list.images ?? [],
                                                                                                  data: widget.data,
                                                                                                  timeNow: widget.timeNow,
                                                                                                  client: widget.client,
                                                                                                  isChart: widget.isChart,
                                                                                                  isArchive: widget.isArchive,
                                                                                                  isNarrative: widget.isNarrative,
                                                                                                )));

                                                                                    if (load != null && load == true) {
                                                                                      fetchData();
                                                                                    }
                                                                                  },
                                                                                  icon: Icon(Icons.edit)),
                                                                              // IconButton(
                                                                              //   icon: Icon(Icons.delete),
                                                                              //   onPressed: () {
                                                                              //     deleteDetail("${list.id}");
                                                                              //   },
                                                                              // ),
                                                                            ],
                                                                          ),
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                            Wrap(
                                                                direction: Axis
                                                                    .horizontal,
                                                                spacing: 5,
                                                                runSpacing: 0.0,
                                                                children: [
                                                                  ...list
                                                                      .images!
                                                                      .map((v) => !v
                                                                              .split('/')
                                                                              .last
                                                                              .contains(".")
                                                                          ? SizedBox()
                                                                          : (v.split('.').last != "jpeg") && (v.split('.').last != "jpg") && (v.split('.').last != "png")
                                                                              ? InkWell(
                                                                                  onTap: () {
                                                                                    downloadOpenFile(url: v, fileName: v.split('/').last);
                                                                                  },
                                                                                  child:

                                                                                      // Container(padding: const EdgeInsets.all(10), decoration: BoxDecoration(color: Constant.primaryColor, borderRadius: BorderRadius.all(Radius.circular(15))), child: Icon(Icons.download, color: Colors.white))

                                                                                      Padding(
                                                                                          padding: const EdgeInsets.symmetric(vertical: 20.0),
                                                                                          child: IconButton(
                                                                                            onPressed: () {
                                                                                              downloadOpenFile(url: v, fileName: v.split('/').last);
                                                                                            },
                                                                                            icon: Icon(Icons.download),
                                                                                          )
                                                                                          // Text(v.split('/').last),
                                                                                          ))
                                                                              : GestureDetector(
                                                                                  onTap: () {
                                                                                    showDialog(
                                                                                      context: context,
                                                                                      builder: (BuildContext context) {
                                                                                        return StatefulBuilder(builder: (context, setState2) {
                                                                                          setPageIndex(int index) {
                                                                                            print("index >> $index");
                                                                                            pageIndex = index;
                                                                                            controller.jumpToPage(pageIndex);
                                                                                            setState2(() {});
                                                                                          }

                                                                                          return AlertDialog(
                                                                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                                                            content: Container(
                                                                                                height: 300,
                                                                                                width: 300,
                                                                                                child: PageView.builder(
                                                                                                  controller: controller,
                                                                                                  onPageChanged: (i) {
                                                                                                    setPageIndex(i);
                                                                                                  },
                                                                                                  itemCount: list.images!.length,
                                                                                                  scrollDirection: Axis.horizontal,
                                                                                                  itemBuilder: (context, index) {
                                                                                                    return Container(
                                                                                                      decoration: BoxDecoration(image: DecorationImage(image: Image.network(v, height: 200).image)),
                                                                                                      child: Row(
                                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                        children: [
                                                                                                          IconButton(
                                                                                                            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                                                                                                            onPressed: () {
                                                                                                              if (pageIndex >= 0) {
                                                                                                                setPageIndex(--pageIndex);
                                                                                                              }
                                                                                                            },
                                                                                                          ),
                                                                                                          GestureDetector(
                                                                                                              onTap: () {
                                                                                                                Navigator.push(context, MaterialPageRoute(builder: (context) => ViewPhoto(image: v)));
                                                                                                              },
                                                                                                              child: Container(
                                                                                                                height: 200,
                                                                                                                width: 200,
                                                                                                                color: Colors.transparent,
                                                                                                              )),
                                                                                                          IconButton(
                                                                                                            icon: Icon(Icons.arrow_forward_ios, color: Colors.white),
                                                                                                            onPressed: () {
                                                                                                              setPageIndex(++pageIndex);
                                                                                                            },
                                                                                                          ),
                                                                                                        ],
                                                                                                      ),
                                                                                                    );
                                                                                                  },
                                                                                                )),
                                                                                          );
                                                                                        });
                                                                                      },
                                                                                    );
                                                                                  },
                                                                                  child: Container(margin: EdgeInsets.all(2), height: 100, child: ClipRRect(borderRadius: BorderRadius.circular(10), child: Image.network(v, fit: BoxFit.cover))),
                                                                                ))
                                                                      .toList(),
                                                                ]),
                                                          ]),
                                                    ),

                                                    // Container(
                                                    //     width: size.width * .7,
                                                    //     child: Column(
                                                    //         crossAxisAlignment:
                                                    //             CrossAxisAlignment
                                                    //                 .start,
                                                    //         children: [
                                                    //           SubText(
                                                    //               text:
                                                    //                   list.descriptions ??
                                                    //                       "",
                                                    //               textAlign:
                                                    //                   TextAlign
                                                    //                       .start),
                                                    //           Wrap(
                                                    //               direction: Axis
                                                    //                   .horizontal,
                                                    //               spacing: 5,
                                                    //               runSpacing: 0.0,
                                                    //               children: [
                                                    //                 ...list.images!
                                                    //                     .map((v) =>
                                                    //                         GestureDetector(
                                                    //                           onTap:
                                                    //                               () {
                                                    //                             showDialog(
                                                    //                               context: context,
                                                    //                               builder: (BuildContext context) {
                                                    //                                 return StatefulBuilder(builder: (context, setState2) {
                                                    //                                   setPageIndex(int index) {
                                                    //                                     print("index >> $index");
                                                    //                                     pageIndex = index;
                                                    //                                     controller.jumpToPage(pageIndex);
                                                    //                                     setState2(() {});
                                                    //                                   }

                                                    //                                   return AlertDialog(
                                                    //                                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                    //                                     content: Container(
                                                    //                                         height: 300,
                                                    //                                         width: 300,
                                                    //                                         child: PageView.builder(
                                                    //                                           controller: controller,
                                                    //                                           onPageChanged: (i) {
                                                    //                                             setPageIndex(i);
                                                    //                                           },
                                                    //                                           itemCount: list.images!.length,
                                                    //                                           scrollDirection: Axis.horizontal,
                                                    //                                           itemBuilder: (context, index) {
                                                    //                                             return Container(
                                                    //                                               decoration: BoxDecoration(image: DecorationImage(image: Image.network(list.images![index], height: 200).image)),
                                                    //                                               child: Row(
                                                    //                                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    //                                                 children: [
                                                    //                                                   IconButton(
                                                    //                                                     icon: Icon(Icons.arrow_back_ios, color: Colors.white),
                                                    //                                                     onPressed: () {
                                                    //                                                       if (pageIndex >= 0) {
                                                    //                                                         setPageIndex(--pageIndex);
                                                    //                                                       }
                                                    //                                                     },
                                                    //                                                   ),
                                                    //                                                   IconButton(
                                                    //                                                     icon: Icon(Icons.arrow_forward_ios, color: Colors.white),
                                                    //                                                     onPressed: () {
                                                    //                                                       setPageIndex(++pageIndex);
                                                    //                                                     },
                                                    //                                                   ),
                                                    //                                                 ],
                                                    //                                               ),
                                                    //                                             );
                                                    //                                           },
                                                    //                                         )),
                                                    //                                   );
                                                    //                                 });
                                                    //                               },
                                                    //                             );
                                                    //                           },
                                                    //                           child: Container(
                                                    //                               margin: EdgeInsets.all(2),
                                                    //                               height: 100,
                                                    //                               child: ClipRRect(borderRadius: BorderRadius.circular(10), child: Image.network(v, fit: BoxFit.cover))),
                                                    //                         ))
                                                    //                     .toList(),
                                                    //                 // Padding(
                                                    //                 //     padding:
                                                    //                 //         EdgeInsets
                                                    //                 //             .all(5),
                                                    //                 //     child:
                                                    //                 //         GestureDetector(
                                                    //                 //       onTap: () {
                                                    //                 //         Navigator.push(
                                                    //                 //             context,
                                                    //                 //             MaterialPageRoute(
                                                    //                 //                 builder: (context) =>
                                                    //                 //                     AddDocScreen()));
                                                    //                 //       },
                                                    //                 //       child: Container(
                                                    //                 //           height: 60,
                                                    //                 //           width: 60,
                                                    //                 //           decoration: BoxDecoration(
                                                    //                 //               color: Constant
                                                    //                 //                   .bgColor,
                                                    //                 //               borderRadius:
                                                    //                 //                   BorderRadius.circular(
                                                    //                 //                       7)),
                                                    //                 //           child: Icon(
                                                    //                 //               Icons
                                                    //                 //                   .add,
                                                    //                 //               color: Constant
                                                    //                 //                   .primaryColor)),
                                                    //                 //     ))
                                                    //               ]),
                                                    //         ])),
                                                  ]),
                                            ],
                                          );
                                        }),
                                // CommonButtonWidget(
                                //     onTap: () async {
                                //       var pop = await Navigator.push(
                                //           context,
                                //           MaterialPageRoute(
                                //               builder: (context) =>
                                //                   AddNoteScreen(
                                //                       data: widget.data,
                                //                       isChart: widget.isChart,
                                //                       isNarrative:
                                //                           widget.isNarrative)));

                                //       if (pop != null && pop) {
                                //         Navigator.pop(context, true);
                                //       }
                                //     },
                                //     text:
                                //         "Add ${widget.isChart ? 'Prog Notes' : widget.isNarrative ? 'Narrative' : 'Note'}")
                              ])),
                        ]),
                  ),
                ))));
    });
  }
}
