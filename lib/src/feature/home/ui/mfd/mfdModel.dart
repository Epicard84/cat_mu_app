class MFDResp {
  bool? success;
  String? message;
  List<MFDData>? data;

  MFDResp({this.success, this.message, this.data});

  MFDResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['data'] != null) {
      data = <MFDData>[];
      json['data'].forEach((v) {
        data!.add(new MFDData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MFDData {
  int? id;
  String? masterUserId;
  String? date;
  String? message;
  String? createdAt;
  String? updatedAt;

  MFDData(
      {this.id,
      this.masterUserId,
      this.date,
      this.message,
      this.createdAt,
      this.updatedAt});

  MFDData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    masterUserId = json['master_user_id'].toString();
    date = json['date'];
    message = json['message'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['master_user_id'] = this.masterUserId;
    data['date'] = this.date;
    data['message'] = this.message;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
