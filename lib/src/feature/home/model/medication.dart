class MedicationResp {
  bool? success;
  ClientData? clientData;
  String? date;
  List<Chart>? chart;
  List<Narrative>? narrative;
  List<Note>? note;

  MedicationResp(
      {this.success,
      this.clientData,
      this.date,
      this.chart,
      this.narrative,
      this.note});

  MedicationResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    clientData = json['client_data'] != null
        ? new ClientData.fromJson(json['client_data'])
        : null;
    date = json['date'];
    if (json['chart'] != null) {
      chart = <Chart>[];
      json['chart'].forEach((v) {
        chart!.add(new Chart.fromJson(v));
      });
    }
    if (json['narrative'] != null) {
      narrative = <Narrative>[];
      json['narrative'].forEach((v) {
        narrative!.add(new Narrative.fromJson(v));
      });
    }
    if (json['note'] != null) {
      note = <Note>[];
      json['note'].forEach((v) {
        note!.add(new Note.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.clientData != null) {
      data['client_data'] = this.clientData!.toJson();
    }
    data['date'] = this.date;

    return data;
  }
}

class ClientData {
  int? id;
  String? firstName;
  String? lastName;
  String? dob;
  String? gender;
  String? additionalMiscInformation;
  String? status;
  String? documents;
  String? masterUser;
  List<GetMedicines>? getMedicines;

  ClientData(
      {this.id,
      this.firstName,
      this.lastName,
      this.dob,
      this.gender,
      this.additionalMiscInformation,
      this.status,
      this.documents,
      this.masterUser,
      this.getMedicines});

  ClientData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    gender = json['gender'];
    additionalMiscInformation = json['additional_misc_information'] ?? "";
    status = json['status'];
    documents = json['documents'];
    masterUser = json['master_user'].toString();
    if (json['get_medicines'] != null) {
      getMedicines = <GetMedicines>[];
      json['get_medicines'].forEach((v) {
        getMedicines!.add(new GetMedicines.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['additional_misc_information'] = this.additionalMiscInformation;
    data['status'] = this.status;
    data['documents'] = this.documents;
    data['master_user'] = this.masterUser;
    if (this.getMedicines != null) {
      data['get_medicines'] =
          this.getMedicines!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetMedicines {
  int? id;
  String? clientId;
  String? medicineName;
  String? startDate;
  String? endDate;
  int? timeDays;
  int? status;
  String? descriptions;
  List<String>? docs;
  String? scheduledTime;
  List<GetMedicineSchedule>? getMedicineSchedule;

  GetMedicines(
      {this.id,
      this.clientId,
      this.medicineName,
      this.startDate,
      this.endDate,
      this.timeDays,
      this.status,
      this.descriptions,
      this.docs,
      this.scheduledTime,
      this.getMedicineSchedule});

  GetMedicines.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    medicineName = json['medicine_name'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    timeDays = json['time_days'];
    status = json['status'];
    scheduledTime = json['scheduled_time'];
    descriptions = json['descriptions'];
    // docs = json['docs'];

    if (json['docs'] != null) {
      docs = <String>[];
      json['docs'].forEach((v) {
        docs!.add(v);
      });
    }

    if (json['get_medicine_schedule'] != null) {
      getMedicineSchedule = <GetMedicineSchedule>[];
      json['get_medicine_schedule'].forEach((v) {
        getMedicineSchedule!.add(new GetMedicineSchedule.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['medicine_name'] = this.medicineName;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['time_days'] = this.timeDays;
    data['scheduled_time'] = this.scheduledTime;
    data['status'] = this.status;
    data['descriptions'] = this.descriptions;
    data['docs'] = this.docs;
    if (this.getMedicineSchedule != null) {
      data['get_medicine_schedule'] =
          this.getMedicineSchedule!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetMedicineSchedule {
  int? id;
  int? medicineId;
  String? date;
  String? completeDetails;
  String? scheduledTime;
  String? status;

  GetMedicineSchedule(
      {this.id,
      this.medicineId,
      this.date,
      this.completeDetails,
      this.status,
      this.scheduledTime});

  GetMedicineSchedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    medicineId = json['medicine_id'];
    date = json['date'];
    completeDetails = json['complete_details'];
    scheduledTime = json['scheduled_time'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['medicine_id'] = this.medicineId;
    data['date'] = this.date;
    data['complete_details'] = this.completeDetails ?? "";
    data['scheduled_time'] = this.scheduledTime ?? "";
    data['status'] = this.status;
    return data;
  }
}

class Chart {
  int? id;
  String? date;

  Chart({this.id, this.date});

  Chart.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date'] = this.date;
    return data;
  }
}

class Narrative {
  int? id;
  String? date;

  Narrative({this.id, this.date});

  Narrative.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date'] = this.date;
    return data;
  }
}

class Note {
  int? id;
  String? scheduleId;

  Note({this.id, this.scheduleId});

  Note.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    scheduleId = json['schedule_id'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['schedule_id'] = this.scheduleId;
    return data;
  }
}
