import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class CommonAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool isBack;
  final List<Widget>? actions;
  final bool isWidget;
  final Widget widget;

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
  const CommonAppBar(
      {super.key,
      this.title = "",
      this.isBack = true,
      this.actions,
      this.isWidget = false,
      this.widget = const SizedBox()});

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: AppBar(
            title: isWidget ? widget : BoldText(text: title, textSize: 14),
            centerTitle: true,
            leading: !isBack
                ? null
                : IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Image.asset(AppAsset.back, height: 15)),
            actions: actions,
            elevation: 0,
            backgroundColor: Colors.transparent));
  }
}
