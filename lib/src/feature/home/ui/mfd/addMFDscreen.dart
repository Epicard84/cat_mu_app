// import 'dart:convert';
// import 'package:cat/src/core/common/client/APIhelper.dart';
// import 'package:cat/src/core/common/helper/decoration.dart';
// import 'package:cat/src/core/common/widget/commonAppbar.dart';
// import 'package:cat/src/core/common/widget/commonButton.dart';
// import 'package:cat/src/core/common/widget/common_textfield.dart';
// import 'package:cat/src/core/common/widget/snackbar.dart';
// import 'package:cat/src/core/constant/constant.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_form_builder/flutter_form_builder.dart';

// class AddMFDScreen extends StatefulWidget {
//   const AddMFDScreen({super.key});

//   @override
//   State<AddMFDScreen> createState() => _AddMFDScreenState();
// }

// class _AddMFDScreenState extends State<AddMFDScreen> {
//   TextEditingController mfd = TextEditingController();
//   TextEditingController date = TextEditingController();

//   bool loading = false;
//   final client = HTTPClient();

//   addMFD() async {
//     try {
//       setState(() {
//         loading = true;
//       });

//       var data = {"date": "${date.text}", "message": mfd.text};
//       var res = await client.postWithToken(APIEndpoint.createMFD, data);
//       if (res.statusCode == 200) {
//         var jsonResp = jsonDecode(res.body);
//         if (jsonResp['success']) {
//           showSnackBar(context, text: "${jsonResp['message']}", success: true);
//           Navigator.pop(context, true);
//         } else {
//           showSnackBar(context, text: "${jsonResp['message']}");
//         }
//       } else {
//         var jsonResp = jsonDecode(res.body);
//         showSnackBar(context, text: "${jsonResp['message']}");
//       }
//     } catch (e) {
//       print("err >> $e");
//       // showSnackBar(context, text: ValidationString.somethingWentWrong);
//     } finally {
//       setState(() {
//         loading = false;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         height: double.infinity,
//         width: double.infinity,
//         decoration: BoxDecoration(
//             image: DecorationImage(
//                 image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
//         child: Scaffold(
//             backgroundColor: Colors.transparent,
//             appBar: CommonAppBar(isBack: true, title: "Enter MFD"),
//             body: loading
//                 ? Center(child: CircularProgressIndicator())
//                 : Padding(
//                     padding: const EdgeInsets.all(15.0),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         SizedBox(height: 20),
//                         FormBuilderDateTimePicker(
//                             name: 'date',
//                             initialEntryMode: DatePickerEntryMode.calendar,
//                             initialValue: null,
//                             onChanged: (e) {
//                               setState(() {
//                                 date.text = "${e!.year}-${e.month}-${e.day}";
//                               });
//                             },
//                             inputType: InputType.date,
//                             decoration: textFieldDecoration(
//                                 hintText: "Select Date",
//                                 prefix: Icon(Icons.calendar_month))),
//                         SizedBox(height: 20),
//                         CommonTextFiled(
//                             maxLine: 5,
//                             controller: mfd,
//                             hintText: "Enter your MFD",
//                             name: "mfd",
//                             validator: null),
//                         SizedBox(height: 20),
//                         CommonButtonWidget(
//                             onTap: () {
//                               addMFD();
//                             },
//                             text: "Submit"),
//                         SizedBox(height: 20),
//                       ],
//                     ),
//                   )));
  
  
  
//   }
// }
