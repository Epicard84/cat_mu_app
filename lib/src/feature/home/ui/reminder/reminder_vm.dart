import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/feature/home/ui/reminder/reminderModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final remindereVm = ChangeNotifierProvider.autoDispose<RemindereVM>((ref) {
  return RemindereVM();
});

class RemindereVM with ChangeNotifier {
  List<ReminderData> reminderList = [];
  bool loading = true;
  bool loadReminder = false;
  int loadReminderIndex = 0;

  final client = HTTPClient();
  DateTime selectedDate = DateTime.now();
  DateTime serverDate = DateTime.now();
  TextEditingController text = TextEditingController();

  RemindereVM() {
    onInit();
  }

  onInit() async {
    await setTimeNow();
    getReminderList();
  }

  setTimeNow() async {
    var res = await client.getWithToken("${APIEndpoint.getTime}");
    if (res.statusCode == 200) {
      var jsonResp = jsonDecode(res.body);
      if (jsonResp['success']) {
        selectedDate = DateTime.parse(jsonResp['message']);
        serverDate = DateTime.parse(jsonResp['message']);
        notifyListeners();
      }
    }
  }

  getReminderList({bool isLoad = true}) async {
    try {
      if (isLoad) {
        loading = true;
      }
      notifyListeners();
      var res = await await client.getWithToken(
          "${APIEndpoint.reminderList}/${selectedDate.year}-${selectedDate.month}-${selectedDate.day}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          ReminderResp resp = ReminderResp.fromJson(jsonResp);
          reminderList = resp.data ?? [];
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      loading = false;
      notifyListeners();
    }
  }

  addReminder(context, {required String date}) async {
    try {
      loading = true;
      notifyListeners();
      var data = {"reminder_date": "${date}", "reminder_details": text.text};
      var res = await await client.postWithToken(APIEndpoint.newReminder, data);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          text.clear();
          await getReminderList();
          showSnackBar(context, text: jsonResp['message'], success: true);
        }
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      loading = false;
      notifyListeners();
    }
  }

  updateStatus(context, {required String reminderId}) async {
    try {
      loading = true;
      notifyListeners();
      var res =
          await client.getWithToken("${APIEndpoint.checkReminder}$reminderId");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          await getReminderList();
          showSnackBar(context, text: jsonResp['message'], success: true);
        } else {
          showSnackBar(context, text: jsonResp['message']);
        }
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      loading = false;
      notifyListeners();
    }
  }

  deleteReminder(context,
      {required String reminderId, required int index}) async {
    loadReminder = true;
    loadReminderIndex = index;
    notifyListeners();
    try {
      var res =
          await client.getWithToken("${APIEndpoint.deleteReminder}$reminderId");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          reminderList.removeAt(index);
          // await getReminderList(isLoad: false);
        } else {
          showSnackBar(context, text: jsonResp['message']);
        }
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      // loading = false;
      loadReminder = false;
      notifyListeners();
    }
  }
}
