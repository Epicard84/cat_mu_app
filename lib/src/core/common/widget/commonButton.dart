import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class CommonButtonWidget extends StatelessWidget {
  final Function onTap;
  final String text;
  final Color buttonColors;
  final Color textColors;
  final double? width;
  final double height;
  final double radius;
  final double textSize;

  const CommonButtonWidget(
      {key,
      required this.onTap,
      required this.text,
      this.buttonColors = Constant.primaryColor,
      this.textColors = Colors.white,
      this.width,
      this.height = 50,
      this.radius = 10,
      this.textSize = 16});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
            alignment: Alignment.center,
            height: height,
            width: width ?? size.width,
            decoration: BoxDecoration(
                color: buttonColors,
                borderRadius: BorderRadius.circular(radius)),
            child: BoldText(
                text: text,
                textColor: textColors,
                textSize: textSize)));
  }
}

class ButttonWithIconWidget extends StatelessWidget {
  final Function onTap;
  final String text;
  final Color buttonColors;
  final Color textColors;
  final double? width;
  final double height;
  final double radius;
  final double textSize;
  final IconData icon;
  final MainAxisAlignment align;

  const ButttonWithIconWidget(
      {key,
      required this.onTap,
      required this.text,
      this.buttonColors = Constant.primaryColor,
      this.textColors = Colors.white,
      this.width,
      this.height = 50,
      this.radius = 10,
      this.textSize = 16,
      required this.icon,
      this.align = MainAxisAlignment.spaceEvenly});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
            alignment: Alignment.center,
            height: height,
            width: width ?? size.width,
            decoration: BoxDecoration(
                color: buttonColors,
                borderRadius: BorderRadius.circular(radius)),
            child: Row(
              mainAxisAlignment: align,
              children: [
                Icon(icon, color: Colors.white),
                BoldText(
                    text: text,
                    textColor: textColors,
                    textSize: textSize),
                SizedBox()
              ],
            )));
  }
}
