class SubUserResp {
  bool? success;
  String? message;
  List<SubUserData>? data;

  SubUserResp({this.success, this.message, this.data});

  SubUserResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['data'] != null) {
      data = <SubUserData>[];
      json['data'].forEach((v) {
        data!.add(new SubUserData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SubUserData {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  String? companyName;
  String? city;
  String? country;
  String? state;
  String? permanentAddress;
  String? mailingAddress;
  int? userType;
  int? masterUser;
  String? profileImageName;

  SubUserData(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phoneNumber,
      this.companyName,
      this.city,
      this.country,
      this.state,
      this.permanentAddress,
      this.mailingAddress,
      this.userType,
      this.masterUser,
      this.profileImageName});

  SubUserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'].toString();
    companyName = json['company_name'];
    city = json['city'];
    country = json['country'];
    state = json['state'];
    permanentAddress = json['permanent_address'];
    mailingAddress = json['mailing_address'];
    userType = json['user_type'];
    masterUser = json['master_user'];
    profileImageName = json['profile_image_name'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['company_name'] = this.companyName;
    data['city'] = this.city;
    data['country'] = this.country;
    data['state'] = this.state;
    data['permanent_address'] = this.permanentAddress;
    data['mailing_address'] = this.mailingAddress;
    data['user_type'] = this.userType;
    data['master_user'] = this.masterUser;
    data['profile_image_name'] = this.profileImageName;
    return data;
  }
}
