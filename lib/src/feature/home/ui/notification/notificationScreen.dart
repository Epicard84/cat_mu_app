import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/notification/notificationModel.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List<NotificationData> notificationList = [];
  bool loading = false;
  final client = HTTPClient();

  List<String> seenList = [];

  void initState() {
    super.initState();
    fetchNotification();
  }

  fetchNotification() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(APIEndpoint.notificationList);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          NotificationResp resp = NotificationResp.fromJson(jsonResp);
          notificationList = resp.data ?? [];
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  seenNotification(String id) async {
    seenList.add(id);
    try {
      setState(() {
        loading = true;
      });
      var res = await await client
          .getWithToken("${APIEndpoint.notificationSeen}/$id");
      if (res.statusCode == 200) {
        print(res.body);
        // var jsonResp = jsonDecode(res.body);
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(50),
                child: CommonAppBar(isBack: true, title: "Notification")),
            body: loading
                ? Center(child: CircularProgressIndicator())
                : Padding(
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                    child: ListView.builder(
                        itemCount: notificationList.length,
                        itemBuilder: (BuildContext context, int i) {
                          NotificationData list = notificationList[i];
                          return Padding(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: GestureDetector(
                                onTap: () {
                                  seenNotification("${list.id}");
                                },
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: seenList.contains("${list.id}")
                                          ? Colors.white.withOpacity(.6)
                                          : Colors.white,
                                      borderRadius: BorderRadius.circular(15)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset(AppAsset.calender,
                                                  height: 20),
                                              SizedBox(width: 5),
                                              Text(
                                                  "${formatDate(list.getNotiDetails!.createdAt!, time: true)} ${timezone}",
                                                  style: TextStyle(
                                                      color: Colors.grey))
                                            ],
                                          ),
                                          // Icon(Icons.circle,
                                          //     size: 15,
                                          //     color: Constant.primaryColor)
                                        ],
                                      ),
                                      SizedBox(height: 15),
                                      Text(
                                        list.getNotiDetails?.notification ?? "",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(height: 1.5),
                                      ),
                                    ],
                                  ),
                                ),
                              ));
                        }))));
  }
}
