import 'dart:convert';
import 'dart:io';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/subuser/addSubuserVm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class AddSubuserScreen extends StatefulWidget {
  const AddSubuserScreen({super.key});

  @override
  State<AddSubuserScreen> createState() => _AddSubuserScreenState();
}

class _AddSubuserScreenState extends State<AddSubuserScreen> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();

  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  // TextEditingController company = TextEditingController();
  // TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  // TextEditingController address = TextEditingController();
  // TextEditingController mailAddress = TextEditingController();
  // TextEditingController city = TextEditingController();
  // TextEditingController state = TextEditingController();
  // TextEditingController country = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController cPassword = TextEditingController();
  File? file;

  bool loading = false;
  final client = HTTPClient();

  toggle() {
    obscureText = !obscureText;
    setState(() {});
  }

  addSubUser(AddSubuserVM _vm) async {
    try {
      setState(() {
        loading = true;
      });

      var res = await await client.post5FileWithToken(
        APIEndpoint.addSubUser,
        {
          "first_name": firstName.text,
          "last_name": lastName.text,
          "email": email.text,
          "password": password.text,
          "confirm_password": cPassword.text,
        },

        fileKey: "profile_image",
        files: _vm.file != null ? [_vm.file!] : [],
        fileKey2: "operators_license",
        files2:
            _vm.operatorsLicensefile != null ? [_vm.operatorsLicensefile!] : [],
        fileKey3: "tb_clearance",
        files3: _vm.clearancefile != null ? [_vm.clearancefile!] : [],
        fileKey4: "physical_checkup",
        files4:
            _vm.physicalCheckupfile != null ? [_vm.physicalCheckupfile!] : [],
        fileKey5: "misc_doc",
        files5: _vm.miscDocsFile != null ? [_vm.miscDocsFile!] : [],
        // fileKey2: "profile_image",
        // files2: _vm.file != null ? [_vm.file!] : [],
        // fileKey: "document",
        // files: _vm.docFile
      );
      var respp = await res.stream.bytesToString();
      print("respp >> ${respp}");

      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          Navigator.pop(context, true);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(respp);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      final _vm = ref.watch(addSubuserVm);
      return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: CommonAppBar(title: "Add New Subuser"),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: FormBuilder(
                    key: formKey,
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height * 2),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: GestureDetector(
                                  onTap: () {
                                    _vm.mediaOption(context, isDp: true);
                                  },
                                  child: CircleAvatar(
                                    radius: 40,
                                    backgroundColor: Constant.bgColor,
                                    backgroundImage: _vm.file != null
                                        ? Image.file(_vm.file!).image
                                        : Image.asset(AppAsset.dp).image,
                                    child: Stack(children: [
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: CircleAvatar(
                                          radius: 15,
                                          backgroundColor: Colors.white,
                                          child: Icon(Icons.add,
                                              color: Constant.primaryColor),
                                        ),
                                      ),
                                    ]),
                                  )),
                            ),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: firstName,
                                hintText: "First name",
                                name: "First name",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: lastName,
                                hintText: "Last name",
                                name: "Last name",
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required()])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: company,
                            //     hintText: "Company Name",
                            //     name: "Company Name",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: phone,
                            //     hintText: "Phone number",
                            //     name: "Phone number",
                            //     keyboardType: TextInputType.number,
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                controller: email,
                                hintText: "Email Address",
                                name: "Email Addres",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            // CommonTextFiled(
                            //     maxLine: 4,
                            //     keyboardType: TextInputType.multiline,
                            //     controller: address,
                            //     hintText: "Address",
                            //     name: "Address",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     maxLine: 4,
                            //     keyboardType: TextInputType.multiline,
                            //     controller: mailAddress,
                            //     hintText: "Mailing Address",
                            //     name: "Mailing",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: city,
                            //     hintText: "City",
                            //     name: "City",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: state,
                            //     hintText: "State",
                            //     name: "State",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),
                            // CommonTextFiled(
                            //     controller: country,
                            //     hintText: "Country",
                            //     name: "Country",
                            //     validator: FormBuilderValidators.compose([
                            //       FormBuilderValidators.required(),
                            //     ])),
                            // SizedBox(height: 20),

                            // Text("Upload Documents"),
                            // SizedBox(height: 10),

                            Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: LabelText(label: "Documents")),
                            Text("Operators License"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(0);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.operatorsLicensefile != null
                                        ? Image.file(_vm.operatorsLicensefile!,
                                            height: 100)
                                        : Image.asset(AppAsset.pdf,
                                            height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),
                            SizedBox(height: 15),
                            Text("TB Clearance"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(1);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.clearancefile != null
                                        ? Image.file(_vm.clearancefile!,
                                            height: 100)
                                        : Image.asset(AppAsset.pdf,
                                            height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),
                            SizedBox(height: 15),
                            Text("Physical Checkup"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(2);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.physicalCheckupfile != null
                                        ? Image.file(_vm.physicalCheckupfile!,
                                            height: 100)
                                        : Image.asset(AppAsset.pdf,
                                            height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),
                            SizedBox(height: 15),
                            Text("Miscellaneous Doc"),
                            SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                _vm.pickDoc(3);
                              },
                              child: Stack(
                                  alignment: AlignmentDirectional.topEnd,
                                  children: [
                                    _vm.miscDocsFile != null
                                        ? Image.file(_vm.miscDocsFile!,
                                            height: 100)
                                        : Image.asset(AppAsset.pdf,
                                            height: 100),
                                    CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.white,
                                      child: Icon(Icons.edit,
                                          color: Constant.primaryColor),
                                    )
                                  ]),
                            ),

                            // Row(
                            //   children: [
                            //     Container(
                            //         decoration: BoxDecoration(
                            //             color: Constant.primaryColor,
                            //             borderRadius:
                            //                 BorderRadius.circular(15)),
                            //         child: IconButton(
                            //             onPressed: () {
                            //               _vm.openCamera();
                            //             },
                            //             icon: Icon(Icons.photo_camera_outlined,
                            //                 color: Colors.white))),
                            //     SizedBox(width: 20),
                            //     Container(
                            //         decoration: BoxDecoration(
                            //             color: Constant.primaryColor,
                            //             borderRadius:
                            //                 BorderRadius.circular(15)),
                            //         child: IconButton(
                            //             onPressed: () {
                            //               _vm.pickFile();
                            //             },
                            //             icon: Icon(Icons.attachment,
                            //                 color: Colors.white))),
                            //     SizedBox(width: 20),
                            //     Platform.isIOS
                            //         ? Container(
                            //             decoration: BoxDecoration(
                            //                 color: Constant.primaryColor,
                            //                 borderRadius:
                            //                     BorderRadius.circular(15)),
                            //             child: IconButton(
                            //                 onPressed: () {
                            //                   _vm.pickPhotos();
                            //                 },
                            //                 icon: Icon(Icons.image_outlined,
                            //                     color: Colors.white)))
                            //         : SizedBox()
                            //   ],
                            // ),
                            // SizedBox(height: 20),
                            // ListView.builder(
                            //     shrinkWrap: true,
                            //     physics: NeverScrollableScrollPhysics(),
                            //     itemCount: _vm.docFile.length,
                            //     padding: EdgeInsets.zero,
                            //     itemBuilder: (BuildContext context, int i) {
                            //       var text = _vm.docFile[i];
                            //       return Padding(
                            //         padding: const EdgeInsets.symmetric(
                            //             vertical: 5.0),
                            //         child: ListTile(
                            //             shape: RoundedRectangleBorder(
                            //                 borderRadius: BorderRadius.all(
                            //                     Radius.circular(10))),
                            //             tileColor: Colors.white,
                            //             title: Text(text.path.split('/').last)),
                            //       );
                            //     }),
                            SizedBox(height: 20),

                            CommonTextFiled(
                                maxLength: 4,
                                controller: password,
                                hintText: "Passcode",
                                name: "Password",
                                keyboardType: TextInputType.number,
                                obscureText: obscureText,
                                // suffixIcon: GestureDetector(
                                //     onTap: () {
                                //       toggle();
                                //     },
                                //     child: Icon(obscureText
                                //         ? Icons.visibility_outlined
                                //         : Icons.visibility_off_outlined)),
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ])),
                            SizedBox(height: 20),
                            CommonTextFiled(
                                maxLength: 4,
                                controller: cPassword,
                                keyboardType: TextInputType.number,
                                hintText: "Confirm Passcode",
                                name: "Confirm Password",
                                obscureText: obscureText,
                                // suffixIcon: GestureDetector(
                                //     onTap: () {
                                //       toggle();
                                //     },
                                //     child: Icon(obscureText
                                //         ? Icons.visibility_outlined
                                //         : Icons.visibility_off_outlined)),
                                validator: (e) {
                                  if (e!.isEmpty)
                                    return ValidationString.cPasswordRequired;
                                  if (e != password.text)
                                    return ValidationString.passNotMatched;
                                  return null;
                                }),
                            SizedBox(height: 20),

                            loading
                                ? Center(child: CircularProgressIndicator())
                                : CommonButtonWidget(
                                    onTap: () {
                                      if (formKey.currentState!.validate()) {
                                        addSubUser(_vm);
                                      }
                                    },
                                    text: "Save"),
                            SizedBox(height: 20),
                          ]),
                    )),
              ),
            )),
      );
    });
  }
}
