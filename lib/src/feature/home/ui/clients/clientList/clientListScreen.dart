import 'dart:convert';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/client.dart';
import 'package:cat/src/feature/home/ui/clients/add_client/add_client_screen.dart';
import 'package:cat/src/feature/home/ui/clients/medication/medication_screen.dart';
import 'package:cat/src/feature/home/ui/clients/medication/noteScreen.dart';
import 'package:cat/src/feature/home/ui/clients/medication/past_notes.dart';
import 'package:cat/src/feature/home/ui/profile/profile_screen.dart';
import 'package:cat/src/feature/home/ui/profile/settingScreen.dart';
import 'package:cat/src/feature/home/ui/reminder/reminderScreen.dart';
import 'package:flutter/material.dart';

class ClientListScreen extends StatefulWidget {
  const ClientListScreen({super.key});

  @override
  State<ClientListScreen> createState() => _ClientListScreenState();
}

class _ClientListScreenState extends State<ClientListScreen> {
  List<ClientData> clientList = [];
  bool loading = false;
  final client = HTTPClient();

  void initState() {
    super.initState();
    setTimeNow();
    getClientList();
  }

  DateTime selectedDate = DateTime.now();

  getClientList() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(APIEndpoint.clientList);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          ClientResp resp = ClientResp.fromJson(jsonResp);
          clientList = resp.data ?? [];
        }
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      loading = false;
      setState(() {});
    }
  }

  archiveClient({required String clientId}) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Are you sure?"),
          content: Text("You won't be able to revert this!"),
          actions: [
            TextButton(
              child: Text("No"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text("Yes"),
              onPressed: () async {
                Navigator.pop(context);
                try {
                  setState(() {
                    loading = true;
                  });
                  var res = await client.postWithToken(
                      APIEndpoint.archiveClient, {"client_id": clientId});
                  if (res.statusCode == 200) {
                    getClientList();
                    var jsonResp = jsonDecode(res.body);
                    showSnackBar(context,
                        text: jsonResp['message'], success: true);
                  }
                } finally {
                  setState(() {
                    loading = false;
                  });
                }
              },
            ),
          ],
        );
      },
    );
  }

  DateTime timeNow = DateTime.now();
  setTimeNow() async {
    var res = await client.getWithToken("${APIEndpoint.getTime}");
    if (res.statusCode == 200) {
      var jsonResp = jsonDecode(res.body);
      if (jsonResp['success']) {
        setState(() {
          timeNow = DateTime.parse(jsonResp['message']);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constant.bgColor,
        body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(15),
                child: ListView(
                    physics: BouncingScrollPhysics(),
                    children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(children: [
                        Image.asset(AppAsset.logo, scale: 3),
                        SizedBox(width: 10),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MasterProfileScreen()));
                          },
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SubText(text: "${GlobalVar.cityName}"),
                                BoldText(
                                    text:
                                        "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}")
                              ]),
                        ),
                      ]),
                      IconButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SettingScreen()));
                          },
                          icon: Icon(Icons.settings, color: Colors.black))
                    ],
                  ),
                  SizedBox(height: 15),
                  loading
                      ? Center(child: CircularProgressIndicator())
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            showReminder
                                ? TextButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ReminderScreen()));
                                    },
                                    child:
                                        Text("* Please check Today's reminder"))
                                : SizedBox(),
                            ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: clientList.length,
                                padding: EdgeInsets.zero,
                                itemBuilder: (BuildContext context, int i) {
                                  ClientData user = clientList[i];
                                  return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: ListTile(
                                          leading: IconButton(
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            PastNotesScreen(
                                                                goBack: false,
                                                                client: user,
                                                                timeNow:
                                                                    timeNow,
                                                                data: MedData(
                                                                    key:
                                                                        "${user.id}}",
                                                                    date:
                                                                        "${timeNow.year}-${timeNow.month.toString().padLeft(2, '0')}-${timeNow.day.toString().padLeft(2, '0')}",
                                                                    patientId:
                                                                        "${user.id}",
                                                                    scheduleId:
                                                                        ""),
                                                                isChart:
                                                                    true)));
                                              },
                                              icon: Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Constant
                                                              .primaryColor),
                                                      shape: BoxShape.circle),
                                                  child: Text("P",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Constant
                                                              .primaryColor)))),
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        MedicationScreen(
                                                            clientList:
                                                                clientList,
                                                            clientData: user)));
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          tileColor: Colors.white,
                                          trailing: IconButton(
                                              onPressed: () {
                                                archiveClient(
                                                    clientId: "${user.id}");
                                              },
                                              icon:
                                                  Icon(Icons.archive_outlined)),
                                          title: BoldText(
                                              text:
                                                  "${user.firstName} ${user.lastName}",
                                              align: TextAlign.center)));
                                }),
                            SizedBox(height: 10),
                            clientList.length > 4
                                ? Container()
                                : ButttonWithIconWidget(
                                    icon: Icons.add,
                                    align: MainAxisAlignment.center,
                                    text: "Add Client",
                                    onTap: () async {
                                      var isLoad = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AddClientScreen()));
                                      if (isLoad != null && (isLoad == true)) {
                                        getClientList();
                                      }
                                    })
                          ],
                        ),
                ]))));
  }
}
