import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final editProfileVm = ChangeNotifierProvider.autoDispose<EditProfileVM>((ref) {
  return EditProfileVM();
});

class EditProfileVM with ChangeNotifier {
  File? file;

  File? operatorsLicensefile;
  File? clearancefile;
  File? physicalCheckupfile;
  File? miscDocsFile;
  TextEditingController operatorsLicense = TextEditingController();
  TextEditingController clearance = TextEditingController();
  TextEditingController physicalCheckup = TextEditingController();
  TextEditingController miscDocs = TextEditingController();

  EditProfileVM() {}

  mediaOption(context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                      leading: Icon(Icons.photo_camera_outlined),
                      onTap: () {
                        openCamera();
                        Navigator.pop(context);
                      },
                      title: Text("Camera")),
                  ListTile(
                      leading: Icon(Icons.folder_open_outlined),
                      onTap: () {
                        pickFile();
                        Navigator.pop(context);
                      },
                      title: Text("Gallery")),
                ],
              ),
            ),
          );
        });
  }

  pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      file = File(result.files.single.path!);
    }
    notifyListeners();
  }

  pickDoc(int type) async {
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      if (type == 0) {
        operatorsLicensefile = File(result.files.single.path!);
        operatorsLicense.text = result.files.single.path!;
      } else if (type == 1) {
        clearancefile = File(result.files.single.path!);
        clearance.text = result.files.single.path!;
      } else if (type == 2) {
        physicalCheckupfile = File(result.files.single.path!);
        physicalCheckup.text = result.files.single.path!;
      } else {
        miscDocsFile = File(result.files.single.path!);
        miscDocs.text = result.files.single.path!;
      }
    }
    notifyListeners();
  }

  void openCamera() async {
    var imgCamera = await ImagePicker().pickImage(source: ImageSource.camera);
    if (imgCamera != null) {
      file = File(imgCamera.path);
    }
    notifyListeners();
  }
}
