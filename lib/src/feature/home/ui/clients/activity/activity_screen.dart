import 'dart:convert';
import 'dart:io';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/activity.dart';
import 'package:cat/src/feature/home/model/client.dart' as c;
import 'package:cat/src/feature/home/model/medication.dart';
import 'package:cat/src/feature/home/ui/clients/activity/add/add_activity_screen.dart';
import 'package:cat/src/feature/home/ui/clients/activity/note/activity_note_screen.dart';
import 'package:cat/src/feature/home/ui/clients/clientDetails.dart';
import 'package:cat/src/feature/home/ui/clients/medication/noteScreen.dart';
import 'package:cat/src/feature/home/ui/profile/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:share_plus/share_plus.dart';

class ActivityScreen extends StatefulWidget {
  final List<c.ClientData> clientList;
  final c.ClientData clientData;
  const ActivityScreen(
      {super.key, required this.clientData, required this.clientList});

  @override
  State<ActivityScreen> createState() => _ActivityScreenState();
}

class _ActivityScreenState extends State<ActivityScreen> {
  late c.ClientData selectedClient;
  bool loading = true;
  final client = HTTPClient();
  DateTime selectedDate = DateTime.now();
  List<GetActivities> activityList = [];
  List<String> chartList = [];
  List<String> noteList = [];
  List<String> narrativeList = [];
  MedData selectedDay = MedData();
  MedData selectedMedicine = MedData();
  bool activityLoad = false;
  DateTime timeNow = DateTime.now();
  bool visible = false;

  void initState() {
    super.initState();
    for (c.ClientData i in widget.clientList) {
      if (widget.clientData.id == i.id) {
        selectedClient = i;
      }
    }
    onInit();
  }

  onInit() async {
    await setTimeNow();
    getActivityList();
  }

  setTimeNow() async {
    var res = await client.getWithToken("${APIEndpoint.getTime}");
    if (res.statusCode == 200) {
      var jsonResp = jsonDecode(res.body);
      if (jsonResp['success']) {
        setState(() {
          selectedDate = DateTime.parse(jsonResp['message']);
          timeNow = DateTime.parse(jsonResp['message']);
        });
      }
    }
  }

  getActivityList({bool isLoad = true}) async {
    try {
      if (isLoad) {
        setState(() {
          loading = true;
        });
      }
      String date =
          "${selectedDate.day}-${selectedDate.month}-${selectedDate.year}";
      var res = await await client.getWithToken(
          "${APIEndpoint.acivitySchedule}/${selectedClient.id}/$date");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          ActivityRespModel resp = ActivityRespModel.fromJson(jsonResp);
          activityList = resp.clientData?.getActivities ?? [];
          // for (Chart i in resp.chart ?? []) {
          //   int backendDay = DateFormat("yyyy-MM-dd").parse(i.date ?? "").day;
          //   chartList.add("${backendDay}");
          // }
          for (Note i in resp.note ?? []) {
            noteList.add("${i.scheduleId}");
          }
          // for (Narrative i in resp.narrative ?? []) {
          //   int backendDay = DateFormat("yyyy-MM-dd").parse(i.date ?? "").day;
          //   narrativeList.add("${backendDay}");
          // }
        }
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      if (!isLoad) {
        activityLoad = false;
      }
      setState(() {
        loading = false;
      });
    }
  }

  selectOne(MedData data, {bool isDay = false}) {
    selectedDay = MedData();
    selectedMedicine = MedData();
    isDay ? selectedDay = data : selectedMedicine = data;
    setState(() {});
  }

  updateMedication(
      {required String scheduleId,
      required String index,
      bool done = true}) async {
    String api = done
        ? "${APIEndpoint.updateActivity}/${scheduleId}/${index}"
        : "${APIEndpoint.updateActivityUndo}/${scheduleId}/${index}";

    try {
      setState(() {
        activityLoad = true;
      });
      var res = await await client.getWithToken(api);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);

        if (jsonResp['success']) {
          getActivityList(isLoad: false);
          showSnackBar(context, text: jsonResp['message'], success: true);
        } else {
          setState(() {
            activityLoad = false;
          });
          showSnackBar(context, text: jsonResp['message']);
        }
      }
    } catch (e) {
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {}
  }

  commonTitle(
      {required DateTime date,
      required String clientId,
      required String scheduleId}) {
    return InkWell(
      onTap: () {
        selectOne(
            MedData(
                key: "${date.day}",
                date:
                    "${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
                patientId: "${clientId}",
                scheduleId: "${scheduleId}"),
            isDay: true);
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.0.w),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            selectedDay.key == "${date.day}"
                ? Container(
                    height: 50,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Constant.online, width: 2.0)),
                    alignment: Alignment.center,
                    child: Text("${date.day}".padLeft(2, '0'),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)))
                : Container(
                    alignment: Alignment.center,
                    height: 50.h,
                    child: Text("${date.day}".padLeft(2, '0'),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold))),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                !chartList.contains("${date.day}")
                    ? Container()
                    : Container(
                        alignment: Alignment.center,
                        width: 12.0.sp,
                        height: 12.0.sp,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(100.r)),
                            color: Colors.green),
                        child: Text("P",
                            style: TextStyle(
                                color: Colors.white, fontSize: 10.sp))),
                !narrativeList.contains("${date.day}")
                    ? Container()
                    : Container(
                        alignment: Alignment.center,
                        width: 12.0.sp,
                        height: 12.0.sp,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(100.0.r)),
                            color: Colors.red),
                        child: Text("N",
                            style: TextStyle(
                                color: Colors.white, fontSize: 10.sp))),
              ],
            ),
          ],
        ),
      ),
    );
  }

  statusCircle(
      {required GetActivities activity,
      required int time,
      required DateTime frontendDate,
      required List<GetActivitySchedule> schedule,
      required String clientId}) {
    return Column(
      children: [
        ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: schedule.length,
            padding: EdgeInsets.zero,
            itemBuilder: (BuildContext context, int i) {
              GetActivitySchedule item = schedule[i];
              DateTime backend =
                  DateFormat("yyyy-MM-dd").parse(item.date ?? "");
              DateTime front = frontendDate;
              String backendDay =
                  "${backend.day}-${backend.month}-${backend.year}";
              String frontendDay = "${front.day}-${front.month}-${front.year}";
              String completeString =
                  (item.completeDetails ?? "") + ", , , , , , , , , ,";
              List<String> comList = completeString.split(",");
              // String scheduleString =
              //     (item.scheduledTime ?? "") + ", , , , , , , , , ,";
              // List<String> scheList = scheduleString.split(",");
              List<String> scheList = item.scheduledTime?.split(",") ?? [];
              // print("backendDay  >> ${frontendDay} ${backendDay}");
              return frontendDay != backendDay
                  ? SizedBox()
                  : Column(
                      children: [
                        ...List.generate(scheList.length, (v) {
                          return Padding(
                            padding: EdgeInsets.all(6.0.sp),
                            child: frontendDay != backendDay
                                ? Container()
                                : (activityLoad &&
                                        selectedDay.key == "${item.id}$v")
                                    ? Container(
                                        alignment: Alignment.center,
                                        height: 67,
                                        child: CircularProgressIndicator())
                                    : Column(
                                        children: [
                                          comList[v].length > 1
                                              ? GestureDetector(
                                                  onTap: () {
                                                    selectedDay = MedData(
                                                        key: "${item.id}$v",
                                                        date:
                                                            "${selectedDate.year}-${selectedDate.month.toString().padLeft(2, '0')}-${selectedDate.day.toString().padLeft(2, '0')}",
                                                        patientId:
                                                            "${clientId}",
                                                        scheduleId:
                                                            "${item.id}");
                                                    updateMedication(
                                                        scheduleId:
                                                            "${item.id}",
                                                        index: "$v",
                                                        done: false);
                                                  },
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                          height: 50,
                                                          decoration:
                                                              BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  color: Constant
                                                                      .online),
                                                          alignment:
                                                              Alignment.center,
                                                          child: Text(
                                                              comList[v],
                                                              // getInitials(
                                                              //     string:
                                                              //         "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                                              //     limitTo: 2),
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize:
                                                                      12.sp))),
                                                      // SizedBox(height: 5.h),
                                                      // Text(
                                                      //     scheList[v].length > 1
                                                      //         ? "${scheList[v]}"
                                                      //         : "",
                                                      //     style: TextStyle(
                                                      //         fontSize: 10.sp,
                                                      //         color: Colors
                                                      //             .red[700])),
                                                    ],
                                                  ),
                                                )
                                              : GestureDetector(
                                                  onTap: () {
                                                    selectedDay = MedData(
                                                        key: "${item.id}$v",
                                                        date:
                                                            "${selectedDate.year}-${selectedDate.month.toString().padLeft(2, '0')}-${selectedDate.day.toString().padLeft(2, '0')}",
                                                        patientId:
                                                            "${clientId}",
                                                        scheduleId:
                                                            "${item.id}");
                                                    updateMedication(
                                                        scheduleId:
                                                            "${item.id}",
                                                        index: "$v");
                                                  },
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                          height: 50,
                                                          decoration: BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              border: Border.all(
                                                                  color: Constant
                                                                      .online,
                                                                  width: 2.0)),
                                                          alignment:
                                                              Alignment.center),
                                                      // SizedBox(height: 5),
                                                      // Text(
                                                      //     scheList[v].length > 1
                                                      //         ? "${scheList[v]}"
                                                      //             .replaceAll(
                                                      //                 ":", "")
                                                      //         : "",
                                                      //     style: TextStyle(
                                                      //         fontSize: 10.sp,
                                                      //         color: Colors
                                                      //             .red[700])),
                                                    ],
                                                  ),
                                                ),
                                        ],
                                      ),
                          );
                        }).toList(),
                      ],
                    );
            }),
        ...schedule.map((item) {
          DateTime backend = DateFormat("yyyy-MM-dd").parse(item.date ?? "");
          DateTime front = frontendDate;
          String backendDay = "${backend.day}-${backend.month}-${backend.year}";
          String frontendDay = "${front.day}-${front.month}-${front.year}";
          return frontendDay != backendDay
              ? SizedBox()
              : Stack(
                  alignment: Alignment.topRight,
                  children: [
                    InkWell(
                      onTap: () async {
                        var load = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ActivityNotesScreen(
                                    client: selectedClient,
                                    timeNow: timeNow,
                                    data: MedData(
                                        key:
                                            "${item.id},${schedule.indexOf(item)}",
                                        date: "${item.date}",
                                        patientId: "${clientId}",
                                        scheduleId: "${item.id}"))));

                        if (load != null && load) {
                          getActivityList();
                        }
                      },
                      child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 6.0.h),
                          margin: EdgeInsets.only(
                              top: 2.0.h, left: 5.w, right: 5.w, bottom: 5.h),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.r)),
                              border: Border.all(color: Colors.blue)),
                          child: Text("Note",
                              style: TextStyle(
                                  fontSize: 12.sp, color: Colors.blue))),
                    ),
                    noteList.contains("${item.id}")
                        ? Container(
                            alignment: Alignment.center,
                            width: 12.0.sp,
                            height: 12.0.sp,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.r)),
                                color: Colors.blue),
                            child: Text("N",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 10.sp)))
                        : SizedBox(),
                  ],
                );
        }).toList(),
      ],
    );
  }

  DateTime getPastDate(int day) {
    return selectedDate.subtract(Duration(days: day));
  }

  GlobalKey _containerKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(
          title: "Activity Schedule",
          actions: [
            IconButton(
                onPressed: () async {
                  setState(() {
                    visible = true;
                  });
                  File file = await captureImage(context, _containerKey);
                  Share.shareXFiles([XFile(file.path)]);
                  setState(() {
                    visible = false;
                  });
                },
                icon: Icon(Icons.share, color: Colors.black)),
            IconButton(
                onPressed: () async {
                  setState(() {
                    visible = true;
                  });
                  printImage(await captureImage(context, _containerKey));
                  setState(() {
                    visible = false;
                  });
                },
                icon: Icon(Icons.print, color: Colors.black))
          ],
        ),
        body: Padding(
            padding: EdgeInsets.all(10.sp),
            child: loading
                ? Center(child: CircularProgressIndicator())
                : Column(
                    children: [
                      visible
                          ? SizedBox()
                          : Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8.0.sp),
                                  width: size.width,
                                  decoration: BoxDecoration(
                                      color: Constant.primaryColor,
                                      borderRadius:
                                          BorderRadius.circular(15.r)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            widget.clientList.indexOf(
                                                        selectedClient) ==
                                                    0
                                                ? SizedBox()
                                                : IconButton(
                                                    onPressed: () {
                                                      selectedClient = widget
                                                          .clientList[widget
                                                              .clientList
                                                              .indexOf(
                                                                  selectedClient) -
                                                          1];
                                                      getActivityList();
                                                    },
                                                    icon: Icon(
                                                        Icons.arrow_back_ios,
                                                        color: Colors.white)),
                                            Container(
                                              alignment: Alignment.center,
                                              color: Colors.transparent,
                                              child: GestureDetector(
                                                  onTap: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ClientsDetails(
                                                                    clientData:
                                                                        selectedClient)));
                                                  },
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5.0),
                                                    child: BoldText(
                                                        text:
                                                            "${selectedClient.firstName} ${selectedClient.lastName}",
                                                        textSize: 17,
                                                        textColor:
                                                            Colors.white),
                                                  )),
                                            ),
                                            widget.clientList.indexOf(
                                                        selectedClient) ==
                                                    (widget.clientList.length -
                                                        1)
                                                ? SizedBox()
                                                : IconButton(
                                                    onPressed: () {
                                                      print(
                                                          "${widget.clientList.indexOf(widget.clientData)}");

                                                      selectedClient = widget
                                                          .clientList[widget
                                                              .clientList
                                                              .indexOf(
                                                                  selectedClient) +
                                                          1];
                                                      getActivityList();
                                                    },
                                                    icon: Icon(
                                                        Icons.arrow_forward_ios,
                                                        color: Colors.white)),
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      MasterProfileScreen()));
                                        },
                                        child: Column(
                                          children: [
                                            CircleAvatar(
                                                radius: 30.r,
                                                backgroundColor: Colors.blue,
                                                child: CircleAvatar(
                                                    radius: 25.r,
                                                    backgroundColor:
                                                        Constant.primaryColor,
                                                    child: Text(getInitials(
                                                        string:
                                                            "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                                        limitTo: 2)))),
                                            // SizedBox(height: 5),
                                            // Text(
                                            //     "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                            //     style:
                                            //         TextStyle(color: Colors.white)),
                                            // Text(
                                            //     "${GlobalVar.userData.phoneNumber}",
                                            //     style:
                                            //         TextStyle(color: Colors.white))
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  width: size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15.r),
                                          topLeft: Radius.circular(15.r))),
                                  child: Table(
                                      columnWidths: {
                                        0: FlexColumnWidth(2.w),
                                        4: FlexColumnWidth(.4.w)
                                      },
                                      border: TableBorder(
                                          horizontalInside: BorderSide(
                                              width: 1,
                                              color: Colors.grey[400]!,
                                              style: BorderStyle.solid)),
                                      children: [
                                        TableRow(
                                          children: <Widget>[
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      selectedDate =
                                                          selectedDate.subtract(
                                                              Duration(
                                                                  days: 1));
                                                      getActivityList();
                                                    },
                                                    icon: Icon(
                                                        Icons.arrow_back_ios)),
                                                Container(
                                                    alignment: Alignment.center,
                                                    height: 60.h,
                                                    child: Text(
                                                        DateFormat('MMM, yyyy')
                                                            .format(
                                                                selectedDate),
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                            fontSize: 11.sp,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)))
                                              ],
                                            ),
                                            commonTitle(
                                                date: getPastDate(2),
                                                clientId:
                                                    "${widget.clientData.id}",
                                                scheduleId: ""),
                                            commonTitle(
                                                date: getPastDate(1),
                                                clientId:
                                                    "${widget.clientData.id}",
                                                scheduleId: ""),
                                            commonTitle(
                                                date: getPastDate(0),
                                                clientId:
                                                    "${widget.clientData.id}",
                                                scheduleId: ""),
                                            Container(
                                              alignment: Alignment.center,
                                              height: 60.h,
                                              child: IconButton(
                                                onPressed: () {
                                                  if ("${selectedDate.day}-${selectedDate.month}" !=
                                                      "${timeNow.day}-${timeNow.month}") {
                                                    selectedDate = selectedDate
                                                        .add(Duration(days: 1));
                                                    getActivityList();
                                                  }
                                                },
                                                icon: Icon(
                                                    Icons.arrow_forward_ios),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ]),
                                )
                              ],
                            ),
                      Flexible(
                        child: SingleChildScrollView(
                          child: RepaintBoundary(
                            key: _containerKey,
                            child: Column(
                              children: [
                                !visible
                                    ? SizedBox()
                                    : Container(
                                        padding: EdgeInsets.all(8.0.sp),
                                        width: size.width,
                                        decoration: BoxDecoration(
                                            color: Constant.primaryColor,
                                            borderRadius:
                                                BorderRadius.circular(15.r)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  widget.clientList.indexOf(
                                                              selectedClient) ==
                                                          0
                                                      ? SizedBox()
                                                      : IconButton(
                                                          onPressed: () {
                                                            selectedClient = widget
                                                                .clientList[widget
                                                                    .clientList
                                                                    .indexOf(
                                                                        selectedClient) -
                                                                1];
                                                            getActivityList();
                                                          },
                                                          icon: Icon(
                                                              Icons
                                                                  .arrow_back_ios,
                                                              color: Colors
                                                                  .white)),
                                                  Container(
                                                    alignment: Alignment.center,
                                                    color: Colors.transparent,
                                                    child: GestureDetector(
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      ClientsDetails(
                                                                          clientData:
                                                                              selectedClient)));
                                                        },
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 5.0),
                                                          child: BoldText(
                                                              textSize: 17,
                                                              text:
                                                                  "${selectedClient.firstName} ${selectedClient.lastName}",
                                                              textColor:
                                                                  Colors.white),
                                                        )),
                                                  ),
                                                  widget.clientList.indexOf(
                                                              selectedClient) ==
                                                          (widget.clientList
                                                                  .length -
                                                              1)
                                                      ? SizedBox()
                                                      : IconButton(
                                                          onPressed: () {
                                                            print(
                                                                "${widget.clientList.indexOf(widget.clientData)}");

                                                            selectedClient = widget
                                                                .clientList[widget
                                                                    .clientList
                                                                    .indexOf(
                                                                        selectedClient) +
                                                                1];
                                                            getActivityList();
                                                          },
                                                          icon: Icon(
                                                              Icons
                                                                  .arrow_forward_ios,
                                                              color: Colors
                                                                  .white)),
                                                ],
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MasterProfileScreen()));
                                              },
                                              child: Column(
                                                children: [
                                                  CircleAvatar(
                                                      radius: 30.r,
                                                      backgroundColor:
                                                          Colors.blue,
                                                      child: CircleAvatar(
                                                          radius: 25.r,
                                                          backgroundColor:
                                                              Constant
                                                                  .primaryColor,
                                                          child: Text(getInitials(
                                                              string:
                                                                  "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                                              limitTo: 2)))),
                                                  // SizedBox(height: 5),
                                                  // Text(
                                                  //     "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                                  //     style: TextStyle(
                                                  //         color: Colors.white)),
                                                  // Text(
                                                  //     "${GlobalVar.userData.phoneNumber}",
                                                  //     style: TextStyle(
                                                  //         color: Colors.white))
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),

                                //! MEDICINE LIST
                                Container(
                                  width: size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(15.r),
                                          bottomRight: Radius.circular(15.r))),
                                  child: Stack(
                                    alignment: Alignment.centerRight,
                                    children: [
                                      Positioned.fill(
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                left: 255.w,
                                                right: 30.w,
                                                top: 5.h,
                                                bottom: 0),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(15.r)),
                                                color: selectedDate.day ==
                                                        timeNow.day
                                                    ? Constant.primaryColor
                                                        .withOpacity(.2)
                                                    : Colors.white)),
                                      ),
                                      activityList.length < 1
                                          ? Container()
                                          : Table(
                                              columnWidths: {
                                                0: FlexColumnWidth(2.w),
                                                4: FlexColumnWidth(.4.w)
                                              },
                                              border: TableBorder(
                                                  horizontalInside: BorderSide(
                                                      width: 1,
                                                      color: Colors.grey[400]!,
                                                      style:
                                                          BorderStyle.solid)),
                                              children: [
                                                !visible
                                                    ? TableRow(children: [
                                                        SizedBox(),
                                                        SizedBox(),
                                                        SizedBox(),
                                                        SizedBox(),
                                                        SizedBox(),
                                                      ])
                                                    : TableRow(
                                                        children: <Widget>[
                                                          Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              IconButton(
                                                                  onPressed:
                                                                      () {
                                                                    selectedDate =
                                                                        selectedDate.subtract(Duration(
                                                                            days:
                                                                                1));
                                                                    getActivityList();
                                                                  },
                                                                  icon: Icon(Icons
                                                                      .arrow_back_ios)),
                                                              Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  height: 60.h,
                                                                  child: Text(
                                                                      DateFormat(
                                                                              'MMMM, yyyy')
                                                                          .format(
                                                                              selectedDate),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      style: TextStyle(
                                                                          fontSize: 11
                                                                              .sp,
                                                                          fontWeight:
                                                                              FontWeight.bold)))
                                                            ],
                                                          ),
                                                          commonTitle(
                                                              date: getPastDate(
                                                                  2),
                                                              clientId:
                                                                  "${widget.clientData.id}",
                                                              scheduleId: ""),
                                                          commonTitle(
                                                              date: getPastDate(
                                                                  1),
                                                              clientId:
                                                                  "${widget.clientData.id}",
                                                              scheduleId: ""),
                                                          commonTitle(
                                                              date: getPastDate(
                                                                  0),
                                                              clientId:
                                                                  "${widget.clientData.id}",
                                                              scheduleId: ""),
                                                          Container(
                                                            alignment: Alignment
                                                                .center,
                                                            height: 60.h,
                                                            child: IconButton(
                                                              onPressed: () {
                                                                if ("${selectedDate.day}-${selectedDate.month}" !=
                                                                    "${timeNow.day}-${timeNow.month}") {
                                                                  selectedDate =
                                                                      selectedDate.add(
                                                                          Duration(
                                                                              days: 1));
                                                                  getActivityList();
                                                                }
                                                              },
                                                              icon: Icon(Icons
                                                                  .arrow_forward_ios),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                for (GetActivities i
                                                    in activityList)
                                                  i.status == 1
                                                      ? TableRow(
                                                          children: <Widget>[
                                                              SizedBox(),
                                                              SizedBox(),
                                                              SizedBox(),
                                                              SizedBox(),
                                                              SizedBox()
                                                            ])
                                                      : TableRow(
                                                          children: <Widget>[
                                                              GestureDetector(
                                                                onTap: () {
                                                                  // Navigator.push(
                                                                  //     context,
                                                                  //     MaterialPageRoute(
                                                                  //         builder: (context) =>
                                                                  //             MedicationDetailScreen(activity: i)));
                                                                },
                                                                child:
                                                                    Container(
                                                                  color: Colors
                                                                      .transparent,
                                                                  child: Column(
                                                                    children: [
                                                                      SizedBox(
                                                                          height:
                                                                              30.h),
                                                                      Text(
                                                                          "${i.activityName}",
                                                                          style:
                                                                              TextStyle(fontWeight: FontWeight.bold)),
                                                                      SizedBox(
                                                                          height:
                                                                              10),
                                                                      Text(
                                                                          "${i.descriptions ?? ""}",
                                                                          style:
                                                                              TextStyle(fontSize: 11.sp)),
                                                                      SizedBox(
                                                                          height:
                                                                              30.h),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              statusCircle(
                                                                  activity: i,
                                                                  clientId:
                                                                      i.clientId ??
                                                                          "",
                                                                  time:
                                                                      i.timeDays ??
                                                                          0,
                                                                  frontendDate:
                                                                      getPastDate(
                                                                          2),
                                                                  schedule:
                                                                      i.getActivitySchedule ??
                                                                          []),
                                                              statusCircle(
                                                                  activity: i,
                                                                  clientId:
                                                                      i.clientId ??
                                                                          "",
                                                                  time:
                                                                      i.timeDays ??
                                                                          0,
                                                                  frontendDate:
                                                                      getPastDate(
                                                                          1),
                                                                  schedule:
                                                                      i.getActivitySchedule ??
                                                                          []),
                                                              statusCircle(
                                                                  activity: i,
                                                                  clientId:
                                                                      i.clientId ??
                                                                          "",
                                                                  time:
                                                                      i.timeDays ??
                                                                          0,
                                                                  frontendDate:
                                                                      getPastDate(
                                                                          0),
                                                                  schedule:
                                                                      i.getActivitySchedule ??
                                                                          []),
                                                              Container()
                                                            ]),
                                              ]),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 20.h),

                                CommonButtonWidget(
                                    onTap: () async {
                                      var load = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AddActivityScreen(
                                                      item: activityList,
                                                      client: selectedClient)));

                                      if (load != null && load) {
                                        getActivityList();
                                      }
                                    },
                                    text: "Manage Activities"),
                                SizedBox(height: 20),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )));
  }
}
