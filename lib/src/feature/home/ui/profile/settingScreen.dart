import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/profile/disableAccount.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cat/src/feature/auth/login/loginScreen.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(title: "Settings"),
        body: Column(children: [
          ListTile(
            leading: Icon(Icons.delete),
            title: Text("Deactivate Account"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DisableAccount()));
            },
          ),
          ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                await prefs.remove("authToken");
                await Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => LoginScreen()),
                    (Route<dynamic> route) => false);
              })
        ]));
  }
}
