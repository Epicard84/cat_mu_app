import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/auth/login/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class NewPassworsScreen extends StatefulWidget {
  final String email;
  final String otp;
  const NewPassworsScreen({super.key, required this.email, required this.otp});

  @override
  State<NewPassworsScreen> createState() => _NewPassworsScreenState();
}

class _NewPassworsScreenState extends State<NewPassworsScreen> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  TextEditingController cPassword = TextEditingController();
  TextEditingController password = TextEditingController();
  bool obscureText = true;
  toggle() {
    obscureText = !obscureText;
    setState(() {});
  }

  bool loading = false;
  final client = HTTPClient();

  onNewPassword(BuildContext context,
      {required String email,
      required String password,
      required String cPassword,
      required String otp}) async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.postMethod(APIEndpoint.newPassword, {
        'email': email,
        'password': password,
        'confirm_password': cPassword,
        'otp': otp
      });
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          await Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => LoginScreen()),
              (Route<dynamic> route) => false);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
            child: SingleChildScrollView(
                child: Column(children: [
              CommonAppBar(isBack: true),
              Padding(
                  padding: EdgeInsets.all(25),
                  child: FormBuilder(
                    key: formKey,
                    child: Column(children: [
                      SizedBox(height: 50),
                      Image.asset(AppAsset.forgetIcons, height: 70),
                      SizedBox(height: 75),
                      BoldText(text: "Enter new password!"),
                      SizedBox(height: 20),
                      SubText(
                          text:
                              "Your new password must be deferent from \nthe previous password."),
                      SizedBox(height: 50),
                      CommonTextFiled(
                          hintText: "New password",
                          name: "New password",
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(),
                            FormBuilderValidators.minLength(6),
                          ]),
                          controller: password,
                          obscureText: obscureText,
                          suffixIcon: GestureDetector(
                              onTap: () {
                                toggle();
                              },
                              child: Icon(obscureText
                                  ? Icons.visibility_outlined
                                  : Icons.visibility_off_outlined))),
                      SizedBox(height: 20),
                      CommonTextFiled(
                          hintText: "Confirm password",
                          name: "Confirm password",
                          validator: (e) {
                            if (e!.isEmpty)
                              return ValidationString.cPasswordRequired;
                            if (e != password.text)
                              return ValidationString.passNotMatched;
                            return null;
                          },
                          controller: cPassword,
                          obscureText: obscureText,
                          suffixIcon: GestureDetector(
                              onTap: () {
                                toggle();
                              },
                              child: Icon(obscureText
                                  ? Icons.visibility_outlined
                                  : Icons.visibility_off_outlined))),
                      Align(
                          heightFactor: 3,
                          alignment: Alignment.bottomCenter,
                          child: loading
                              ? CircularProgressIndicator()
                              : CommonButtonWidget(
                                  onTap: () {
                                    if (formKey.currentState!.validate()) {
                                      onNewPassword(context,
                                          email: widget.email,
                                          password: password.text,
                                          cPassword: cPassword.text,
                                          otp: widget.otp);
                                    }
                                  },
                                  text: "Change password"))
                    ]),
                  ))
            ]))));
  }
}
