import 'dart:convert';
import 'dart:io';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/medication/edit_note_vm.dart';
import 'package:cat/src/feature/home/ui/clients/medication/model/medicineNote.dart';
import 'package:cat/src/feature/home/ui/clients/medication/noteScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;

import '../../../../model/client.dart';

class EditActivityNoteScreen extends ConsumerStatefulWidget {
  final String desc;
  final String noteId;
  final List<String> image;
  final MedData data;
  final bool isChart;
  final bool isNarrative;
  final bool isArchive;
  final DateTime timeNow;
  final ClientData client;

  const EditActivityNoteScreen({
    required this.noteId,
    required this.desc,
    required this.image,
    super.key,
    required this.data,
    this.isChart = false,
    this.isNarrative = false,
    this.isArchive = false,
    required this.timeNow,
    required this.client,
  });

  @override
  ConsumerState<EditActivityNoteScreen> createState() => _EditNoteScreenState();
}

class _EditNoteScreenState extends ConsumerState<EditActivityNoteScreen> {
  TextEditingController title = TextEditingController();
  MedicineData? medicineNote;
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  bool loading = false;
  final client = HTTPClient();

  @override
  void initState() {
    super.initState();
    title.text = widget.desc;
    ref.read(editNoteVm).saveData(widget.image);
  }

  int pageIndex = 0;

  updateNote(EditNoteVM _vm) async {
    try {
      setState(() {
        loading = true;
      });

// [type: note=0, chart=1, narrative=2]

      var data = {
        'details_id': widget.noteId,
        'description': title.text,
        'docs': _vm.docs
            .toString()
            .replaceAll("[", "")
            .replaceAll("]", "")
            .replaceAll(" ", "")
      };
      http.StreamedResponse res = await client.postFileWithToken(
          APIEndpoint.activityEditDetails, data,
          fileKey: "detailsImage", files: _vm.docFile);
      var respp = await res.stream.bytesToString();
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        if (jsonResp['success']) {
          Navigator.pop(context, true);
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
        }
      }
    } catch (e) {
      print("err >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final _vm = ref.watch(editNoteVm);
      return Scaffold(
          backgroundColor: Constant.bgColor,
          appBar: CommonAppBar(
              isWidget: true,
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BoldText(
                      text:
                          "Edit ${widget.isChart ? "Prog Notes" : widget.isNarrative ? "Dr's Order" : "Notes"}",
                      textSize: 14),
                  BoldText(
                      text:
                          "${widget.client.firstName} ${widget.client.lastName}",
                      textSize: 14),
                ],
              ),
              title: widget.isChart
                  ? "Prog Notes"
                  : widget.isNarrative
                      ? "Dr's Order"
                      : "Notes",
              isBack: true),
          body: SafeArea(
              child: SingleChildScrollView(
                  child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: FormBuilder(
              key: formKey,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widget.isArchive ||
                            DateTime.parse(widget.data.date).isBefore(
                                DateTime.now().subtract(Duration(days: 4)))
                        // "${widget.data.date}" !=
                        //     "${widget.timeNow.year}-${widget.timeNow.month.toString().padLeft(2, '0')}-${widget.timeNow.day.toString().padLeft(2, '0')}"
                        ? SizedBox()
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CommonTextFiled(
                                controller: title,
                                maxLine: 4,
                                hintText: "Enter Description",
                                keyboardType: TextInputType.multiline,
                                name: "Add Title",
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ]),
                              ),
                              SizedBox(height: 20),
                              Text("Upload Documents"),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Container(
                                      decoration: BoxDecoration(
                                          color: Constant.primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: IconButton(
                                          onPressed: () {
                                            _vm.openCamera();
                                          },
                                          icon: Icon(
                                              Icons.photo_camera_outlined,
                                              color: Colors.white))),
                                  SizedBox(width: 20),
                                  Container(
                                      decoration: BoxDecoration(
                                          color: Constant.primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: IconButton(
                                          onPressed: () {
                                            _vm.pickFile();
                                          },
                                          icon: Icon(Icons.attachment,
                                              color: Colors.white))),
                                  SizedBox(width: 20),
                                  Platform.isIOS
                                      ? Container(
                                          decoration: BoxDecoration(
                                              color: Constant.primaryColor,
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          child: IconButton(
                                              onPressed: () {
                                                _vm.pickPhotos();
                                              },
                                              icon: Icon(Icons.image_outlined,
                                                  color: Colors.white)))
                                      : SizedBox()
                                ],
                              ),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: _vm.exitsFile.length,
                                  padding: EdgeInsets.zero,
                                  itemBuilder: (BuildContext context, int i) {
                                    var text = _vm.exitsFile[i];
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: ListTile(
                                          trailing: _vm.docs.contains(
                                                  text.split('/').last)
                                              ? Icon(Icons.check_box,
                                                  color: Constant.primaryColor)
                                              : Icon(Icons
                                                  .check_box_outline_blank),
                                          onTap: () {
                                            _vm.docs.contains(
                                                    text.split('/').last)
                                                ? _vm.removeImage(
                                                    text.split('/').last)
                                                : _vm.addImage(
                                                    text.split('/').last);
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          tileColor: Colors.white,
                                          title: Text(text.split('/').last)),
                                    );
                                  }),
                              SizedBox(height: 20),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: _vm.docFile.length,
                                  padding: EdgeInsets.zero,
                                  itemBuilder: (BuildContext context, int i) {
                                    var text = _vm.docFile[i];
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: ListTile(
                                          trailing: IconButton(
                                              onPressed: () {
                                                _vm.removeFile(i);
                                              },
                                              icon: Icon(Icons.close)),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          tileColor: Colors.white,
                                          title:
                                              Text(text.path.split('/').last)),
                                    );
                                  }),
                              SizedBox(height: 20),
                              loading
                                  ? Center(child: CircularProgressIndicator())
                                  : CommonButtonWidget(
                                      onTap: () {
                                        if (formKey.currentState!.validate()) {
                                          updateNote(_vm);
                                        }
                                      },
                                      text: "Update"),
                            ],
                          ),
                    SizedBox(height: 20),
                  ]),
            ),
          ))));
    });
  }
}
