import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/photo_view.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/medication.dart';
import 'package:flutter/material.dart';
import 'model/medicineDetail.dart';

class MedicationDetailScreen extends StatefulWidget {
  final GetMedicines medicine;
  const MedicationDetailScreen({super.key, required this.medicine});

  @override
  State<MedicationDetailScreen> createState() => _MedicationDetailScreenState();
}

class _MedicationDetailScreenState extends State<MedicationDetailScreen> {
  MedicineData? detail;
  bool loading = false;
  final client = HTTPClient();

  void initState() {
    super.initState();
    getDetail();
  }

  getDetail() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client
          .getWithToken("${APIEndpoint.medicineDetail}/${widget.medicine.id}");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          MedicineDetail resp = MedicineDetail.fromJson(jsonResp);
          detail = resp.data;
        }
      }
    } catch (e) {
      print("error >> $e");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constant.bgColor,
        appBar: CommonAppBar(title: "${widget.medicine.medicineName}"),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(children: [
                      Padding(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          child: Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Image.asset(AppAsset.calender,
                                            height: 20),
                                        SizedBox(width: 5),
                                        Text("${detail?.startDate}",
                                            style:
                                                TextStyle(color: Colors.grey))
                                      ],
                                    ),
                                    Container(
                                      padding: const EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Constant.primaryColor),
                                          shape: BoxShape.circle),
                                      child: Text(
                                          getInitials(
                                              string:
                                                  "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                              limitTo: 2),
                                          style: TextStyle(
                                              color: Constant.primaryColor)),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Text("${detail?.descriptions ?? ""}",
                                    style: TextStyle(
                                        height: 1.5, color: Color(0xff545C7A)),
                                    textAlign: TextAlign.start),
                                SizedBox(height: 15),

                                // Text(detail!.docs.toString()),
                                ...detail!.docs!
                                    .map((v) => !v.split('/').last.contains(".")
                                        ? SizedBox()
                                        : (v.split('.').last != "jpeg") &&
                                                (v.split('.').last != "jpg") &&
                                                (v.split('.').last != "png")
                                            ? ListTile(
                                                title: Text(v.split('/').last),
                                                leading:
                                                    Icon(Icons.description),
                                                trailing: Icon(Icons.download,
                                                    color:
                                                        Constant.primaryColor),
                                                onTap: () {
                                                  downloadOpenFile(
                                                      url: v, fileName: v);
                                                },
                                              )
                                            : GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ViewPhoto(
                                                                  image: v)));
                                                },
                                                child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: Image.network(v,
                                                        fit: BoxFit.cover)),
                                              ))
                                    .toList(),

                                // Image.asset(AppAsset.medicine),
                                SizedBox(height: 15),
                                // Container(
                                //     padding: const EdgeInsets.all(5.0),
                                //     decoration: BoxDecoration(
                                //         border:
                                //             Border.all(width: 1, color: Colors.grey),
                                //         borderRadius:
                                //             BorderRadius.all(Radius.circular(15))),
                                //     child: ListTile(
                                //         leading: Image.asset(AppAsset.pdf,
                                //             color: Colors.blue),
                                //         title: Text("Otezla Manual"),
                                //         subtitle: Text("12 MB"))),
                                SizedBox(height: 15),
                              ],
                            ),
                          )),
                      SizedBox(height: 20),
                      // ButttonWithIconWidget(
                      //     icon: Icons.add,
                      //     align: MainAxisAlignment.center,
                      //     onTap: () {},
                      //     text: "Upload Image/documents")
                    ])),
              ));
  }
}
