import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final editNoteVm = ChangeNotifierProvider.autoDispose<EditNoteVM>((ref) {
  return EditNoteVM();
});

class EditNoteVM with ChangeNotifier {
  List<File> docFile = [];
  List<String> exitsFile = [];
  List<String> existFile2 = [];
  List<String> docs = [];

  removeImage(String index) {
    docs.remove(index);
    notifyListeners();
  }

  addImage(String index) {
    docs.add(index);
    notifyListeners();
  }

  saveData(List<String> clientData) {
    print("********saveData**********");
    exitsFile = clientData;
    for (var i in clientData) {
      existFile2.add(i.split('/').last);
      docs.add(i.split('/').last);
    }
  }

  mediaOption(context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                      leading: Icon(Icons.photo_camera_outlined),
                      onTap: () {
                        openCamera();
                        Navigator.pop(context);
                      },
                      title: Text("Camera")),
                  ListTile(
                      leading: Icon(Icons.folder_open_outlined),
                      onTap: () {
                        pickFile();
                        Navigator.pop(context);
                      },
                      title: Text("Gallery")),
                ],
              ),
            ),
          );
        });
  }

  pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      docFile.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  pickPhotos() async {
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      docFile.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  removeFile(int index) async {
    docFile.removeAt(index);
    notifyListeners();
  }

  void openCamera() async {
    var imgCamera = await ImagePicker().pickImage(source: ImageSource.camera);
    if (imgCamera != null) {
      docFile.add(File(imgCamera.path));
    }
    notifyListeners();
  }
}
