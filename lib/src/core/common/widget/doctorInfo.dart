import 'package:cat/src/core/common/widget/commonCont.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/client.dart';
import 'package:flutter/material.dart';

class DoctorInfo extends StatelessWidget {
  final List<Doctor> doctors;

  const DoctorInfo({super.key, required this.doctors});
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: LabelText(label: "Doctor’s Information")),

// for i in doctor
      ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: doctors.length,
          padding: EdgeInsets.zero,
          itemBuilder: (BuildContext context, int i) {
            var item = doctors[i];
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: CommonContainer(
                  image: AssetImage(AppAsset.roundEffect2),
                  child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 30),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BoldText(
                                      text: "${item.doctorName}",
                                      textColor: Colors.white)
                                ]),
                            SizedBox(height: 15),
                            CommonRichText(
                                maxLines: 2,
                                text1: "Address: ",
                                text2: "${item.doctorAddress}"),
                            SizedBox(height: 10),
                            CommonRichText(
                                text1: "Mobile number: ",
                                text2: "${item.doctorPhoneNumber}")
                          ]))),
            );
          })
    ]);
  }
}
