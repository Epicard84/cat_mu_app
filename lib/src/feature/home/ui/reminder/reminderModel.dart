class ReminderResp {
  bool? success;
  String? date;
  List<ReminderData>? data;

  ReminderResp({this.success, this.date, this.data});

  ReminderResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    date = json['date'];
    if (json['data'] != null) {
      data = <ReminderData>[];
      json['data'].forEach((v) {
        data!.add(new ReminderData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['date'] = this.date;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReminderData {
  int? id;
  int? masteruserId;
  String? reminder;
  int? status;
  String? date;
  String? createdAt;
  String? updatedAt;

  ReminderData(
      {this.id,
      this.masteruserId,
      this.reminder,
      this.status,
      this.date,
      this.createdAt,
      this.updatedAt});

  ReminderData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    masteruserId = json['masteruser_id'];
    reminder = json['reminder'];
    status = json['status'];
    date = json['date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['masteruser_id'] = this.masteruserId;
    data['reminder'] = this.reminder;
    data['status'] = this.status;
    data['date'] = this.date;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
