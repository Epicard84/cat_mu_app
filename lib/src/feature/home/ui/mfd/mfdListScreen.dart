import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/decoration.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/ui/clients/medication/edit_mfd.dart';
import 'package:cat/src/feature/home/ui/mfd/mfdModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class MFDListScreen extends StatefulWidget {
  const MFDListScreen({super.key});

  @override
  State<MFDListScreen> createState() => _MFDListScreenState();
}

class _MFDListScreenState extends State<MFDListScreen> {
  List<MFDData> mfdList = [];
  bool loading = false;
  final client = HTTPClient();

  void initState() {
    super.initState();
    getMFDList();
  }

  getMFDList() async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.getWithToken(APIEndpoint.listMFD);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          MFDResp resp = MFDResp.fromJson(jsonResp);
          mfdList = resp.data ?? [];
        }
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  TextEditingController mfd = TextEditingController();
  TextEditingController date = TextEditingController();

  addMFD() async {
    try {
      setState(() {
        loading = true;
      });

      var data = {"date": "${date.text}", "message": mfd.text};
      var res = await client.postWithToken(APIEndpoint.createMFD, data);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          mfd.clear();
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          getMFDList();
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("err >> $e");
      // showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: CommonAppBar(isBack: true, title: "MFD"),
            body: loading
                ? Center(child: CircularProgressIndicator())
                : SingleChildScrollView(
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                      child: Column(
                        children: [
                          FormBuilderDateTimePicker(
                              name: 'date',
                              initialEntryMode: DatePickerEntryMode.calendar,
                              initialValue: null,
                              onChanged: (e) {
                                setState(() {
                                  date.text = "${e!.year}-${e.month}-${e.day}";
                                });
                              },
                              inputType: InputType.date,
                              decoration: textFieldDecoration(
                                  hintText: "Select Date",
                                  prefix: Icon(Icons.calendar_month))),
                          SizedBox(height: 20),
                          CommonTextFiled(
                              textInputAction: TextInputAction.newline,
                              keyboardType: TextInputType.multiline,
                              maxLine: 5,
                              controller: mfd,
                              hintText: "Enter your MFD",
                              name: "mfd",
                              validator: null),
                          SizedBox(height: 20),
                          CommonButtonWidget(
                              onTap: () {
                                addMFD();
                              },
                              text: "Submit"),
                          SizedBox(height: 20),
                          ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: mfdList.length,
                              itemBuilder: (BuildContext context, int i) {
                                var list = mfdList[i];
                                return Padding(
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    child: Container(
                                      padding: EdgeInsets.all(8),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset(AppAsset.calender,
                                                  height: 20),
                                              SizedBox(width: 5),
                                              Text(formatDate(list.createdAt!),
                                                  style: TextStyle(
                                                      color: Colors.grey))
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  list.message ?? "",
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                              Column(
                                                children: [
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Constant
                                                                .primaryColor),
                                                        shape: BoxShape.circle),
                                                    child: Text(
                                                        getInitials(
                                                            string:
                                                                "${GlobalVar.userData.firstName} ${GlobalVar.userData.lastName}",
                                                            limitTo: 2),
                                                        style: TextStyle(
                                                            color: Constant
                                                                .primaryColor)),
                                                  ),
                                                  IconButton(
                                                      onPressed: () async {
                                                        var load = await Navigator
                                                            .push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            EditMfdScreen(
                                                                              noteId: "${list.id}",
                                                                              desc: "${list.message}",
                                                                            )));

                                                        if (load != null &&
                                                            load == true) {
                                                          getMFDList();
                                                        }
                                                      },
                                                      icon: Icon(Icons.edit))
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ));
                              }),
                        ],
                      ),
                    ),
                  )));
  }
}
