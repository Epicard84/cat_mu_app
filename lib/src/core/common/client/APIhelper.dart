import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

String BASE_URL = "https://mychartspace.com/";
// "http://dev8.mydevfactory.com/cat/";
String API_URL = '${BASE_URL}api/master_user/';
String IMG_PATH = "${BASE_URL}public/images/";
String IMG_PATH_MASTER = "${BASE_URL}images/";
String DOC_PATH = "${BASE_URL}public/documents/Client/";

class APIEndpoint {
  static String login = "login";
  static String userDetail = "user_details";
  static String editMasterUser = "edit";
  static String archiveClient = "client_archive";
  static String restoreClient = "client_restore";
  static String restoreSubuser = "subuser_restore";
  static String archiveList = "archive_client";
  static String archiveSubuser = "archive_subuser";
  static String getTime = "now_datetime";
  static String allChart = "medication_all_chart";
  static String filterNotes = "filter_medication_all_chart";

  static String subUserProfile = "sub_user_profile";

  static String forgetPassword = "forgot_password";
  static String otpVerification = "otp_verifiaction";
  static String newPassword = "new_password";

  static String clientList = "clients_list";
  static String addClient = "client_register";
  static String editClient = "update_client";

  static String disableAccount = "disable_reason";

  static String subUserList = "sub_user_list";
  static String addSubUser = "sub_user_register";
  static String editSubUser = "sub_user_edit";
  static String subuserDocsEdit = "subuser_docs_edit";
  static String deleteSubUser = "delete_user";
  static String removeSubUser = "delete_subuser";

  static String addMedicine = "add_new_medicine";
  static String getMedication = "medicine_schedule";
  static String getArchivedMedication = "archived_medicine_schedule";
  static String updateMedication = "client_medication_update";
  static String updateMedicationUndo = "client_medication_undo";

  static String medicineNote = "medication_note";
  static String medicineChart = "medication_chart";
  static String medicineNarrative = "medication_narrative";
  static String newDetails = "new_details";
  static String editDetail = "edit_details";
  static String deleteMedicine = "medicine_delete";
  static String medicineDetail = "medication_details";
  static String archivedMedictaionlist = "archived_medictaionlist";

  static String editMfd = "edit_mfd";
  static String deleteDetail = "details_delete";

  static String reminderList = "mu_reminder";
  static String newReminder = "new_reminder";
  static String checkReminder = "check_reminder/";
  static String deleteReminder = "delete_reminder/";

  static String createMFD = "create_mfd";
  static String listMFD = "mfd_list";

  static String notificationList = "mu_notifications";
  static String notificationSeen = "notification_seen";
  // static String medicineDetails = "medicine_details";

  static String acivitySchedule = "activity_schedule";
  static String addActivity = "add_new_activity";
  static String archiveActivity = "activity_archive";
  static String deleteActivity = "activity_delete";
  static String updateActivity = "client_activity_update";
  static String updateActivityUndo = "client_activity_undo";
  static String activityNote = "activity_note";
  static String activityNewDetails = "new_activity_details";
  static String activityEditDetails = "edit_activity_details";
}

class HTTPClient {
  Future<http.Response> getMethod(String url) async {
    log("$API_URL$url", name: "EndPoint");
    var result = await http.get(Uri.parse(API_URL + url));
    log("${jsonDecode(result.body)}", name: "Server Response");
    return result;
  }

  Future<http.Response> getWithToken(String url) async {
    log("$API_URL$url", name: "EndPoint");
    final token = GlobalVar.bearerToken;
    log("$token", name: "Token");
    final result = await http.get(Uri.parse("$API_URL$url"), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    });
    log("${jsonDecode(result.body)}", name: "Server Response");
    return result;
  }

  Future<http.Response> postMethod(
      String url, Map<String, dynamic> data) async {
    log("$API_URL$url", name: "EndPoint");
    log("${json.encode(data)}}", name: " Request Body");
    final result = await http.post(Uri.parse("$API_URL$url"),
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        });
    log("${jsonDecode(result.body)}", name: "Server Response");
    return result;
  }

  Future<http.Response> putMethod(String url, Map<String, dynamic> data) async {
    log("$API_URL$url", name: "EndPoint");
    log("${json.encode(data)}}", name: " Request Body");
    final result = await http.put(Uri.parse("$API_URL$url"),
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        });
    log("${jsonDecode(result.body)}", name: "Server Response");
    return result;
  }

  Future<http.Response> putWithToken(
      String url, Map<String, dynamic> data) async {
    log("$API_URL$url", name: "putWithToken");
    final token = GlobalVar.bearerToken;
    log("${json.encode(data)}", name: " Request Body");
    final result = await http
        .put(Uri.parse("$API_URL$url"), body: json.encode(data), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    });
    log("${jsonDecode(result.body)}", name: "Server Response");
    return result;
  }

  Future<http.Response> postWithToken(
      String url, Map<String, dynamic> data) async {
    log("$API_URL$url", name: "EndPoint");
    final token = GlobalVar.bearerToken;
    log('Bearer $token', name: "postWithToken");

    final result = await http.post(Uri.parse("$API_URL$url"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        },
        body: jsonEncode(data));
    log("${jsonDecode(result.body)}", name: "Server Response");
    return result;
  }

  Future<http.StreamedResponse> postMultipleFileWithToken(
      String url, Map<String, String> data,
      {required String fileKey,
      required List<File> files,
      Uint8List? signature}) async {
    final token = GlobalVar.bearerToken;
    log("$API_URL$url", name: "postFileWithToken");
    log("${json.encode(data)}", name: " Request Body");
    log("${files}", name: " Request Body");
    log("${signature}", name: " Request Body");
    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var request = http.MultipartRequest('POST', Uri.parse('$API_URL$url'));
    request.fields.addAll(data);
    if (signature != null) {
      request.files.add(await http.MultipartFile.fromBytes(
        "signature",
        signature,
        filename: "${DateTime.now()}_signature.png",
      ));
    }

    for (var i in files) {
      request.files
          .add(await http.MultipartFile.fromPath("$fileKey[]", i.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    return response;
  }

  Future<http.StreamedResponse> postFileWithToken(
      String url, Map<String, String> data,
      {required String fileKey, required List<File> files}) async {
    final token = GlobalVar.bearerToken;
    log("$API_URL$url", name: "postFileWithToken");
    log("${json.encode(data)}", name: " Request Body");
    log("${files}", name: " Request Body");
    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var request = http.MultipartRequest('POST', Uri.parse('$API_URL$url'));
    request.fields.addAll(data);

    for (var i in files) {
      request.files
          .add(await http.MultipartFile.fromPath("$fileKey[]", i.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    return response;
  }

  Future<http.StreamedResponse> post2FileWithToken(
      String url, Map<String, String> data,
      {required String fileKey,
      required List<File> files,
      required String fileKey2,
      required List<File> files2}) async {
    final token = GlobalVar.bearerToken;
    log("$API_URL$url", name: "postFileWithToken");
    log("${json.encode(data)}", name: " Request Body");
    log("${files}", name: " Request Body");
    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var request = http.MultipartRequest('POST', Uri.parse('$API_URL$url'));
    request.fields.addAll(data);

    for (var i in files) {
      request.files
          .add(await http.MultipartFile.fromPath("$fileKey[]", i.path));
    }

    for (var j in files2) {
      request.files.add(await http.MultipartFile.fromPath("$fileKey2", j.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    return response;
  }

  Future<http.StreamedResponse> post5FileWithToken(
    String url,
    Map<String, String> data, {
    required String fileKey,
    required List<File> files,
    required String fileKey2,
    required List<File> files2,
    required String fileKey3,
    required List<File> files3,
    required String fileKey4,
    required List<File> files4,
    required String fileKey5,
    required List<File> files5,
  }) async {
    final token = GlobalVar.bearerToken;
    log("$API_URL$url", name: "postFileWithToken");
    log("${json.encode(data)}", name: " Request Body");
    log("${files}", name: " Request Body");
    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var request = http.MultipartRequest('POST', Uri.parse('$API_URL$url'));
    request.fields.addAll(data);

    for (var i in files) {
      request.files.add(await http.MultipartFile.fromPath("$fileKey", i.path));
    }

    for (var j in files2) {
      request.files.add(await http.MultipartFile.fromPath("$fileKey2", j.path));
    }

    for (var k in files3) {
      request.files.add(await http.MultipartFile.fromPath("$fileKey3", k.path));
    }
    for (var l in files4) {
      request.files.add(await http.MultipartFile.fromPath("$fileKey4", l.path));
    }
    for (var m in files5) {
      request.files.add(await http.MultipartFile.fromPath("$fileKey5", m.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    return response;
  }

  Future<http.StreamedResponse> postSingleFileWithToken(
      String url, Map<String, String> data,
      {required String fileKey,
      bool hasFile = true,
      String filePath = ""}) async {
    final token = GlobalVar.bearerToken;
    log("$API_URL$url", name: "postSingleFileWithToken");
    log("${json.encode(data)}", name: " Request Body");
    log("${filePath}", name: " filePath Body");
    log("${fileKey}", name: " fileKey Body");
    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var request = http.MultipartRequest('POST', Uri.parse('$API_URL$url'));
    request.fields.addAll(data);
    if (hasFile) {
      request.files
          .add(await http.MultipartFile.fromPath("$fileKey", filePath));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    return response;
  }
}
