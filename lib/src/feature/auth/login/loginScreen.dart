import 'dart:convert';
import 'dart:io';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/auth/forget/forgetScreen.dart';
import 'package:cat/src/feature/auth/model/loginResp.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:upgrader/upgrader.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool obscureText = true;
  bool loading = false;

  final client = HTTPClient();

  toggle() {
    obscureText = !obscureText;
    setState(() {});
  }

  onLogin(BuildContext context,
      {required String email, required String password}) async {
    if (email.isEmpty) {
      showSnackBar(context, text: ValidationString.emailRequried);
      return null;
    }
    if (!email.validateEmail()) {
      showSnackBar(context, text: ValidationString.validEmail);
      return null;
    }
    if (password.isEmpty) {
      showSnackBar(context, text: ValidationString.passwordRequired);
      return null;
    }

    try {
      setState(() {
        loading = true;
      });
      var res = await await client.postMethod(APIEndpoint.login, {
        'email': email,
        'password': password,
        "location": "${GlobalVar.cityName}",
        "latitude": "${GlobalVar.lat}",
        "longitude": "${GlobalVar.lon}",
      });
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          LoginResp loginResp = LoginResp.fromJson(jsonResp);
          SharedPreferences prefs = await SharedPreferences.getInstance();
          await prefs.setString("authToken", loginResp.authToken!);
          GlobalVar.bearerToken = await prefs.getString("authToken") ?? null;
          GlobalVar.userData = loginResp.data!;
          await Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DashboardScreen()),
              (Route<dynamic> route) => false);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } on SocketException catch (_) {
      showSnackBar(context, text: ValidationString.noInternet);
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return UpgradeAlert(
      upgrader: Upgrader(
          canDismissDialog: false, dialogStyle: UpgradeDialogStyle.cupertino),
      child: Scaffold(
          body: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
              child: SingleChildScrollView(
                child: Padding(
                    padding: EdgeInsets.all(30),
                    child: FormBuilder(
                      key: formKey,
                      child: Column(children: [
                        SizedBox(height: 100),
                        BoldText(text: "Welcome to MyChartSpace!"),
                        // SizedBox(height: 15),
                        // SubText(
                        //     text:
                        //         "There are many variations of passages of \nLorem Ipsum available."),
                        SizedBox(height: 50),
                        CommonTextFiled(
                            controller: email,
                            keyboardType: TextInputType.emailAddress,
                            hintText: "Enter email address",
                            name: "Enter email address",
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(),
                              FormBuilderValidators.email(),
                            ])),
                        SizedBox(height: 20),
                        CommonTextFiled(
                            obscureText: obscureText,
                            controller: password,
                            hintText: "Password",
                            name: "Password",
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(),
                              FormBuilderValidators.minLength(6),
                            ]),
                            suffixIcon: GestureDetector(
                                onTap: () {
                                  toggle();
                                },
                                child: Icon(obscureText
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined))),
                        SizedBox(height: 10),

                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ForgetScreen()));
                                  },
                                  child: SubText(
                                      text: "Forgot password?",
                                      textColor: Constant.primaryColor))
                            ]),
                        SizedBox(height: 10),
                        RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              text: "*By logging in , I am accepting ",
                              style: TextStyle(color: Colors.black)),
                          TextSpan(
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  await launchUrl(Uri.parse(terms));
                                },
                              text: " Terms & conditions ",
                              style: TextStyle(color: Constant.primaryColor)),
                          TextSpan(
                              text: " and ",
                              style: TextStyle(color: Colors.black)),
                          TextSpan(
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  await launchUrl(Uri.parse(privacy));
                                },
                              text: " Privacy policies.",
                              style: TextStyle(color: Constant.primaryColor)),
                        ])),

                        SizedBox(height: 20),

                        loading
                            ? CircularProgressIndicator()
                            : CommonButtonWidget(
                                onTap: () {
                                  if (formKey.currentState!.validate()) {
                                    onLogin(context,
                                        email: email.text.trim(),
                                        password: password.text);
                                  }
                                },
                                text: "Login"),

                        SizedBox(height: 20),
                      ]),
                    )),
              ))),
    );
  }
}
