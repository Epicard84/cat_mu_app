import 'package:flutter/material.dart';

showSnackBar(BuildContext context,
    {required String text, bool success = false}) {
  final snackBar = SnackBar(
      backgroundColor: success ? Colors.green[400] : Colors.red[400],
      content: Text('$text'));
  return ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
