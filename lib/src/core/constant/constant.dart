import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:flutter/material.dart';

import '../../feature/auth/model/loginResp.dart';

String appName = "MyChartSpace";

String timezone = "HST"; // DateTime.now().timeZoneName; // "EST";

String terms = "${BASE_URL}terms-and-conditions";
String privacy = "${BASE_URL}privacy-and-policy";

String deviceToken = "";

bool showReminder = false;

class Constant {
  static String appName = "cat";
  static String mapKey = "AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0";
  static const Color primaryColor = Color(0xff3D4EC6);
  static const Color secondaryColor = Color(0xff354270);
  static const Color bgColor = Color(0xffEBEDFF);
  static const Color textColor = Color(0xff1B1F2D);
  static const Color textColor2 = Color(0xff545C7A);
  static const Color textColor3 = Color(0xff04E0FE);
  static const Color online = Color(0xff33AF47);
  static const Color offline = Color(0xffF2C245);
  static const Color nevIconColor = Color(0xffC8CCDD);
  static const Color buttonColor = Color(0xff66C4E1);
}

String getInitials({required String string, required int limitTo}) {
  var buffer = StringBuffer();
  var split = string.split(' ');
  for (var i = 0; i < (limitTo); i++) {
    buffer.write(split[i][0]);
  }
  return buffer.toString().toUpperCase();
}

class AppAsset {
  static String forgetIcons = "assets/icons/forgetIcon.png";
  static String newPassword = "assets/icons/newPasswordIcon.png";
  static String enterCodeIcon = "assets/icons/enterCodeIcon.png";
  static String back = "assets/icons/back.png";
  static String fb = "assets/icons/facebook.png";
  static String google = "assets/icons/google.png";
  static String logoName = "assets/icons/logoName.png";
  static String logo = "assets/icons/logo.png";
  static String bg = "assets/images/bg.png";
  static String bg2 = "assets/images/bg2.png";
  static String splashBg = "assets/images/splashBg.png";
  static String roundEffect = "assets/images/roundEffect.png";
  static String roundEffect2 = "assets/images/roundEffect2.png";
  static String archive = "assets/icons/archive.png";
  static String pdf = "assets/icons/pdf.png";
  static String edit = "assets/icons/edit.png";
  static String hold = "assets/icons/hold.png";
  static String delete = "assets/icons/delete.png";
//////
  static String reminder = "assets/icons/reminder.png";
  static String subUsers = "assets/icons/subUsers.png";
  static String mfd = "assets/icons/mfd.png";
  static String dashboardIcon = "assets/icons/dashboardIcon.png";
  static String clients = "assets/icons/clients.png";
  static String notification = "assets/icons/notification.png";
  static String calender = "assets/icons/calender.png";
  static String medicine = "assets/images/medicine.png";

  static String img1 = "assets/images/img1.png";
  static String img2 = "assets/images/img2.png";
  static String img3 = "assets/images/img3.png";

  static String lock = "assets/icons/lock.png";
  static String camera = "assets/icons/camera.png";
  static String doc = "assets/icons/doc.png";

  static String dp = "assets/images/dp.png";
}

class AppFont {
  static String josefinSansMedium = "JosefinSansMedium";
  static String josefinSansBold = "JosefinSansBold";
}

class GlobalVar {
  static String? bearerToken;
  static UserModel userData = UserModel();
  static String cityName = "";
  static String lat = '';
  static String lon = '';
}
