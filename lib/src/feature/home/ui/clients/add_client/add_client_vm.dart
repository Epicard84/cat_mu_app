import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final addClientVm = ChangeNotifierProvider.autoDispose<AddClientVM>((ref) {
  return AddClientVM();
});
TextEditingController email = TextEditingController();
TextEditingController password = TextEditingController();
bool obscureText = true;

class AddClientVM with ChangeNotifier {
  List<File> docFile = [];
  String? sGender;

  List<PersonInfo> doctorList = [
    PersonInfo(
        personName: TextEditingController(),
        personAddress: TextEditingController(),
        personPhoneNumber: TextEditingController())
  ];

  List<PersonInfo> guardianList = [
    PersonInfo(
        personName: TextEditingController(),
        personAddress: TextEditingController(),
        personPhoneNumber: TextEditingController())
  ];
  AddClientVM() {}

  addDoctor() {
    doctorList.add(PersonInfo(
        personName: TextEditingController(),
        personAddress: TextEditingController(),
        personPhoneNumber: TextEditingController()));
    notifyListeners();
  }

  addGuardian() {
    guardianList.add(PersonInfo(
        personName: TextEditingController(),
        personAddress: TextEditingController(),
        personPhoneNumber: TextEditingController()));
    notifyListeners();
  }

  removeDoctor(index) {
    doctorList.remove(index);
    notifyListeners();
  }

  removeGuardian(index) {
    guardianList.remove(index);
    notifyListeners();
  }

  setGender(gen) {
    sGender = gen;
    notifyListeners();
  }

  mediaOption(context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                      leading: Icon(Icons.photo_camera_outlined),
                      onTap: () {
                        openCamera();
                        Navigator.pop(context);
                      },
                      title: Text("Camera")),
                  ListTile(
                      leading: Icon(Icons.folder_open_outlined),
                      onTap: () {
                        pickFile();
                        Navigator.pop(context);
                      },
                      title: Text("Gallery")),
                ],
              ),
            ),
          );
        });
  }

  pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      docFile.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  pickPhotos() async {
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      docFile.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  void openCamera() async {
    var imgCamera = await ImagePicker().pickImage(source: ImageSource.camera);
    if (imgCamera != null) {
      docFile.add(File(imgCamera.path));
    }
    notifyListeners();
  }
}

class PersonInfo {
  TextEditingController personName;
  TextEditingController personAddress;
  TextEditingController personPhoneNumber;

  PersonInfo(
      {required this.personName,
      required this.personAddress,
      required this.personPhoneNumber});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.personName.text;
    data['address'] = this.personAddress.text;
    data['phone_number'] = this.personPhoneNumber.text;
    return data;
  }
}
