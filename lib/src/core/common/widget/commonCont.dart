import 'package:cat/src/core/constant/constant.dart';
import 'package:flutter/material.dart';

class CommonContainer extends StatelessWidget {
  final double? height;
  final Widget? child;
  final AssetImage image;
  const CommonContainer({
    super.key,
    this.height,
    this.child,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Container(
        height: height,
        width: screenWidth,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Constant.primaryColor,
            image: DecorationImage(image: image, fit: BoxFit.fill)),
        child: child);
  }
}
