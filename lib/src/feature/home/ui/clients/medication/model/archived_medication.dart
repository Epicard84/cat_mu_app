class ArchivedMedication {
  bool? success;
  List<ArchivedMedicationData>? data;

  ArchivedMedication({this.success, this.data});

  ArchivedMedication.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = <ArchivedMedicationData>[];
      json['data'].forEach((v) {
        data!.add(new ArchivedMedicationData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ArchivedMedicationData {
  int? id;
  String? clientId;
  String? medicineName;
  String? startDate;
  String? endDate;
  int? timeDays;
  String? scheduledTime;
  int? status;
  String? descriptions;
  String? docs;
  String? createdAt;
  String? updatedAt;

  ArchivedMedicationData(
      {this.id,
      this.clientId,
      this.medicineName,
      this.startDate,
      this.endDate,
      this.timeDays,
      this.scheduledTime,
      this.status,
      this.descriptions,
      this.docs,
      this.createdAt,
      this.updatedAt});

  ArchivedMedicationData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    medicineName = json['medicine_name'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    timeDays = json['time_days'];
    scheduledTime = json['scheduled_time'];
    status = json['status'];
    descriptions = json['descriptions'];
    docs = json['docs'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['medicine_name'] = this.medicineName;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['time_days'] = this.timeDays;
    data['scheduled_time'] = this.scheduledTime;
    data['status'] = this.status;
    data['descriptions'] = this.descriptions;
    data['docs'] = this.docs;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
