import 'dart:async';
import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/auth/forget/newPasswordScreen.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class EnterCodeScreen extends StatefulWidget {
  final String email;
  const EnterCodeScreen({super.key, required this.email});

  @override
  State<EnterCodeScreen> createState() => _EnterCodeScreenState();
}

class _EnterCodeScreenState extends State<EnterCodeScreen> {
  StreamController<ErrorAnimationType>? errorController;
  bool hasError = false;
  String otp = "";
  final formKey = GlobalKey<FormState>();

  bool loading = false;
  final client = HTTPClient();

  onVerify(BuildContext context, {required String email}) async {
    try {
      setState(() {
        loading = true;
      });
      var res = await await client.postMethod(
          APIEndpoint.otpVerification, {'email': email, 'otp': otp});
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      NewPassworsScreen(email: email, otp: otp)));
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(res.body);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController!.close();
    super.dispose();
  }

  snackBar(String? message) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: SubText(text: message!, textColor: Constant.textColor),
        duration: const Duration(seconds: 2)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
            child: SingleChildScrollView(
                child: Column(children: [
              CommonAppBar(isBack: true),
              Padding(
                  padding: EdgeInsets.all(25),
                  child: Column(children: [
                    SizedBox(height: 50),
                    Image.asset(AppAsset.enterCodeIcon, height: 80),
                    SizedBox(height: 75),
                    BoldText(text: "Enter code!"),
                    SizedBox(height: 15),
                    SubText(text: "Please enter 4 digit code sent to"),
                    SizedBox(height: 5),
                    SubText(
                        text: "${widget.email}", textColor: Constant.textColor),
                    SizedBox(height: 50),
                    PinCodeTextField(
                        appContext: context,
                        pastedTextStyle: TextStyle(fontWeight: FontWeight.bold),
                        length: 4,
                        obscureText: true,
                        obscuringCharacter: '*',
                        blinkWhenObscuring: true,
                        animationType: AnimationType.fade,
                        pinTheme: PinTheme(
                            fieldOuterPadding:
                                EdgeInsets.symmetric(horizontal: 15),
                            shape: PinCodeFieldShape.box,
                            borderRadius: BorderRadius.circular(5),
                            fieldHeight: 50,
                            fieldWidth: 40,
                            errorBorderColor: Constant.primaryColor,
                            borderWidth: 1,
                            activeColor: Constant.textColor2,
                            activeFillColor: Constant.textColor2,
                            inactiveColor: Constant.textColor2,
                            selectedColor: Constant.primaryColor),
                        cursorColor: Constant.primaryColor,
                        animationDuration: const Duration(milliseconds: 300),
                        // enableActiveFill: true,
                        errorAnimationController: errorController,
                        keyboardType: TextInputType.number,
                        onCompleted: (v) {
                          debugPrint("Completed");
                        },
                        onChanged: (value) {
                          debugPrint(value);
                          setState(() {
                            otp = value;
                          });
                        },
                        beforeTextPaste: (text) {
                          debugPrint("Allowing to paste $text");
                          return true;
                        }),
                    Align(
                        heightFactor: 3.5,
                        alignment: Alignment.bottomCenter,
                        child: loading
                            ? CircularProgressIndicator()
                            : CommonButtonWidget(
                                onTap: () {
                                  if (otp.length != 4) {
                                    showSnackBar(context,
                                        text: "Enter valid otp");
                                  } else {
                                    onVerify(context, email: widget.email);
                                  }
                                },
                                text: "Next"))
                  ]))
            ]))));
  }
}
