import 'dart:convert';
import 'dart:developer';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/auth/login/loginScreen.dart';
import 'package:cat/src/feature/auth/model/loginResp.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  // await NotificationService().onInit();
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({key});
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: appName,
          theme: ThemeData(
              disabledColor: Colors.black,
              primarySwatch: Colors.indigo,
              fontFamily: AppFont.josefinSansMedium),
          home: child,
        );
      },
      child: const Splash(),
    );
  }
}

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);
  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    checkAuth();
  }

  checkAuth() async {
    var res = await http.get(Uri.parse('http://ip-api.com/json'));
    var resp = jsonDecode(res.body);
    log("$resp");
    GlobalVar.cityName = "${resp['city']}, ${resp['country']}";
    GlobalVar.lat = "${resp['lat']}";
    GlobalVar.lon = "${resp['lon']}";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    GlobalVar.bearerToken = await prefs.getString("authToken");
    log("token >> ${GlobalVar.bearerToken}");
    await [Permission.camera, Permission.storage].request();
    if (GlobalVar.bearerToken != null) {
      try {
        final client = HTTPClient();
        http.Response res = await client.getWithToken(APIEndpoint.userDetail);
        if (res.statusCode == 200) {
          var jsonResp = jsonDecode(res.body);
          if (jsonResp['success']) {
            GlobalVar.userData = UserModel.fromJson(jsonResp['data']);
            log("userData >> ${GlobalVar.userData.toJson()}");
            await Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => DashboardScreen()),
                (Route<dynamic> route) => false);
          }
        } else {
          // var jsonResp = jsonDecode(res.body);
          // showSnackBar(context, text: jsonResp['message']);
          SharedPreferences prefs = await SharedPreferences.getInstance();
          await prefs.remove("authToken");
          await Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => LoginScreen()),
              (Route<dynamic> route) => false);
        }
      } catch (e) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginScreen()),
            (Route<dynamic> route) => false);
      }
    } else {
      await Future.delayed(Duration(milliseconds: 800));
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => LoginScreen()),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: double.infinity,
            width: double.infinity,
            child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Spacer(),
                      Padding(
                          padding: const EdgeInsets.all(50),
                          child: Image.asset(AppAsset.logoName)),
                      Spacer(),
                      SubText(
                          text: "App version : AP1.123",
                          textColor: Constant.textColor)
                    ])),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAsset.splashBg), fit: BoxFit.fill))));
  }
}
