import 'dart:developer';
import 'dart:io';
import 'package:cat/src/feature/home/model/client.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final editClientVm = ChangeNotifierProvider.autoDispose<EditClientVM>((ref) {
  return EditClientVM();
});
TextEditingController email = TextEditingController();
TextEditingController password = TextEditingController();
bool obscureText = true;

class EditClientVM with ChangeNotifier {
  List<File> docFile = [];

  List<String> exitsFile = [];
  String? sGender;

  List<PersonInfo> doctorList = [];

  List<PersonInfo> guardianList = [];

  List<String> docs = [];

  EditClientVM() {}

  removeImage(String index) {
    docs.remove(index);
    notifyListeners();
  }

  addImage(String index) {
    docs.add(index);
    notifyListeners();
  }

  saveData(ClientData clientData) {
    log("gender >> ${clientData.gender}");
    sGender = clientData.gender;

    for (Doctor i in clientData.doctor ??
        [Doctor(doctorAddress: "", doctorName: "", doctorPhoneNumber: "")]) {
      doctorList.add(PersonInfo(
          personName: TextEditingController(text: i.doctorName),
          personAddress: TextEditingController(text: i.doctorAddress),
          personPhoneNumber: TextEditingController(text: i.doctorPhoneNumber)));
    }

    for (Guardian i in clientData.guardian ??
        [
          Guardian(
              guardianAddress: "", guardianName: "", guardianPhoneNumber: "")
        ]) {
      guardianList.add(PersonInfo(
          personName: TextEditingController(text: i.guardianName),
          personAddress: TextEditingController(text: i.guardianAddress),
          personPhoneNumber:
              TextEditingController(text: i.guardianPhoneNumber)));
    }

    exitsFile = clientData.documents ?? [];
    for (var i in exitsFile) {
      docs.add(i.split('/').last);
    }
  }

  addDoctor() {
    doctorList.add(PersonInfo(
        personName: TextEditingController(),
        personAddress: TextEditingController(),
        personPhoneNumber: TextEditingController()));
    notifyListeners();
  }

  addGuardian() {
    guardianList.add(PersonInfo(
        personName: TextEditingController(),
        personAddress: TextEditingController(),
        personPhoneNumber: TextEditingController()));
    notifyListeners();
  }

  removeDoctor(index) {
    doctorList.remove(index);
    notifyListeners();
  }

  removeGuardian(index) {
    guardianList.remove(index);
    notifyListeners();
  }

  setGender(gen) {
    sGender = gen;
    notifyListeners();
  }

  mediaOption(context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  ListTile(
                      leading: Icon(Icons.photo_camera_outlined),
                      onTap: () {
                        openCamera();
                        Navigator.pop(context);
                      },
                      title: Text("Camera")),
                  ListTile(
                      leading: Icon(Icons.folder_open_outlined),
                      onTap: () {
                        pickFile();
                        Navigator.pop(context);
                      },
                      title: Text("Gallery")),
                ],
              ),
            ),
          );
        });
  }

  pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      docFile.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  pickPhotos() async {
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      docFile.add(File(result.files.single.path!));
    }
    notifyListeners();
  }

  void openCamera() async {
    var imgCamera = await ImagePicker().pickImage(source: ImageSource.camera);
    if (imgCamera != null) {
      docFile.add(File(imgCamera.path));
    }
    notifyListeners();
  }
}

class PersonInfo {
  TextEditingController personName;
  TextEditingController personAddress;
  TextEditingController personPhoneNumber;

  PersonInfo(
      {required this.personName,
      required this.personAddress,
      required this.personPhoneNumber});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.personName.text;
    data['address'] = this.personAddress.text;
    data['phone_number'] = this.personPhoneNumber.text;
    return data;
  }
}
