class ValidationString {
  static String noInternet = "No Internet Connection";
  static String somethingWentWrong = "Something went wrong";
  static String emailRequried = "Email address is required";
  static String validEmail = "Please enter valid email address";
  static String passwordRequired = "Password is required";
  static String cPasswordRequired = "Confirm Password is required";
  static String passMinLength =
      "Password & Confirm Password must be greater than 6 characters";
  static String passNotMatched =
      "Password & Confirm Password dosn't matched";
}
