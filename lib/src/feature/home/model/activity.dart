import 'package:cat/src/feature/home/model/medication.dart';

class ActivityRespModel {
  bool? success;
  ClientActivity? clientData;
  String? date;
  List<Note>? note;

  ActivityRespModel({this.success, this.clientData, this.date});

  ActivityRespModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    clientData = json['client_data'] != null
        ? new ClientActivity.fromJson(json['client_data'])
        : null;
    date = json['date'];
    if (json['note'] != null) {
      note = <Note>[];
      json['note'].forEach((v) {
        note!.add(new Note.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.clientData != null) {
      data['client_data'] = this.clientData!.toJson();
    }
    data['date'] = this.date;
    if (this.note != null) {
      data['note'] = this.note!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ClientActivity {
  int? id;
  String? firstName;
  String? lastName;
  String? dob;
  String? gender;
  String? status;
  String? masterUser;
  String? createdAt;
  String? updatedAt;
  List<GetActivities>? getActivities;

  ClientActivity(
      {this.id,
      this.firstName,
      this.lastName,
      this.dob,
      this.gender,
      this.status,
      this.masterUser,
      this.createdAt,
      this.updatedAt,
      this.getActivities});

  ClientActivity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dob = json['dob'];
    gender = json['gender'];
    status = json['status'];
    masterUser = json['master_user'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['get_activities'] != null) {
      getActivities = <GetActivities>[];
      json['get_activities'].forEach((v) {
        getActivities!.add(new GetActivities.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['status'] = this.status;
    data['master_user'] = this.masterUser;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.getActivities != null) {
      data['get_activities'] =
          this.getActivities!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetActivities {
  int? id;
  String? clientId;
  String? activityName;
  String? startDate;
  String? endDate;
  int? timeDays;
  String? scheduledTime;
  int? status;
  String? descriptions;
  List<String>? docs;
  String? createdAt;
  String? updatedAt;
  List<GetActivitySchedule>? getActivitySchedule;

  GetActivities(
      {this.id,
      this.clientId,
      this.activityName,
      this.startDate,
      this.endDate,
      this.timeDays,
      this.scheduledTime,
      this.status,
      this.descriptions,
      this.docs,
      this.createdAt,
      this.updatedAt,
      this.getActivitySchedule});

  GetActivities.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clientId = json['client_id'];
    activityName = json['activity_name'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    timeDays = json['time_days'];
    scheduledTime = json['scheduled_time'];
    status = json['status'];
    descriptions = json['descriptions'];
    docs = json['docs'].cast<String>();
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['get_activity_schedule'] != null) {
      getActivitySchedule = <GetActivitySchedule>[];
      json['get_activity_schedule'].forEach((v) {
        getActivitySchedule!.add(new GetActivitySchedule.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['client_id'] = this.clientId;
    data['activity_name'] = this.activityName;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['time_days'] = this.timeDays;
    data['scheduled_time'] = this.scheduledTime;
    data['status'] = this.status;
    data['descriptions'] = this.descriptions;
    data['docs'] = this.docs;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.getActivitySchedule != null) {
      data['get_activity_schedule'] =
          this.getActivitySchedule!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetActivitySchedule {
  int? id;
  int? activityId;
  String? date;
  String? scheduledTime;
  String? completeDetails;
  String? status;
  String? createdAt;
  String? updatedAt;

  GetActivitySchedule(
      {this.id,
      this.activityId,
      this.date,
      this.scheduledTime,
      this.completeDetails,
      this.status,
      this.createdAt,
      this.updatedAt});

  GetActivitySchedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    activityId = json['activity_id'];
    date = json['date'];
    scheduledTime = json['scheduled_time'];
    completeDetails = json['complete_details'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['activity_id'] = this.activityId;
    data['date'] = this.date;
    data['scheduled_time'] = this.scheduledTime;
    data['complete_details'] = this.completeDetails;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
