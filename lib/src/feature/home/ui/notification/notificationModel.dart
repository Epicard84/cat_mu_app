class NotificationResp {
  bool? success;
  List<NotificationData>? data;

  NotificationResp({this.success, this.data});

  NotificationResp.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = <NotificationData>[];
      json['data'].forEach((v) {
        data!.add(new NotificationData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NotificationData {
  int? id;
  int? receiverId;
  int? notificationId;
  int? status;
  String? createdAt;
  String? updatedAt;
  GetNotiDetails? getNotiDetails;

  NotificationData(
      {this.id,
      this.receiverId,
      this.notificationId,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.getNotiDetails});

  NotificationData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    receiverId = json['receiver_id'];
    notificationId = json['notification_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    getNotiDetails = json['get_noti_details'] != null
        ? new GetNotiDetails.fromJson(json['get_noti_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['receiver_id'] = this.receiverId;
    data['notification_id'] = this.notificationId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.getNotiDetails != null) {
      data['get_noti_details'] = this.getNotiDetails!.toJson();
    }
    return data;
  }
}

class GetNotiDetails {
  int? id;
  int? patientId;
  String? notification;
  String? createdAt;
  String? updatedAt;

  GetNotiDetails(
      {this.id,
      this.patientId,
      this.notification,
      this.createdAt,
      this.updatedAt});

  GetNotiDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    patientId = json['patient_id'];
    notification = json['notification'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['patient_id'] = this.patientId;
    data['notification'] = this.notification;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
