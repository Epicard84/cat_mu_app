import 'dart:convert';
import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/subuserDetail.dart';
import 'package:cat/src/feature/home/ui/clients/dashbaord_screen.dart';
import 'package:cat/src/feature/home/ui/subuser/editSubuserVm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;

class EditSubuserScreen extends ConsumerStatefulWidget {
  final SubuserDetailNew user;
  const EditSubuserScreen({super.key, required this.user});

  @override
  ConsumerState<EditSubuserScreen> createState() => _EditSubuserScreenState();
}

class _EditSubuserScreenState extends ConsumerState<EditSubuserScreen> {
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController email = TextEditingController();

  void initState() {
    super.initState();
    firstName.text = widget.user.profile?.firstName ?? "";
    lastName.text = widget.user.profile?.lastName ?? "";
    email.text = widget.user.profile?.email ?? "";
    ref.read(editSubuserVm).saveData(widget.user);
    setState(() {});
  }

  bool loading = false;
  final client = HTTPClient();

  editSubUser(EditSubuserVM _vm) async {
    try {
      setState(() {
        loading = true;
      });

      var data = {
        'id': "${widget.user.profile?.id}",
        "first_name": firstName.text,
        "last_name": lastName.text,
      };

      http.StreamedResponse res = await client.post5FileWithToken(
        APIEndpoint.editSubUser,
        data,
        fileKey: "profile_image",
        files: _vm.file != null ? [_vm.file!] : [],
        fileKey2: "operatorLicense",
        files2:
            _vm.operatorsLicensefile != null ? [_vm.operatorsLicensefile!] : [],
        fileKey3: "tbClearance",
        files3: _vm.clearancefile != null ? [_vm.clearancefile!] : [],
        fileKey4: "physicalCheckup",
        files4:
            _vm.physicalCheckupfile != null ? [_vm.physicalCheckupfile!] : [],
        fileKey5: "misc_doc",
        files5: _vm.miscDocsFile != null ? [_vm.miscDocsFile!] : [],
      );
      var respp = await res.stream.bytesToString();
      print(respp);
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          await Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DashboardScreen()),
              (Route<dynamic> route) => false);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(respp);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      final _vm = ref.watch(editSubuserVm);
      return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppAsset.bg), fit: BoxFit.fill)),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: CommonAppBar(title: "Edit sub user"),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: GestureDetector(
                            onTap: () {
                              _vm.mediaOption(context, isDp: true);
                            },
                            child: CircleAvatar(
                              radius: 40,
                              backgroundColor: Constant.bgColor,
                              backgroundImage: _vm.file != null
                                  ? Image.file(_vm.file!).image
                                  : ((widget.user.profile?.profileImageName
                                                  ?.length ??
                                              0) >
                                          1)
                                      ? Image.network(
                                              "$IMG_PATH/SubUserProfile/${widget.user.profile?.profileImageName}")
                                          .image
                                      : Image.asset(AppAsset.dp).image,
                              child: Stack(children: [
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: CircleAvatar(
                                    radius: 15,
                                    backgroundColor: Colors.white,
                                    child: Icon(Icons.add,
                                        color: Constant.primaryColor),
                                  ),
                                ),
                              ]),
                            )),
                      ),
                      SizedBox(height: 20),
                      CommonTextFiled(
                          controller: firstName,
                          hintText: "First name",
                          name: "First name",
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(),
                          ])),
                      SizedBox(height: 20),
                      CommonTextFiled(
                          controller: lastName,
                          hintText: "Last name",
                          name: "Last name",
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required()])),
                      SizedBox(height: 20),
                      // CommonTextFiled(
                      //     controller: company,
                      //     hintText: "Company name",
                      //     name: "Company name",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      // SizedBox(height: 20),
                      // CommonTextFiled(
                      //     controller: phone,
                      //     hintText: "Phone number",
                      //     name: "Phone number",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      CommonTextFiled(
                          isEnabled: false,
                          controller: email,
                          hintText: "Email address",
                          name: "Email address",
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(),
                          ])),
                      SizedBox(height: 20),
                      // Text("Upload Documents"),
                      // SizedBox(height: 10),
                      // Row(
                      //   children: [
                      //     Container(
                      //         decoration: BoxDecoration(
                      //             color: Constant.primaryColor,
                      //             borderRadius: BorderRadius.circular(15)),
                      //         child: IconButton(
                      //             onPressed: () {
                      //               _vm.openCamera2();
                      //             },
                      //             icon: Icon(Icons.photo_camera_outlined,
                      //                 color: Colors.white))),
                      //     SizedBox(width: 20),
                      //     Container(
                      //         decoration: BoxDecoration(
                      //             color: Constant.primaryColor,
                      //             borderRadius: BorderRadius.circular(15)),
                      //         child: IconButton(
                      //             onPressed: () {
                      //               _vm.pickFile2();
                      //             },
                      //             icon: Icon(Icons.attachment,
                      //                 color: Colors.white))),
                      //     SizedBox(width: 20),
                      //     Platform.isIOS
                      //         ? Container(
                      //             decoration: BoxDecoration(
                      //                 color: Constant.primaryColor,
                      //                 borderRadius: BorderRadius.circular(15)),
                      //             child: IconButton(
                      //                 onPressed: () {
                      //                   _vm.pickPhotos();
                      //                 },
                      //                 icon: Icon(Icons.image_outlined,
                      //                     color: Colors.white)))
                      //         : SizedBox()
                      //   ],
                      // ),
                      // SizedBox(height: 20),
                      // ListView.builder(
                      //     shrinkWrap: true,
                      //     physics: NeverScrollableScrollPhysics(),
                      //     itemCount: _vm.docFile.length,
                      //     padding: EdgeInsets.zero,
                      //     itemBuilder: (BuildContext context, int i) {
                      //       var text = _vm.docFile[i];
                      //       return Padding(
                      //         padding:
                      //             const EdgeInsets.symmetric(vertical: 5.0),
                      //         child: ListTile(
                      //             shape: RoundedRectangleBorder(
                      //                 borderRadius: BorderRadius.all(
                      //                     Radius.circular(10))),
                      //             tileColor: Colors.white,
                      //             title: Text(text.path.split('/').last)),
                      //       );
                      //     }),

                      // CommonTextFiled(
                      //     controller: address,
                      //     hintText: "Address",
                      //     name: "Address",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      // SizedBox(height: 20),
                      // CommonTextFiled(
                      //     controller: mailAddress,
                      //     hintText: "Mailing address",
                      //     name: "Mailing address",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      // SizedBox(height: 20),
                      // CommonTextFiled(
                      //     controller: city,
                      //     hintText: "City",
                      //     name: "City",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      // SizedBox(height: 20),
                      // CommonTextFiled(
                      //     controller: state,
                      //     hintText: "State",
                      //     name: "State",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      // SizedBox(height: 20),
                      // CommonTextFiled(
                      //     controller: country,
                      //     hintText: "Country",
                      //     name: "Country",
                      //     validator: FormBuilderValidators.compose([
                      //       FormBuilderValidators.required(),
                      //     ])),
                      // SizedBox(height: 20),
                      // ListView.builder(
                      //     shrinkWrap: true,
                      //     physics: NeverScrollableScrollPhysics(),
                      //     itemCount: _vm.exitsFile.length,
                      //     padding: EdgeInsets.zero,
                      //     itemBuilder: (BuildContext context, int i) {
                      //       var text = _vm.exitsFile[i];
                      //       return Padding(
                      //         padding:
                      //             const EdgeInsets.symmetric(vertical: 5.0),
                      //         child: ListTile(
                      //             trailing:
                      //                 _vm.docs.contains(text.split('/').last)
                      //                     ? Icon(Icons.check_box,
                      //                         color: Constant.primaryColor)
                      //                     : Icon(Icons.check_box_outline_blank),
                      //             onTap: () {
                      //               _vm.docs.contains(text.split('/').last)
                      //                   ? _vm.removeImage(text.split('/').last)
                      //                   : _vm.addImage(text.split('/').last);
                      //             },
                      //             shape: RoundedRectangleBorder(
                      //                 borderRadius: BorderRadius.all(
                      //                     Radius.circular(10))),
                      //             tileColor: Colors.white,
                      //             title: Text(text.split('/').last)),
                      //       );
                      //     }),

                      Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: LabelText(label: "Documents")),
                      Text("Operators License"),
                      SizedBox(height: 5),
                      GestureDetector(
                        onTap: () {
                          _vm.pickDoc(0);
                        },
                        child: Stack(
                            alignment: AlignmentDirectional.topEnd,
                            children: [
                              _vm.operatorsLicensefile != null
                                  ? Image.file(_vm.operatorsLicensefile!,
                                      height: 100)
                                  : (widget.user.profile?.operatorsLicense !=
                                          null)
                                      ? Image.network(
                                          "${IMG_PATH_MASTER}SubUserProfile/${widget.user.profile?.operatorsLicense}",
                                          height: 100)
                                      : Image.asset(AppAsset.pdf, height: 100),
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.edit,
                                    color: Constant.primaryColor),
                              )
                            ]),
                      ),
                      SizedBox(height: 15),
                      Text("TB Clearance"),
                      SizedBox(height: 5),
                      GestureDetector(
                        onTap: () {
                          _vm.pickDoc(1);
                        },
                        child: Stack(
                            alignment: AlignmentDirectional.topEnd,
                            children: [
                              _vm.clearancefile != null
                                  ? Image.file(_vm.clearancefile!, height: 100)
                                  : (widget.user.profile?.tbClearance != null)
                                      ? Image.network(
                                          "${IMG_PATH_MASTER}SubUserProfile/${widget.user.profile?.tbClearance}",
                                          height: 100)
                                      : Image.asset(AppAsset.pdf, height: 100),
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.edit,
                                    color: Constant.primaryColor),
                              )
                            ]),
                      ),
                      SizedBox(height: 15),
                      Text("Physical Checkup"),
                      SizedBox(height: 5),
                      GestureDetector(
                        onTap: () {
                          _vm.pickDoc(2);
                        },
                        child: Stack(
                            alignment: AlignmentDirectional.topEnd,
                            children: [
                              _vm.physicalCheckupfile != null
                                  ? Image.file(_vm.physicalCheckupfile!,
                                      height: 100)
                                  : (widget.user.profile?.physicalCheckup !=
                                          null)
                                      ? Image.network(
                                          "${IMG_PATH_MASTER}SubUserProfile/${widget.user.profile?.physicalCheckup}",
                                          height: 100)
                                      : Image.asset(AppAsset.pdf, height: 100),
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.edit,
                                    color: Constant.primaryColor),
                              )
                            ]),
                      ),
                      SizedBox(height: 15),
                      Text("Miscellaneous Doc"),
                      SizedBox(height: 5),
                      GestureDetector(
                        onTap: () {
                          _vm.pickDoc(3);
                        },
                        child: Stack(
                            alignment: AlignmentDirectional.topEnd,
                            children: [
                              _vm.miscDocsFile != null
                                  ? Image.file(_vm.miscDocsFile!, height: 100)
                                  : (widget.user.profile?.miscDoc != null)
                                      ? Image.network(
                                          "${IMG_PATH_MASTER}SubUserProfile/${widget.user.profile?.miscDoc}",
                                          height: 100)
                                      : Image.asset(AppAsset.pdf, height: 100),
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.edit,
                                    color: Constant.primaryColor),
                              )
                            ]),
                      ),

                      SizedBox(height: 20),

                      loading
                          ? Center(child: CircularProgressIndicator())
                          : CommonButtonWidget(
                              onTap: () {
                                editSubUser(_vm);
                              },
                              text: "Save"),
                      SizedBox(height: 20),
                    ]),
              ),
            )),
      );
    });
  }
}

// import 'package:cat/src/core/common/helper/decoration.dart';
// import 'package:cat/src/core/common/widget/commonAppbar.dart';
// import 'package:cat/src/core/common/widget/commonButton.dart';
// import 'package:cat/src/core/common/widget/commonTextStyle.dart';
// import 'package:cat/src/core/common/widget/common_textfield.dart';
// import 'package:cat/src/core/constant/constant.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_form_builder/flutter_form_builder.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';

// class EditSubuserScreen extends StatefulWidget {
//   const EditSubuserScreen({super.key});

//   @override
//   State<EditSubuserScreen> createState() => _EditSubuserScreenState();
// }

// class _EditSubuserScreenState extends State<EditSubuserScreen> {
//   TextEditingController name = TextEditingController();
//   TextEditingController reason = TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     return Consumer(builder: (context, ref, child) {
//       return Container(
//           height: double.infinity,
//           width: double.infinity,
//           decoration: BoxDecoration(
//               image: DecorationImage(
//                   image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
//           child: Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar:
//                   CommonAppBar(isBack: true, title: "Change Subuser"),
//               body: Padding(
//                 padding: const EdgeInsets.all(15.0),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     RichText(
//                         textAlign: TextAlign.justify,
//                         maxLines: 1,
//                         text: TextSpan(
//                             style: TextStyle(
//                                 fontSize: 20,
//                                 color: Constant.primaryColor,
//                                 fontFamily:
//                                     AppFont.josefinSansMedium),
//                             children: [
//                               TextSpan(text: "Current sub user"),
//                               WidgetSpan(
//                                   alignment:
//                                       PlaceholderAlignment.middle,
//                                   child: Padding(
//                                       padding: EdgeInsets.symmetric(
//                                           horizontal: 10),
//                                       child: CircleAvatar(
//                                           backgroundImage: NetworkImage(
//                                               "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Z2lybCUyMGltYWdlc3xlbnwwfHwwfHw%3D&w=1000&q=80"),
//                                           radius: 20,
//                                           backgroundColor:
//                                               Colors.blueAccent))),
//                               WidgetSpan(
//                                   child: BoldText(text: "Jenny John"))
//                             ])),
//                     SizedBox(height: 20),
//                     FormBuilderDropdown<String>(
//                         decoration: textFieldDecoration(
//                             hintText: "Change/Remove"),
//                         name: "change",
//                         items: ["Demo 1", "Demo 1"]
//                             .map((v) => DropdownMenuItem(
//                                 value: v, child: Text(v)))
//                             .toList(),
//                         onChanged: (val) => {}),
//                     SizedBox(height: 20),
//                     CommonTextFiled(
//                         controller: name,
//                         suffixIcon: Icon(Icons.calendar_month),
//                         hintText: "Search name",
//                         name: "Search name",
//                         validator: null),
//                     SizedBox(height: 20),
//                     CommonTextFiled(
//                         maxLine: 5,
//                         controller: name,
//                         hintText: "Reason",
//                         name: "Reason",
//                         validator: null),
//                     SizedBox(height: 20),
//                     CommonButtonWidget(
//                         onTap: () {
//                           Navigator.pop(context);
//                         },
//                         text: "Update Now"),
//                     SizedBox(height: 20),
//                   ],
//                 ),
//               )));
//     });
//   }
// }
