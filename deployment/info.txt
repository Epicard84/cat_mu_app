D:\>keytool -genkey -v -keystore D:\key.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias key
Enter keystore password:brainium210623
Re-enter new password:brainium210623
What is your first and last name?
  [Unknown]:  Edward Picard
What is the name of your organizational unit?
  [Unknown]:  MyChartSpace
What is the name of your organization?
  [Unknown]:  MyChartSpace
What is the name of your City or Locality?
  [Unknown]:  Hawaii
What is the name of your State or Province?
  [Unknown]:  Hawaii
What is the two-letter country code for this unit?
  [Unknown]:  01
Is CN=Edward Picard, OU=MyChartSpace, O=MyChartSpace, L=Hawaii, ST=Hawaii, C=01 correct?
  [no]:  yes

Generating 2,048 bit RSA key pair and self-signed certificate (SHA256withRSA) with a validity of 10,000 days
        for: CN=Edward Picard, OU=MyChartSpace, O=MyChartSpace, L=Hawaii, ST=Hawaii, C=01
Enter key password for <key>