import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final clientsProvider = ChangeNotifierProvider.autoDispose<ClientVM>((ref) {
  return ClientVM();
});

bool icon = false;
int hours = 1;

class ClientVM with ChangeNotifier {
  ClientVM() {}

  checkBox() {
    icon = !icon;
    notifyListeners();
  }

  countingP() {
    hours++;
    notifyListeners();
  }

  countingM() {
    if (hours > 1) {
      hours--;
    }
    notifyListeners();
  }
}
