import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:cat/src/core/common/client/APIhelper.dart';
import 'package:cat/src/core/common/helper/decoration.dart';
import 'package:cat/src/core/common/helper/string.dart';
import 'package:cat/src/core/common/helper/utils.dart';
import 'package:cat/src/core/common/widget/commonAppbar.dart';
import 'package:cat/src/core/common/widget/commonButton.dart';
import 'package:cat/src/core/common/widget/commonTextStyle.dart';
import 'package:cat/src/core/common/widget/common_textfield.dart';
import 'package:cat/src/core/common/widget/snackbar.dart';
import 'package:cat/src/core/constant/constant.dart';
import 'package:cat/src/feature/home/model/activity.dart';
import 'package:cat/src/feature/home/model/client.dart' as c;
import 'package:cat/src/feature/home/ui/clients/activity/add/add_activity_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class AddActivityScreen extends ConsumerStatefulWidget {
  final c.ClientData client;
  final List<GetActivities> item;
  const AddActivityScreen(
      {super.key, required this.client, required this.item});

  @override
  ConsumerState<AddActivityScreen> createState() => _AddActivityScreenState();
}

class _AddActivityScreenState extends ConsumerState<AddActivityScreen> {
  bool loading = false;
  final client = HTTPClient();
  final _formKey = GlobalKey<FormState>();

  void initState() {
    super.initState();
    setData();
  }

  setData() {
    ref.read(addActivityVm).setData(widget.item);
  }

  addActivity(AddActivityVm _vm, int index, {String id = ""}) async {
    try {
      setState(() {
        loading = true;
      });

      List<String> time = [];

      for (var i in _vm.activityList[index].times) {
        // time.add(i.text);
        time.add(i);
      }

      print(time);

      String clean = time
          .toString()
          .replaceAll('[', '')
          .replaceAll(']', '')
          .replaceAll(" ", "");

      var data = {
        "act_id": "$id",
        'client_id': "${widget.client.id}",
        'activity_name': _vm.activityList[index].activity.text,
        'start_date': _vm.activityList[index].start,
        'description': _vm.activityList[index].desc.text,
        'time_days': _vm.activityList[index].perDay,
        'status': _vm.activityList[index].status,
        'scheduled_time': clean
      };

      log("data ${data}");
      http.StreamedResponse res = await client.postFileWithToken(
          APIEndpoint.addActivity, data,
          fileKey: "docs", files: _vm.activityList[index].image);
      var respp = await res.stream.bytesToString();

      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(respp);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          Navigator.pop(context, true);
        } else {
          showSnackBar(context, text: "${jsonResp['message']}");
        }
      } else {
        var jsonResp = jsonDecode(respp);
        showSnackBar(context, text: "${jsonResp['message']}");
      }
    } catch (e) {
      print("error >> $e");
      // showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  deleteActivity(String id) async {
    try {
      setState(() {
        loading = true;
      });
      var res =
          await await client.getWithToken("${APIEndpoint.deleteActivity}/$id");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          Navigator.pop(context, true);
        }
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  archiveActivity(String id) async {
    try {
      setState(() {
        loading = true;
      });
      var res =
          await await client.getWithToken("${APIEndpoint.archiveActivity}/$id");
      if (res.statusCode == 200) {
        var jsonResp = jsonDecode(res.body);
        if (jsonResp['success']) {
          showSnackBar(context, text: "${jsonResp['message']}", success: true);
          Navigator.pop(context, true);
        }
      }
    } catch (e) {
      print("error >> $e");
      showSnackBar(context, text: ValidationString.somethingWentWrong);
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      var size = MediaQuery.of(context).size;
      final _vm = ref.watch(addActivityVm);
      return Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppAsset.bg2), fit: BoxFit.fill)),
          child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: CommonAppBar(isBack: true, title: "Manage Activity"),
              body: Padding(
                padding: const EdgeInsets.all(15.0),
                child: loading
                    ? Center(child: CircularProgressIndicator())
                    : SingleChildScrollView(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                height: 60,
                                width: size.width,
                                decoration: BoxDecoration(
                                    color: Constant.primaryColor,
                                    borderRadius: BorderRadius.circular(15)),
                                child: Container(
                                    padding: EdgeInsets.all(20),
                                    alignment: Alignment.center,
                                    child: BoldText(
                                        text:
                                            "${widget.client.firstName} ${widget.client.lastName}",
                                        textColor: Colors.white)),
                              ),
                              SizedBox(height: 20),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: _vm.activityList.length,
                                  itemBuilder: (BuildContext context, int i) {
                                    var item = _vm.activityList[i];
                                    return Container(
                                      margin:
                                          EdgeInsets.symmetric(vertical: 10),
                                      padding: EdgeInsets.all(10),
                                      width: size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              BoldText(
                                                  text: "Activity ${i + 1}"),
                                              Row(
                                                children: [
                                                  item.isUpdate
                                                      ? item.status == "1"
                                                          ? Text('Archived')
                                                          : IconButton(
                                                              onPressed:
                                                                  () async {
                                                                await archiveActivity(
                                                                    item.id);
                                                              },
                                                              icon: Icon(Icons
                                                                  .folder_open))
                                                      : Container(),
                                                  IconButton(
                                                      onPressed: () {
                                                        item.isUpdate
                                                            ? deleteActivity(
                                                                item.id)
                                                            : _vm
                                                                .removeActivity(
                                                                    item);
                                                      },
                                                      icon: Icon(Icons.delete)),
                                                ],
                                              )
                                            ],
                                          ),
                                          SizedBox(height: 20),
                                          CommonTextFiled(
                                            isEnabled: !item.isUpdate,
                                            controller: item.activity,
                                            hintText: "Activity",
                                            name: "Activity",
                                            validator: (value) {
                                              if (value == null) {
                                                return 'This field is required';
                                              }

                                              return null;
                                            },
                                          ),
                                          SizedBox(height: 20),
                                          SubText(text: "Start Date"),
                                          FormBuilderDateTimePicker(
                                              enabled: !item.isUpdate,
                                              name: 'from',
                                              // controller: item.start,
                                              initialEntryMode:
                                                  DatePickerEntryMode.calendar,
                                              initialValue: item.start != ""
                                                  ? DateFormat("yyyy-MM-dd")
                                                      .parse(item.start)
                                                  : null,
                                              firstDate: DateTime.now(),
                                              inputType: InputType.date,
                                              validator: (value) {
                                                if (value == null) {
                                                  return 'The date field is required';
                                                }

                                                return null;
                                              },
                                              onChanged: (e) {
                                                if (e != null)
                                                  setState(() {
                                                    item.start =
                                                        "${e.year}-${e.month}-${e.day}";
                                                  });
                                              },
                                              decoration: textFieldDecoration(
                                                  hintText: "Start Date",
                                                  prefix: Icon(
                                                      Icons.calendar_month))),
                                          SizedBox(height: 20),
                                          CommonTextFiled(
                                              maxLine: 3,
                                              keyboardType:
                                                  TextInputType.multiline,
                                              controller: item.desc,
                                              hintText: "Description",
                                              name: "Description",
                                              validator: null),
                                          SizedBox(height: 20),
                                          FormBuilderDropdown<String>(
                                              decoration: textFieldDecoration(
                                                  hintText: "Time/Day"),
                                              name: "time",
                                              initialValue: item.perDay,
                                              validator: null,
                                              items: [
                                                "1",
                                                "2",
                                                "3",
                                                "4",
                                                "5",
                                                "6",
                                                "7",
                                                "8"
                                              ]
                                                  .map((v) => DropdownMenuItem(
                                                      value: v,
                                                      child: Text(v == "1"
                                                          ? "$v Time"
                                                          : "$v Times")))
                                                  .toList(),
                                              onChanged: (val) {
                                                _vm.setTime(val!, i);
                                                _vm.addTime(i, val);
                                              }),
                                          SizedBox(height: 10),
                                          Text(
                                            // "* Time is based on $timezone timezone"),
                                            "* Based on Local Time. 00:00 = As needed (PRN)",
                                            style: TextStyle(color: Colors.red),
                                          ),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount: item.times.length,
                                              padding: EdgeInsets.zero,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int i) {
                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 5),
                                                  child:
                                                      // CommonTextFiled(
                                                      //     controller: dt,
                                                      //     hintText: "Enter Time",
                                                      //     name: "time",
                                                      //     validator: null),

                                                      FormBuilderDropdown<String>(
                                                          initialValue:
                                                              item.times[i],
                                                          decoration: textFieldDecoration(
                                                              hintText:
                                                                  "Select Time"),
                                                          name: "timesss",
                                                          validator: FormBuilderValidators.compose(
                                                              [FormBuilderValidators.required()]),
                                                          items: _vm.timesString
                                                              .map((v) => DropdownMenuItem(
                                                                  value: v,
                                                                  child:
                                                                      Text(v)))
                                                              .toList(),
                                                          onChanged: (val) =>
                                                              _vm.setTimeDropdown(
                                                                  _vm.activityList
                                                                      .indexOf(
                                                                          item),
                                                                  item.times.indexOf(
                                                                      item.times[i]),
                                                                  val!)),

                                                  // FormBuilderDateTimePicker(
                                                  //     name: '$dt',
                                                  //     controller: dt,
                                                  //     initialEntryMode:
                                                  //         DatePickerEntryMode
                                                  //             .calendar,
                                                  //     initialValue: dt
                                                  //                 .text !=
                                                  //             ""
                                                  //         ? DateFormat(
                                                  //                 "hh:mm")
                                                  //             .parse(
                                                  //                 dt.text)
                                                  //         : null,
                                                  //     firstDate:
                                                  //         DateTime.now(),
                                                  //     inputType:
                                                  //         InputType.time,
                                                  //     validator: (value) {
                                                  //       if (value == null) {
                                                  //         return 'The time field is required';
                                                  //       }

                                                  //       return null;
                                                  //     },
                                                  //     onChanged: (e) {
                                                  //       if (e != null)
                                                  //         _vm.setCtrl(
                                                  //             _vm.activityList
                                                  //                 .indexOf(
                                                  //                     item),
                                                  //             item.times
                                                  //                 .indexOf(
                                                  //                     dt),
                                                  //             "${e.hour.toString().padLeft(2, '0')}:${e.minute.toString().padLeft(2, '0')}");
                                                  //     },
                                                  //     decoration: textFieldDecoration(
                                                  //         hintText:
                                                  //             "Select Time",
                                                  //         prefix: Icon(Icons
                                                  //             .schedule))),
                                                );
                                              }),
                                          SizedBox(height: 20),
                                          item.isUpdate
                                              ? Container()
                                              : Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text("Upload Documents"),
                                                    SizedBox(height: 10),
                                                    Row(
                                                      children: [
                                                        Container(
                                                            decoration: BoxDecoration(
                                                                color: Constant
                                                                    .primaryColor,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15)),
                                                            child: IconButton(
                                                                onPressed: () {
                                                                  _vm.openCamera(
                                                                      i);
                                                                },
                                                                icon: Icon(
                                                                    Icons
                                                                        .photo_camera_outlined,
                                                                    color: Colors
                                                                        .white))),
                                                        SizedBox(width: 20),
                                                        Container(
                                                            decoration: BoxDecoration(
                                                                color: Constant
                                                                    .primaryColor,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15)),
                                                            child: IconButton(
                                                                onPressed: () {
                                                                  _vm.pickFile(
                                                                      i);
                                                                },
                                                                icon: Icon(
                                                                    Icons
                                                                        .attachment,
                                                                    color: Colors
                                                                        .white))),
                                                        SizedBox(width: 20),
                                                        Platform.isIOS
                                                            ? Container(
                                                                decoration: BoxDecoration(
                                                                    color: Constant
                                                                        .primaryColor,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            15)),
                                                                child:
                                                                    IconButton(
                                                                        onPressed:
                                                                            () {
                                                                          _vm.pickPhotos(
                                                                              i);
                                                                        },
                                                                        icon: Icon(
                                                                            Icons
                                                                                .image_outlined,
                                                                            color:
                                                                                Colors.white)))
                                                            : SizedBox()
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                          SizedBox(height: 20),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount: item.imagepath.length,
                                              padding: EdgeInsets.zero,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int j) {
                                                var text = item.imagepath[j];
                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 5.0),
                                                  child: !text
                                                          .split('/')
                                                          .last
                                                          .contains(".")
                                                      ? SizedBox()
                                                      : ListTile(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          10))),
                                                          trailing: IconButton(
                                                              onPressed: () {
                                                                print(
                                                                    "${"${BASE_URL}/activity_download/$text"}");
                                                                downloadOpenFile(
                                                                    url:
                                                                        "${BASE_URL}/activity_download/$text",
                                                                    fileName: text
                                                                        .split(
                                                                            '/')
                                                                        .last);
                                                              },
                                                              icon: Icon(Icons
                                                                  .download)),
                                                          tileColor:
                                                              Colors.white,
                                                          title: Text(text
                                                              .split('/')
                                                              .last)),
                                                );
                                              }),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount: item.image.length,
                                              padding: EdgeInsets.zero,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int j) {
                                                var text = item.image[j];
                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 5.0),
                                                  child: ListTile(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10))),
                                                      tileColor: Colors.white,
                                                      trailing: IconButton(
                                                          onPressed: () {
                                                            _vm.removeFile(
                                                                i, j);
                                                          },
                                                          icon: Icon(
                                                              Icons.close)),
                                                      title: Text(text.path
                                                          .split('/')
                                                          .last)),
                                                );
                                              }),
                                          CommonButtonWidget(
                                              onTap: () {
                                                if (item.times.length !=
                                                    int.parse(item.perDay)) {
                                                  showSnackBar(context,
                                                      text:
                                                          "Please enter correct times");
                                                } else {
                                                  // for (var i in item.times) {
                                                  //   if (i.text == "") {
                                                  //     showSnackBar(context,
                                                  //         text:
                                                  //             "The time field is required");
                                                  //     return;
                                                  //   }
                                                  // }

                                                  if (_formKey.currentState!
                                                      .validate()) {
                                                    item.isUpdate
                                                        ? addActivity(_vm, i,
                                                            id: item.id)
                                                        : addActivity(_vm, i);
                                                  }
                                                }
                                              },
                                              text: item.isUpdate
                                                  ? "Update"
                                                  : "Save"),
                                        ],
                                      ),
                                    );
                                  }),
                              SizedBox(height: 20),
                              !_vm.showAdd
                                  ? SizedBox()
                                  : CommonButtonWidget(
                                      onTap: () {
                                        _vm.addActivity();
                                      },
                                      text: "Add New Activity"),
                              SizedBox(height: 20),
                            ],
                          ),
                        ),
                      ),
              )));
    });
  }
}
